package com.github.xemiru.miniboi.textmenu;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * A menu interface based on Minecraft's clickable text.
 */
public class TextMenu implements PageDisplayable {

    /**
     * Builder class for creating {@link TextMenu} instances.
     */
    public static class Builder {

        private TextMenu tm;
        private TextMenuBlock.Builder body, footer;

        private Builder() {
            this.tm = new TextMenu();
            this.body = TextMenuBlock.builder();
            this.footer = TextMenuBlock.builder();
        }

        /**
         * Sets the header element of the {@link TextMenu}.
         *
         * @param header the header element
         * @return this Builder
         */
        public Builder setHeader(LanguageText header) {
            this.tm.header = () -> header;
            return this;
        }

        /**
         * Sets the header element generator of the {@link TextMenu}.
         *
         * @param header the header element generator
         * @return this Builder
         */
        public Builder setHeader(Supplier<LanguageText> header) {
            this.tm.header = requireNonNull(header);
            return this;
        }

        /**
         * Sets the {@link PageDisplayable} parent of the {@link TextMenu} to redirect to when requested.
         *
         * @param parent the parent PageDisplayable
         * @return this Builder
         */
        public Builder setParent(PageDisplayable parent) {
            this.tm.parent = parent;
            return this;
        }

        /**
         * Configure the {@link TextMenu}'s body block.
         *
         * @param config the configuration function
         * @return this Builder
         */
        public Builder configureBody(Consumer<TextMenuBlock.Builder> config) {
            config.accept(this.body);
            return this;
        }

        /**
         * Configure the {@link TextMenu}'s footer block.
         *
         * @param config the configuration function
         * @return this Builder
         */
        public Builder configureFooter(Consumer<TextMenuBlock.Builder> config) {
            config.accept(this.footer);
            return this;
        }

        /**
         * Returns the {@link TextMenu} configured by the properties set upon this {@link Builder}.
         *
         * @return the TextMenu
         */
        public TextMenu build() {
            this.tm.body = this.body.build();
            this.tm.footer = this.footer.build();
            return this.tm;
        }

    }

    /**
     * The default amount of items that can be displayed by the body section of a {@link TextMenu}.
     */
    private static final int MAX_PAGE_ITEMS = 10;

    private static final String PAGE_FIRST = "\u00AB";
    private static final String PAGE_PREVIOUS = "<";
    private static final String PAGE_PARENT = "@";
    private static final String PAGE_NEXT = ">";
    private static final String PAGE_LAST = "\u00BB";

    /**
     * Generates a page navigation bar for the given {@link PageDisplayable}s.
     *
     * @param parent the parent PageDisplayable
     * @param disp the PageDisplayable owning the navbar
     * @param page the current page
     * @param pageMax a supplying function returning the maximum pages in the owning PageDisplayable
     * @return a page navigation bar
     */
    private static LanguageText createPageNav(PageDisplayable parent, PageDisplayable disp, int page, Supplier<Integer> pageMax) {
        Consumer<Player> first, previous, up, next, last;

        first = p -> {
            if (page == 0) Language.msgError(Language.PAGENAV_ERROR_ONFIRST).send(p);
            else disp.display(parent, p, 0);
        };

        previous = p -> {
            if (page == 0) Language.msgError(Language.PAGENAV_ERROR_ONFIRST).send(p);
            else disp.display(parent, p, page - 1);
        };

        up = parent == null ? null : p -> parent.display(p, 0);

        next = p -> {
            int max = pageMax.get();
            if (page + 1 == max) Language.msgError(Language.PAGENAV_ERROR_ONLAST).send(p);
            else disp.display(parent, p, page + 1);
        };

        last = p -> {
            int max = pageMax.get();
            if (page + 1 == max) Language.msgError(Language.PAGENAV_ERROR_ONLAST).send(p);
            else disp.display(parent, p, max - 1);
        };

        return LanguageText.builder()
            .color(TextColors.AQUA)

            // first page button
            .action(first)
            .hover(Language.hover(Language.PAGENAV_LABEL, Language.PAGENAV_BUTTON_TOFIRST))
            .append(TextStyles.BOLD, PAGE_FIRST)

            // previous page button
            .action(previous)
            .hover(Language.hover(Language.PAGENAV_LABEL, Language.PAGENAV_BUTTON_TOPREVIOUS))
            .append(TextStyles.BOLD, PAGE_PREVIOUS)

            // parent button
            .color(parent == null ? TextColors.DARK_GRAY : TextColors.AQUA)
            .hover(Language.hover(Language.PAGENAV_LABEL,
                parent == null ? Language.PAGENAV_BUTTON_TOOUTER_UNAVAILABLE : Language.PAGENAV_BUTTON_TOOUTER_AVAILABLE))
            .action(up)
            .append(PAGE_PARENT)

            // current / max page
            .reset()
            .color(TextColors.AQUA)
            .append(String.format(" %d / %d ", page + 1, pageMax.get()))

            // next page button
            .action(next)
            .hover(Language.hover(Language.PAGENAV_LABEL, Language.PAGENAV_BUTTON_TONEXT))
            .append(TextStyles.BOLD, PAGE_NEXT)

            // last page button
            .action(last)
            .hover(Language.hover(Language.PAGENAV_LABEL, Language.PAGENAV_BUTTON_TOLAST))
            .append(TextStyles.BOLD, PAGE_LAST)
            .build();
    }

    /**
     * @return a {@link TextMenu.Builder}
     */
    public static TextMenu.Builder builder() {
        return new Builder();
    }

    private Supplier<LanguageText> header;
    private PageDisplayable parent;
    private TextMenuBlock body, footer;

    private TextMenu() {
        this.header = () -> LanguageText.of(Language.PAGENAV_LABEL_UNNAMED);
    }

    @Override
    public Optional<PageDisplayable> getParent() {
        return Optional.ofNullable(this.parent);
    }

    @Override
    public void display(PageDisplayable parent, Player to, int page) {
        try {
            // give the chance of throwing ioobe first before anything else
            List<LanguageText> elements = this.body.getElements(to, page, MAX_PAGE_ITEMS);
            LanguageText.builder()
                .format(Language.FORMAT_TITLE)
                .append(TextMenu.createPageNav(parent, this, page, () -> this.body.getMaxPages(to, MAX_PAGE_ITEMS)))
                .reset()
                .format(Language.FORMAT_TITLE)
                .append(" ", this.header.get())
                .build().send(to);

            elements.forEach(text -> text.send(to));
            this.footer.getElements(to, 0, Integer.MAX_VALUE).forEach(text -> text.send(to));
        } catch (IndexOutOfBoundsException ignored) {
            boolean first = page < 0;
            if (first) this.display(parent, to, 0);
            else this.display(parent, to, this.body.getMaxPages(to, MAX_PAGE_ITEMS) - 1);
        }
    }
}
