package com.github.xemiru.miniboi.textmenu;

import com.github.xemiru.miniboi.language.LanguageText;
import org.spongepowered.api.entity.living.player.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;

import static java.util.Objects.requireNonNull;

/**
 * A block displayed by a {@link TextMenu}.
 *
 * <p>Elements displayed by the block are ordered based on insertion order; if one adds static elements before dynamic
 * elements, the static elements will appear first as their own section.</p>
 *
 * <p>Each insertion of static elements and dynamic elements are placed in their own section; they will <b>never be
 * mixed.</b></p>
 */
public class TextMenuBlock {

    /**
     * Builder class for creating {@link TextMenuBlock} instances.
     */
    public static class Builder {

        private TextMenuBlock obj;

        private Builder() {
            this.obj = new TextMenuBlock();
        }

        /**
         * Adds a set of static elements to this {@link TextMenuBlock}.
         *
         * @param elements the elements to add
         * @return this TextMenuBlock
         *
         * @see TextMenuBlock How elements are treated
         */
        public Builder addStaticElements(List<LanguageText> elements) {
            return this.addDynamicElements((player, page) -> elements);
        }

        /**
         * Adds a set of static elements to this {@link TextMenuBlock}.
         *
         * @param elements the elements to add
         * @return this TextMenuBlock
         *
         * @see TextMenuBlock How elements are treated
         */
        public Builder addStaticElements(LanguageText... elements) {
            List<LanguageText> list = new ArrayList<>();
            Collections.addAll(list, elements);
            return this.addStaticElements(list);
        }

        /**
         * Adds a set of dynamically-generated elements to this {@link TextMenuBlock}.
         *
         * <p>The function will receive the target {@link Player} and requested page index, allowing the factory to
         * build elements that respond by redisplaying the same page.</p>
         *
         * @param elementGen the element generating function
         * @return this TextMenuBlock
         *
         * @see TextMenuBlock How elements are treated
         */
        public Builder addDynamicElements(BiFunction<Player, Integer, List<LanguageText>> elementGen) {
            obj.elementGen.add(elementGen);
            return this;
        }

        /**
         * Sets dynamically-generated pages onto this {@link TextMenuBlock}.
         *
         * <p>The function will receive the target {@link Player} and requested page index, allowing the factory to
         * build elements that respond by redisplaying the same page.</p>
         *
         * <p>Note the label "set" as opposed to "add"; pages are only added once and are always displayed <b>after</b> all
         * other elements.</p>
         *
         * @param pageFactory the page-generating function
         * @return this TextMenuBlock
         */
        public Builder setPageFactory(BiFunction<Player, Integer, List<List<LanguageText>>> pageFactory) {
            obj.pageGen = requireNonNull(pageFactory);
            return this;
        }

        /**
         * Returns the {@link TextMenuBlock} configured by the properties set upon this {@link Builder}.
         *
         * @return the TextMenuBlock
         */
        public TextMenuBlock build() {
            return this.obj;
        }

    }

    /**
     * @return a {@link TextMenuBlock.Builder}
     */
    public static TextMenuBlock.Builder builder() {
        return new Builder();
    }

    private List<BiFunction<Player, Integer, List<LanguageText>>> elementGen = new ArrayList<>();
    private BiFunction<Player, Integer, List<List<LanguageText>>> pageGen = (player, page) -> Collections.emptyList();

    /**
     * Returns the amount of pages held by this {@link TextMenuBlock} when given the provided max item count.
     *
     * @param player the target {@link Player} of the elements
     * @param maxItems the maximum item count to use
     * @return the page count
     */
    public int getMaxPages(Player player, int maxItems) {
        List<List<LanguageText>> pages = this.pageGen.apply(player, 0);
        if (this.elementGen.isEmpty() && pages.isEmpty()) return 0;

        int max = 0;
        for (BiFunction<Player, Integer, List<LanguageText>> supplier : this.elementGen) {
            List<LanguageText> elements = supplier.apply(player, 0);
            int subPages = elements.size() / maxItems;
            subPages += elements.size() % maxItems == 0 ? 0 : 1;

            max += subPages;
        }

        return max + pages.size();
    }

    /**
     * Returns a page of elements held by this {@link TextMenuBlock}, with all elements (except those provided by a page
     * factory) paginated with the given maximum item count.
     *
     * <p>It is possible for this method to return an empty list if no elements were found (only if {@code page} is
     * 0).</p>
     *
     * @param target the target {@link Player} of the elements
     * @param page the 0-based page index
     * @param maxItems the maximum items allowed to be held by a page
     * @return a page of elements
     *
     * @throws IndexOutOfBoundsException if page is below 0 or over the maximum amount of pages
     */
    public List<LanguageText> getElements(Player target, int page, int maxItems) {
        if (page < 0) throw new IndexOutOfBoundsException();

        List<List<LanguageText>> pages = this.pageGen.apply(target, page);
        if (this.elementGen.isEmpty() && pages.isEmpty()) return Collections.emptyList();

        int pageMin = 0;
        int subpage = 0;
        List<LanguageText> elements = null;
        // Each supplier counts as its own section and should be paginated individually.
        for (BiFunction<Player, Integer, List<LanguageText>> supplier : this.elementGen) {
            List<LanguageText> supplied = supplier.apply(target, page);
            if (supplied.isEmpty()) continue; // Ignore empty list.

            int subPages = supplied.size() / maxItems;
            subPages += supplied.size() % maxItems == 0 ? 0 : 1;

            if (page < pageMin + subPages) {
                subpage = page - pageMin;
                elements = supplied;
                break;
            }

            pageMin += subPages;
        }

        if (elements == null) {
            // We've gone over all the element generators; try page generator.
            if (page < pageMin + pages.size()) {
                elements = pages.get(page - pageMin);
                subpage = -1;
            }
        }

        if (elements == null) {
            if (pageMin == 0 && pages.isEmpty()) {
                // We didn't have any elements at all.
                return Collections.emptyList();
            }

            // We've gone completely over everything and have not reached a page; they've went over the max.
            throw new IndexOutOfBoundsException();
        } else if (subpage > -1) {
            // A subpage of -1 means the page was taken from the page generator and has been premade.
            // Otherwise, the current list of elements need to be paginated by the max amount of items.

            int minIndex = subpage * maxItems;
            int maxIndex = ((subpage + 1) * maxItems);
            if (maxIndex > elements.size()) maxIndex = elements.size();

            elements = elements.subList(minIndex, maxIndex);
        }

        return elements;
    }

}
