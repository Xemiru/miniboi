package com.github.xemiru.miniboi.textmenu;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

import java.util.Optional;

/**
 * An object with displayable content separated in pages.
 */
public interface PageDisplayable {

    /**
     * Returns the default parent of this {@link PageDisplayable}.
     *
     * @return the default parent
     */
    default Optional<? extends PageDisplayable> getParent() {
        return Optional.empty();
    }

    /**
     * Displays the content of this {@link PageDisplayable} to the given {@link Player}.
     *
     * <p>It is the responsibility of the implementing class to show interactive {@link Text} to the target Player that
     * allows them to interface with the value of this PageDisplayable, if needed.</p>
     *
     * <p>Note that page values are treated as 1-based values -- a request to display the first page is given the page
     * value of 1, not 0.</p>
     *
     * @param to the Player to show to
     * @param page the page to show
     */
    default void display(Player to, int page) {
        this.display(null, to, page);
    }

    /**
     * Displays the content of this {@link PageDisplayable} to the given {@link Player}.
     *
     * <p>It is the responsibility of the implementing class to show interactive {@link Text} to the target Player that
     * allows them to interface with the value of this PageDisplayable, if needed.</p>
     *
     * <p>Note that page values are treated as 1-based values -- a request to display the first page is given the page
     * value of 1, not 0.</p>
     *
     * @param parent the parent {@link PageDisplayable} that linked to this one, if any
     * @param to the Player to show to
     * @param page the page to show
     */
    void display(PageDisplayable parent, Player to, int page);

}
