package com.github.xemiru.miniboi.input;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.textmenu.TextMenu;
import com.github.xemiru.miniboi.util.Choice;
import com.github.xemiru.miniboi.textmenu.PageDisplayable;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.translation.Translatable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Receives input by allowing a {@link Player} to select a choice from a predefined list.
 */
public class ChoiceInputHandler<T> extends InputHandler<T> implements PageDisplayable {

    private List<Choice<T>> choices = new ArrayList<>();
    private TextMenu menu;

    {
        this.menu = TextMenu.builder()
            .setHeader(Language.prompt(Language.INPUT_CHOICE_LABEL))
            .configureBody(body -> body.addDynamicElements((pl, pg) -> {
                List<LanguageText> page = new ArrayList<>();
                this.getPrompt().ifPresent(page::add);

                this.choices.forEach(choice -> {
                    String repText = choice.getLabel().getTranslation().get(pl.getLocale());
                    if (repText.length() > 32)
                        repText = repText.substring(0, 30).concat("..");

                    page.add(LanguageText.builder()
                        .format(Language.FORMAT_SUBTITLE)
                        .hover(Language.hover(choice.getLabel(), choice.getDescription().orElse(Language.GENERIC_NODESC)))
                        .action(p -> this.submit(choice.getValue()))
                        .append("*) ")
                        .append(Language.FORMAT_INFO, repText)
                        .build());
                });

                return page;
            }))
            .configureFooter(footer -> footer.addStaticElements(LanguageText.builder()
                .format(Language.FORMAT_ERROR)
                .hover(Language.info(Language.INPUT_CHOICE_CANCEL_DESC))
                .action(p -> this.cancel(CancelReason.USER))
                .append("*) ")
                .append(Language.FORMAT_INFO, Language.INPUT_CHOICE_CANCEL_LABEL)
                .build()))
            .build();
    }

    /**
     * Adds {@link Choice}s to this {@link ChoiceInputHandler}.
     *
     * @param choices the choices to add
     * @return this ChoiceInputHandler
     */
    public ChoiceInputHandler<T> addChoices(Choice<T>... choices) {
        Collections.addAll(this.choices, choices);
        return this;
    }

    /**
     * Adds {@link Choice}s to this {@link ChoiceInputHandler}.
     *
     * @param choices the choices to add
     * @return this ChoiceInputHandler
     */
    public ChoiceInputHandler<T> addChoices(Collection<Choice<T>> choices) {
        this.choices.addAll(choices);
        return this;
    }

    /**
     * Sets formatted prompt text for this {@link ChoiceInputHandler}.
     *
     * @param prompt the prompt text
     * @return this ChoiceInputHandler
     *
     * @see #setPrompt(LanguageText)
     */
    public ChoiceInputHandler<T> setPrompt(Translatable prompt) {
        this.setPrompt(Language.subtitle(prompt));
        return this;
    }

    @Override
    protected void init() {
        this.getTargetPlayer().ifPresent(p -> this.display(p, 0));
    }

    @Override
    protected void cleanup() {
    }

    @Override
    public void display(PageDisplayable parent, Player to, int page) {
        this.menu.display(parent, to, page);
    }
}
