package com.github.xemiru.miniboi.input;

import com.github.xemiru.miniboi.language.Language;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.text.translation.Translatable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.BiPredicate;

import static java.util.Objects.requireNonNull;

/**
 * Manages input queries made through {@link InputHandler}s.
 */
public class InputManager {

    private Object plugin;
    private BiPredicate<Player, Translatable[]> errHandler;
    private Map<UUID, InputHandler<?>> active = new HashMap<>();

    public InputManager(Object plugin) {
        this.plugin = requireNonNull(plugin);
        this.errHandler = (p, err) -> {
            if (err.length == 1)
                Language.msgError(Language.INPUT_GENERIC_ERROR_SINGLE.withParameters(err[0])).send(p);
            else {
                Language.msgError(Language.INPUT_GENERIC_ERROR_MULTI).send(p);
                for (Translatable line : err) Language.msgError("- ", line).send(p);
            }

            return true;
        };

        Sponge.getEventManager().registerListeners(plugin, this);
    }

    /**
     * Checks the given {@link InputHandler} for validity against this {@link InputManager}.
     *
     * @param handler the InputHandler to check
     * @return whether or not the handler is still valid
     */
    boolean isValid(InputHandler<?> handler) {
        UUID target = handler.getTarget();
        if (this.active.containsKey(target)) {
            InputHandler<?> in = this.active.get(target);
            return in == handler;
        }

        return false;
    }

    /**
     * Removes an {@link InputHandler}, if it was valid according to {@link #isValid(InputHandler)}.
     *
     * @param handler the InputHandler to remove
     */
    void removeHandler(InputHandler<?> handler) {
        if (this.isValid(handler)) this.active.remove(handler.getTarget(), handler);
    }

    @Listener
    public void onQuit(ClientConnectionEvent.Disconnect e) {
        UUID uid = e.getTargetEntity().getUniqueId();
        if (this.active.containsKey(uid)) this.active.remove(uid).cancel(InputHandler.CancelReason.QUIT);
    }

    /**
     * @return the plugin instance owning this {@link InputManager}
     */
    public Object getPlugin() {
        return this.plugin;
    }

    /**
     * Queries a {@link Player} for input using the provided {@link InputHandler}.
     *
     * <p>This method will also cancel any previous queries currently set on the target player, if any.</p>
     *
     * @param player the Player to query
     * @param handler the InputHandler to query with
     * @param <T> the type of the input
     * @return whether or not a previous query was cancelled as a result of this call
     */
    public <T> boolean query(Player player, InputHandler<T> handler) {
        UUID uid = player.getUniqueId();

        handler.withManager(this);
        handler.withTarget(player.getUniqueId());

        InputHandler<?> old = this.active.get(uid);
        if (old != null) old.cancel(InputHandler.CancelReason.REPLACE);

        this.active.put(uid, handler);
        handler.init();

        if (handler.getTimeout() > 0) Sponge.getScheduler().createTaskBuilder()
            .delay(handler.getTimeout(), TimeUnit.MILLISECONDS)
            .execute(() -> handler.cancel(InputHandler.CancelReason.TIMEOUT))
            .submit(this.plugin);

        return old != null;
    }

    /**
     * Processes an error message using the error handling function set on this {@link InputManager} through
     * {@link #setErrorHandler(BiPredicate)} to determine whether or not processing should continue.
     *
     * @param target the involved Player
     * @param messages the error messages
     * @return if input processing should continue
     */
    boolean handleError(Player target, Translatable... messages) {
        return this.errHandler.test(target, messages);
    }

    /**
     * Sets the error handling function used by this {@link InputManager}.
     *
     * <p>After doing as it wishes with the player and error messages it receives, the function should return whether or
     * not input processing should continue as a result of the error.</p>
     *
     * <p>By default, input processing continues after reporting the errors to the target player.</p>
     *
     * @param handler the new error handling function
     */
    public void setErrorHandler(BiPredicate<Player, Translatable[]> handler) {
        this.errHandler = requireNonNull(handler);
    }

}
