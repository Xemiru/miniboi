package com.github.xemiru.miniboi.input;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.util.Tristate;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * Receives input through selection of a block by the target Player.
 *
 * @see InputHandler
 */
public class BlockInputHandler extends InputHandler<Location<World>> {

    @Listener
    public void onClick(InteractBlockEvent.Secondary e) {
        e.getCause().first(Player.class).filter(it -> it.getUniqueId().equals(this.getTarget())).ifPresent(p -> {
            e.setUseItemResult(Tristate.FALSE);
            e.setUseBlockResult(Tristate.FALSE);
            e.setCancelled(true);

            e.getTargetBlock().getLocation().ifPresent(this::submit);
        });
    }

    @Override
    protected void init() {
        this.getPrompt().ifPresent(tx -> this.getTargetPlayer().ifPresent(tx::send));
        Sponge.getEventManager().registerListeners(this.getManager().getPlugin(), this);
    }

    @Override
    protected void cleanup() {
        Sponge.getEventManager().unregisterListeners(this);
    }

}
