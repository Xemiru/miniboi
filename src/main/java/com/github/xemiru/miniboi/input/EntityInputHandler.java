package com.github.xemiru.miniboi.input;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.InteractEntityEvent;

public class EntityInputHandler extends InputHandler<Entity> {

    @Listener
    public void onClick(InteractEntityEvent.Secondary e) {
        e.getCause().first(Player.class).filter(it -> it.getUniqueId().equals(this.getTarget())).ifPresent(p -> {
            e.setCancelled(true);
            this.submit(e.getTargetEntity());
        });
    }

    @Override
    protected void init() {
        this.getPrompt().ifPresent(tx -> this.getTargetPlayer().ifPresent(tx::send));
        Sponge.getEventManager().registerListeners(this.getManager().getPlugin(), this);
    }

    @Override
    protected void cleanup() {
        Sponge.getEventManager().unregisterListeners(this);
    }

}
