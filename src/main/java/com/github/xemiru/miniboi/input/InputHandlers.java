package com.github.xemiru.miniboi.input;

import com.github.xemiru.miniboi.util.Choice;

import java.util.function.Function;

/**
 * A collection of stock {@link InputHandler}s.
 */
public class InputHandlers {

    public static ChatInputHandler<String> newChatHandler() {
        return newChatHandler(s -> s);
    }

    public static <T> ChatInputHandler<T> newChatHandler(Function<String, T> transform) {
        return new ChatInputHandler<>(transform);
    }

    public static BlockInputHandler newBlockHandler() {
        return new BlockInputHandler();
    }

    public static <T> ChoiceInputHandler<T> newChoiceHandler() {
        return new ChoiceInputHandler<>();
    }

    public static <T> ChoiceInputHandler<T> newChoiceHandler(Choice<T>... choices) {
        return new ChoiceInputHandler<T>().addChoices(choices);
    }

    public static EntityInputHandler newEntityHandler() {
        return new EntityInputHandler();
    }

}
