package com.github.xemiru.miniboi.input;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.translation.Translatable;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * An object responsible for receiving input from a {@link Player} through a specific process.
 *
 * @param <T> the resulting type of the input received from the handler
 */
public abstract class InputHandler<T> {

    private UUID target;
    private InputManager man;
    private Function<T, List<Translatable>> consumer;
    private Consumer<CancelReason> cancel;
    private LanguageText prompt;
    private long timeout;

    public enum CancelReason {
        /**
         * The handler was cancelled due to an error.
         */
        ERROR,
        /**
         * The handler was cancelled by the target Player.
         */
        USER,
        /**
         * The handler was cancelled by a timeout.
         */
        TIMEOUT,
        /**
         * The handler was replaced by a new handler.
         */
        REPLACE,
        /**
         * The handler was cancelled by another plugin.
         */
        PLUGIN,
        /**
         * The target Player left the game.
         */
        QUIT
    }

    //
    // region subclass api
    //

    protected InputHandler() {
        this.man = null;
        this.target = null;
        this.consumer = obj -> Collections.emptyList();

        this.cancel = reason -> this.getTargetPlayer().ifPresent(p ->
            Language.msgError(Language.INPUT_GENERIC_CANCELLED).send(p));

        this.timeout = 0;
        this.prompt = null;
    }

    /**
     * @return the manager set for this {@link InputHandler}
     */
    protected final InputManager getManager() {
        return this.man;
    }

    /**
     * @return the target set for this {@link InputHandler}
     */
    protected final UUID getTarget() {
        return this.target;
    }

    /**
     * @return the {@link Player} owning the target UUID set for this {@link InputHandler}
     */
    protected final Optional<Player> getTargetPlayer() {
        return Sponge.getServer().getPlayer(this.target);
    }

    /**
     * Performs error handling using the owning {@link InputManager}'s error handling function.
     *
     * @param messages the error messages
     */
    protected final void handleError(Translatable... messages) {
        this.getTargetPlayer().ifPresent(p -> {
            if (!this.getManager().handleError(p, messages)) this.cancel(CancelReason.ERROR);
        });
    }

    /**
     * Completes the handling of this {@link InputHandler} with the provided value as the result, returning whether or
     * not the input was valid as decided by this handler's consuming function set by {@link #setConsumer(Function)}.
     *
     * <p>Note that even if this method returns false, input handling may have been cancelled anyway by the manager's
     * error handling function through a call to {@link #cancel(CancelReason)}.</p>
     *
     * @param input the result of the input operation
     * @return whether or not the input was successfully accepted
     */
    protected final boolean submit(T input) {
        if (this.man.isValid(this)) {
            List<Translatable> err = this.consumer.apply(input);
            if (!err.isEmpty()) {
                this.handleError(err.toArray(new Translatable[0]));
                return false;
            } else {
                this.man.removeHandler(this);
                this.cleanup();
                return true;
            }
        } else {
            this.getTargetPlayer().ifPresent(p -> Language.msgError(Language.INPUT_GENERIC_EXPIRED).send(p));
        }

        return true;
    }

    // endregion

    //
    // region internal api
    //

    /**
     * Assigns an {@link InputManager} to this {@link InputHandler}.
     *
     * @param man the InputManager to assign
     */
    final void withManager(InputManager man) {
        this.man = man;
    }

    /**
     * Assigns a target {@link Player}'s {@link UUID} to this {@link InputHandler}.
     *
     * @param target the target Player's UUID
     */
    final void withTarget(UUID target) {
        this.target = target;
    }

    /**
     * @return the timeout value, in milliseconds
     */
    final long getTimeout() {
        return this.timeout;
    }

    /**
     * Cancels this {@link InputHandler}'s processing with the given reason.
     *
     * @param reason the reason for cancelling
     */
    final void cancel(CancelReason reason) {
        if (this.man.isValid(this)) {
            this.man.removeHandler(this);
            this.cancel.accept(reason);
            this.cleanup();
        }
    }

    /**
     * @return the prompt text used to signal to the target {@link Player} the start of this {@link InputHandler}'s
     *     processing
     */
    final Optional<LanguageText> getPrompt() {
        return Optional.ofNullable(this.prompt);
    }

    // endregion

    //
    // implemented methods
    //

    /**
     * Called when this {@link InputHandler} has been prepared with a manager and a target.
     *
     * <p>It is at this point that the handler is allowed to interact with the target player and attempt to receive
     * input.</p>
     *
     * <p>Should any known potential errors occur, the handler should be notified through a call to the function
     * returned by {@link #handleError(Translatable[])}.</p>
     */
    protected abstract void init();

    /**
     * Called when this {@link InputHandler} should no longer try to interface with the target player for input.
     *
     * <p>This method is called regardless of whether or not the input was successful.</p>
     *
     * <p>This method should generally be reversing the setup performed in the {@link #init()} method.</p>
     */
    protected abstract void cleanup();

    //
    // public api
    //

    /**
     * Sets the consuming function of this {@link InputHandler} -- specifically, the consumer of the final input value
     * provided by the user.
     *
     * <p>The consuming function should return an empty List if the value is considered acceptable without issues.
     * Otherwise, the function should return error messages in a List describing the issues found.</p>
     *
     * @param consumer the new consuming function of this InputHandler
     * @return this InputHandler, for chaining
     */
    public InputHandler<T> setConsumer(Function<T, List<Translatable>> consumer) {
        this.consumer = requireNonNull(consumer);
        return this;
    }

    /**
     * Sets the cancel function of this {@link InputHandler}, called when this input is deliberately cancelled by
     * something.
     *
     * <p>The default cancel handler sends a very generic error message to the target player; it is recommended to
     * replace it.</p>
     *
     * @param cancel the new cancel function of this InputHandler
     * @return this InputHandler, for chaining
     */
    public InputHandler<T> setCancelHandler(Consumer<CancelReason> cancel) {
        this.cancel = requireNonNull(cancel);
        return this;
    }

    /**
     * Sets a timeout on this {@link InputHandler}, in milliseconds.
     *
     * <p>A value of 0 or lower disables the timeout. It is 0 by default.</p>
     *
     * @param timeoutMs the timeout of this InputHandler, in milliseconds
     * @return this InputHandler, for chaining
     */
    public InputHandler<T> setTimeout(long timeoutMs) {
        this.timeout = timeoutMs;
        return this;
    }

    /**
     * Sets the prompt text sent to the target {@link Player} of this {@link InputHandler}, signalling the start of this
     * handler's input processing.
     *
     * <p>While it's possible to send a prompt to a player before the call to
     * {@link InputManager#query(Player, InputHandler)}, doing so will cause messages occuring during another handler's
     * cancel function will appear <b>after</b> the sent message, since the cancelling of the previous handler set on
     * the target player will occur inside the query call and thus after the prompt message was sent. It is thus
     * preferred to let the manager send the prompt text through this method.</p>
     *
     * <p>It is up to the handler to display the prompt message through its {@link #init()} method.</p>
     *
     * @param prompt the prompt text, or null to not prompt the player
     * @return this InputHandler, for chaining
     */
    public InputHandler<T> setPrompt(LanguageText prompt) {
        this.prompt = prompt;
        return this;
    }

    /**
     * Cancels this {@link InputHandler}'s processing.
     */
    public void cancel() {
        this.cancel(CancelReason.PLUGIN);
    }
}
