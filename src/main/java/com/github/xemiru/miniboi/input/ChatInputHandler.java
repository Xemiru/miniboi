package com.github.xemiru.miniboi.input;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.command.TabCompleteEvent;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.text.translation.Translatable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * Receives input through a chat message sent by the target {@link Player}.
 *
 * @param <T> the output type of the String transformation used to get the result received from the handler
 * @see InputHandler
 */
public class ChatInputHandler<T> extends InputHandler<T> {

    private Function<String, T> transform;

    private List<String> suggestions;
    private Function<String, Optional<Translatable>> validation;

    ChatInputHandler(Function<String, T> transform) {
        this.transform = transform;
        this.suggestions = new ArrayList<>();
        this.validation = s -> Optional.empty();
    }

    @Listener
    public void onChat(MessageChannelEvent.Chat e) {
        e.getCause()
            .first(Player.class)
            .filter(it -> it.getUniqueId().equals(this.getTarget())).ifPresent(p -> {
                String input = e.getRawMessage().toPlain();
                e.setCancelled(true);

                Optional<Translatable> validation = this.validation.apply(input);
                if (validation.isPresent()) this.handleError(validation.get());
                else this.submit(this.transform.apply(input));
            }
        );
    }

    @Listener
    public void onTab(TabCompleteEvent.Chat e) {
        e.getCause()
            .first(Player.class)
            .filter(it -> it.getUniqueId().equals(this.getTarget())).ifPresent(p -> {
                String input = e.getRawMessage();
                e.getTabCompletions().clear();
                this.suggestions.stream()
                    .filter(it -> it.length() >= input.length())
                    .map(str -> str.substring(input.lastIndexOf(" ") + 1))
                    .forEach(e.getTabCompletions()::add);
            }
        );
    }

    /**
     * Sets suggestions to be made to the target Player.
     *
     * @param suggestions the new suggestions to request with
     * @return this ChatInputHandler, for chaining
     */
    public ChatInputHandler<T> withSuggestions(List<String> suggestions) {
        this.suggestions = requireNonNull(suggestions);
        return this;
    }

    /**
     * Sets a validation handler, checked upon command execution.
     *
     * <p>The function should return an empty Optional if successful; the Optional otherwise contains an error
     * message.</p>
     *
     * @param validation the new validation handler to request with
     * @return this ChatInputHandler, for chaining
     */
    public ChatInputHandler<T> withInputValidation(Function<String, Optional<Translatable>> validation) {
        this.validation = requireNonNull(validation);
        return this;
    }

    @Override
    protected void init() {
        Object pl = this.getManager().getPlugin();
        Sponge.getEventManager().registerListeners(pl, this);
        this.getPrompt().ifPresent(tx -> this.getTargetPlayer().ifPresent(tx::send));

        this.getTargetPlayer().ifPresent(p -> LanguageText.builder()
            .format(Language.FORMAT_PROMPT)
            .hover(Language.prompt(Language.INPUT_CHAT_CANCELPROMPT_HOVER))
            .action(pla -> this.cancel(CancelReason.USER))
            .append(Language.INPUT_CHAT_CANCELPROMPT)
            .build().send(p));
    }

    @Override
    protected void cleanup() {
        Sponge.getEventManager().unregisterListeners(this);
    }

}
