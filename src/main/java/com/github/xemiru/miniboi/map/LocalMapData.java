package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.util.FileUtil;
import ninja.leaping.configurate.ConfigurationNode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.zip.ZipFile;

import static java.util.Objects.requireNonNull;

/**
 * {@link MapData} loaded from a map ZIP file.
 */
public class LocalMapData extends MapData {

    private MapMeta meta;
    private Path tempDir;
    private Supplier<ZipFile> fileSupplier;
    private Map<String, ConfigurationNode> configs;

    public LocalMapData(String id, Supplier<ZipFile> fileSupplier) {
        this.fileSupplier = requireNonNull(fileSupplier);
        this.tempDir = FileUtil.reserveTempPath();
        this.setId(id);
    }

    @Override
    public MapMeta getMeta() {
        return this.meta;
    }

    @Override
    public Optional<Map<String, ConfigurationNode>> getGameConfigs() {
        return Optional.of(this.configs);
    }

    @Override
    protected boolean isSaveable() {
        return true;
    }

    @Override
    protected void init() {
        try {
            FileUtil.deleteRecursive(tempDir);
            ZipFile file = fileSupplier.get();
            FileUtil.extract(file, tempDir);
            file.close();

            this.meta = FileUtil.loadMetaFile(tempDir);
            this.configs = FileUtil.loadGameConfigs(tempDir);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to extract / read map", e);
        }
    }

    @Override
    protected void load(Path destination) {
        try {
            FileUtil.copyRecursive(this.tempDir, destination);
        } catch (IOException e) {
            throw new IllegalStateException("World load error", e);
        }
    }

    @Override
    protected void cleanup() {
        try {
            if (this.tempDir != null && Files.exists(this.tempDir)) FileUtil.deleteRecursive(this.tempDir);
        } catch (IOException ignored) {
        }
    }

}
