package com.github.xemiru.miniboi.map;

import org.spongepowered.api.world.World;
import org.spongepowered.api.world.WorldArchetype;

import java.util.function.Supplier;
import java.util.zip.ZipInputStream;

/**
 * Static class for stock {@link MapData} methods.
 */
public class MapDatas {

    /**
     * @see GeneratedMapData
     */
    public static GeneratedMapData generatedMap(WorldArchetype arch) {
        return new GeneratedMapData(arch);
    }

    /**
     * @see ImportedMapData
     */
    public static ImportedMapData importedMap(World imported) {
        return new ImportedMapData(imported);
    }

    /**
     * @see InternalMapData
     */
    public static InternalMapData internalMap(Supplier<ZipInputStream> stream) {
        return new InternalMapData(stream);
    }

}
