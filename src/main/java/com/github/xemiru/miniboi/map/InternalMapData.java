package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.util.FileUtil;
import ninja.leaping.configurate.ConfigurationNode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.zip.ZipInputStream;

import static java.util.Objects.requireNonNull;

/**
 * {@link MapData} loaded from an internally-stored map ZIP file.
 */
public class InternalMapData extends MapData {

    private MapMeta meta;
    private Path tempDir = null;
    private Map<String, ConfigurationNode> configs;
    private Supplier<ZipInputStream> streamSupplier;

    public InternalMapData(Supplier<ZipInputStream> streamSupplier) {
        this.streamSupplier = requireNonNull(streamSupplier);
    }

    @Override
    public MapMeta getMeta() {
        return this.meta;
    }

    @Override
    public Optional<Map<String, ConfigurationNode>> getGameConfigs() {
        return Optional.of(this.configs);
    }

    @Override
    protected boolean isSaveable() {
        return false;
    }

    @Override
    protected void init() {
        // never needs to reload; an internal source should never change
        if(this.tempDir != null) return;

        try {
            this.tempDir = FileUtil.reserveTempPath();
            ZipInputStream s = streamSupplier.get();
            FileUtil.extract(s, tempDir);
            s.close();

            this.meta = FileUtil.loadMetaFile(tempDir);
            this.configs = FileUtil.loadGameConfigs(tempDir);

            this.setId(null);
        } catch (IOException e) {
            throw new IllegalStateException("Failed to extract internal ZIP archive", e);
        }
    }

    @Override
    protected void load(Path destination) {
        try {
            FileUtil.copyRecursive(this.tempDir, destination);
        } catch (IOException e) {
            throw new IllegalStateException("World load error", e);
        }
    }

    @Override
    protected void cleanup() {
        try {
            if (this.tempDir != null && Files.exists(this.tempDir)) FileUtil.deleteRecursive(this.tempDir);
        } catch (IOException ignored) {
        }
    }
}
