package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.util.Exceptions;
import com.github.xemiru.miniboi.util.FileUtil;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.World;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * {@link MapData} created from a pre-existing world.
 */
public class ImportedMapData extends MapData {

    private World world;
    private MapMeta meta;
    private Path tempDir;
    private Map<String, ConfigurationNode> configs = new HashMap<>();

    public ImportedMapData(World imported) {
        this.world = requireNonNull(imported);
        this.meta = new MapMeta();
    }

    @Override
    public MapMeta getMeta() {
        return this.meta;
    }

    @Override
    public Optional<Map<String, ConfigurationNode>> getGameConfigs() {
        return Optional.of(configs);
    }

    @Override
    protected boolean isSaveable() {
        return true;
    }

    @Override
    protected void init() {
        try {
            if (this.world != null) {
                if (Sponge.getServer().getWorlds().contains(this.world)) {
                    Path worldDir = this.world.getDirectory();
                    this.tempDir = FileUtil.reserveTempPath();

                    Arrays.asList(
                        worldDir.resolve("region"),
                        worldDir.resolve("data"),
                        worldDir.resolve("level.dat")).forEach(path -> {
                        try {
                            FileUtil.copyRecursive(path, tempDir.resolve(worldDir.relativize(path)));
                        } catch (Exception e) {
                            Exceptions.sneakyThrow(e);
                        }
                    });

                    this.world = null;
                } else throw new IllegalStateException("World is no longer available");
            }
        } catch (Exception e) {
            this.world = null;
            throw new IllegalStateException("World import error", e);
        }
    }

    @Override
    protected void load(Path destination) {
        try {
            FileUtil.copyRecursive(this.tempDir, destination);
        } catch (IOException e) {
            throw new IllegalStateException("World load error", e);
        }
    }

    @Override
    protected void cleanup() {
        try {
            FileUtil.deleteRecursive(this.tempDir);
        } catch (Exception e) {
            Exceptions.sneakyThrow(e);
        }
    }

}
