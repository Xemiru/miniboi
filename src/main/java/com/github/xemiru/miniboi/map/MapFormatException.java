package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.language.LiteralTranslatable;

public class MapFormatException extends MapException {

    public MapFormatException(String msg) {
        super(LiteralTranslatable.of(msg));
    }

    public MapFormatException(Throwable cause) {
        super(cause);
    }

    public MapFormatException(String msg, Throwable cause) {
        super(LiteralTranslatable.of(msg), cause);
    }
}
