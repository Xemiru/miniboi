package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.Miniboi;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.util.Exceptions;
import com.github.xemiru.miniboi.util.FileUtil;
import org.lanternpowered.nbt.CompoundTag;
import org.lanternpowered.nbt.StringTag;
import org.lanternpowered.nbt.io.NbtTagInputStream;
import org.lanternpowered.nbt.io.NbtTagOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.world.UnloadWorldEvent;
import org.spongepowered.api.world.SerializationBehaviors;
import org.spongepowered.api.world.World;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Works together with an implemented {@link MapHandler} to interface with the reading and writing of maps.
 */
public class MapManager {

    /**
     * Denotes the current state of a {@link ManagedMap} while under the control of a {@link MapManager}.
     */
    private enum MapState {
        /** The map is currently being saved. */
        SAVING,
        /** The map is being loaded. */
        LOADING,
        /** The map is being deleted. */
        DELETING,
        /** Nothing is reading or writing with the map. */
        IDLE
    }

    /**
     * Container class for {@link MapData} with state-based locking to prevent operations on a single MapData instance
     * from conflicting.
     */
    private class ManagedMap {

        private ReentrantLock lock = new ReentrantLock(true);
        private AtomicReference<MapData> data = new AtomicReference<>();
        private AtomicReference<MapState> state = new AtomicReference<>(MapState.IDLE);

        ManagedMap(MapData data) {
            this.data.set(data);
        }

        /**
         * @return the current state of use of the underlying {@link MapData}
         */
        MapState getState() {
            return this.state.get();
        }

        /**
         * Returns the {@link MapData} instance managed by this {@link ManagedMap}.
         *
         * <p>The held MapData instance can change when maps are reloaded, as the locks are tied to the data ID and not
         * the MapData instance.</p>
         *
         * @return the MapData instance managed by this ManagedMap
         */
        MapData getData() {
            return this.data.get();
        }

        /**
         * Sets the {@link MapData} instance managed by this {@link ManagedMap}.
         *
         * @param data the new MapData
         */
        void setData(MapData data) {
            this.data.set(data);
        }

        /**
         * Locks this {@link ManagedMap} under the provided state.
         *
         * @param state the {@link MapState} to lock with
         * @see Lock#lock()
         */
        void lock(MapState state) {
            lock.lock();
            this.state.set(state);
        }

        /**
         * Unlocks this {@link ManagedMap}.
         *
         * @see Lock#unlock()
         */
        void unlock() {
            if (!lock.isHeldByCurrentThread()) throw new IllegalMonitorStateException();
            this.state.set(MapState.IDLE);
            lock.unlock();
        }

    }

    private static final Logger LOG = LoggerFactory.getLogger(MapManager.class);

    public static final String MB_NAME_PREFIX = "_MINIBOI_";
    public static final String MB_MARKER_NAME = "miniboi.marker";

    private Miniboi miniboi;
    private MapHandler<? extends MapData> handler;
    private Path mapDir = Sponge.getGame().getGameDirectory().resolve(Sponge.getServer().getDefaultWorldName());

    private ReadWriteLock storeLock = new ReentrantReadWriteLock(true);
    private Map<String, ManagedMap> maps = new ConcurrentHashMap<>();

    private Map<World, GameMap> loaded = new WeakHashMap<>();
    private Set<GameMap> saving = Collections.newSetFromMap(new ConcurrentHashMap<>());

    public MapManager(Miniboi miniboi, MapHandler<? extends MapData> mapHan) {
        this.handler = mapHan;
        this.miniboi = miniboi;
        Sponge.getEventManager().registerListeners(miniboi, this);
        this.reloadKnownMaps().join();

        try {
            this.cleanMaps();
        } catch (Exception e) {
            LOG.warn("Map manager couldn't clean up previous map data. This may cause the plugin to eventually hit a disk space limit in subsequent runs.", e);
        }
    }

    /**
     * Returns the {@link MapData} identified by the provided ID.
     *
     * <p>MapData instances returned from this method should never be cached, as MapData instances get <b>replaced, not
     * updated.</b> Specifically, <b>the returned MapData instance may eventually become stale without notice.</b></p>
     *
     * @param id the ID of the MapData to load
     * @return the MapData?
     */
    public CompletableFuture<Optional<MapData>> getKnownMap(String id) {
        return CompletableFuture.supplyAsync(() -> {
            this.storeLock.readLock().lock();

            try {
                return Optional.ofNullable(this.maps.get(id).getData());
            } finally {
                this.storeLock.readLock().unlock();
            }
        });
    }

    /**
     * Returns the list of known map IDs harvested from the current {@link MapHandler}.
     *
     * <p>This list is initialized once on load, and can be reinitialized by a call to {@link #reloadKnownMaps()}.
     *
     * <p>MapData instances returned from this method should never be cached, as MapData instances get <b>replaced, not
     * updated.</b> Specifically, <b>the returned MapData instance may eventually become stale without notice.</b></p>
     *
     * <p>Should MapData from the collection returned by this method need to be used, it is recommended to retrieve the
     * latest variation of it through {@link #getKnownMap(String)} if it is not immediately used. Receiving an empty
     * Optional from the former signifies that the map no longer exists to the manager and that the action to perform
     * should be cancelled.</p>
     *
     * @return a collection of MapData IDs
     */
    public CompletableFuture<Collection<MapData>> getKnownMaps() {
        return CompletableFuture.supplyAsync(() -> {
            this.storeLock.readLock().lock();

            try {
                List<MapData> maps = new ArrayList<>();
                this.maps.values().forEach(mm -> maps.add(mm.getData()));
                return Collections.unmodifiableCollection(maps);
            } finally {
                this.storeLock.readLock().unlock();
            }
        });
    }

    /**
     * Requests a complete refresh of the list of known maps from the current {@link MapHandler}.
     */
    public CompletableFuture<Void> reloadKnownMaps() {
        return this.handler.loadMaps().thenAccept(it -> {
            this.storeLock.writeLock().lock();

            try {
                Map<String, MapData> maps = new HashMap<>();

                try {
                    for (MapData md : it) {
                        try {
                            md.internalInit();
                            maps.put(md.getId().orElseThrow(()
                                -> new MapException(Language.MAPMAN_ERROR_MAPINVALID_ID)), md);
                        } catch (Exception e) {
                            LOG.warn(String.format("Failed to initialize map with ID %s; it can't be loaded.",
                                md.getId().orElse("<no id>")), e);
                        }
                    }

                    // clean up old maps before storing new ones
                    // don't need to lock map here as everything should be locked down by reload flag
                    this.maps.values().forEach(map -> this.safeCleanup(map.getData()));

                    // store new maps without replacing the locks
                    for (String key : maps.keySet()) {
                        MapData data = maps.get(key);
                        if (this.maps.containsKey(key)) this.maps.get(key).setData(data);
                        else this.maps.put(key, new ManagedMap(data));
                    }

                    // remove old maps
                    Iterator<Map.Entry<String, ManagedMap>> iter = this.maps.entrySet().iterator();
                    iter.forEachRemaining(entry -> {
                        if (!maps.containsKey(entry.getKey())) iter.remove();
                    });
                } catch (Exception e) {
                    maps.values().forEach(this::safeCleanup);
                    throw e;
                }
            } finally {
                this.storeLock.writeLock().unlock();
            }
        });
    }

    /**
     * Attempts to load a known {@link GameMap} by ID.
     *
     * <p>This loading method is strictly for maps known by this {@link MapManager} (as returned by
     * {@link #getKnownMaps()}).</p>
     *
     * <p>Loading will fail exceptionally if the map owning the provided ID is currently being deleted as a result of
     * a call to {@link #delete(String)}.</p>
     *
     * @param id the ID of the map to load
     * @return the loaded GameMap
     */
    public CompletableFuture<GameMap> load(String id) {
        return CompletableFuture.supplyAsync(() -> {
            this.storeLock.readLock().lock(); // receiving read lock guarantees we wait for reload/save/delete
            try {
                ManagedMap map = this.maps.get(id);
                if (map == null) throw new MapException(Language.MAPMAN_ERROR_UNKNOWNMAP);
                if (map.getState() == MapState.DELETING)
                    throw new MapException(Language.MAPMAN_LOAD_ERROR_DELETING);

                map.lock(MapState.LOADING);
                try {
                    return this.internalLoad(map.getData()).join();
                } finally {
                    map.unlock();
                }
            } finally {
                this.storeLock.readLock().unlock();
            }
        });
    }

    /**
     * Attempts to load a {@link GameMap} from the provided {@link MapData}.
     *
     * <p>This loading method is strictly for maps not known by this {@link MapManager}. Attempting to pass a MapData
     * instance that holds an ID will raise an exception.</p>
     *
     * @param map the MapData to load from
     * @return the loaded GameMap
     *
     * @throws IllegalArgumentException if the provided MapData has an ID
     */
    public CompletableFuture<GameMap> load(MapData map) {
        if (map.getId().isPresent())
            throw new IllegalArgumentException("Maps with an ID are assumed to be known by the handler and should be loaded through the other load method.");
        return this.internalLoad(map);
    }

    /**
     * Attempts to save the provided {@link GameMap} to the storage unit of the current {@link MapHandler}.
     *
     * @param map the map to save
     */
    public CompletableFuture<Void> save(GameMap map) {
        boolean unlock = map.getId().isPresent();
        return CompletableFuture.runAsync(() -> {
            synchronized (this) { // sync so the map doesnt get modified by another save attempt
                if (this.saving.contains(map)) throw new MapException(Language.MAPMAN_SAVE_ERROR_ALREADY);
                if (!map.isSaveable()) throw new MapException(Language.MAPMAN_SAVE_ERROR_UNSAVEABLE);
                if (!map.getMeta().isValid()) throw new MapException(Language.MAPMAN_ERROR_MAPINVALID_META);

                this.saving.add(map);
            }

            map.getId().map(this.maps::get).ifPresent(d -> d.lock(MapState.SAVING));
        }).thenRun(() -> {
            // save world on main thread
            CompletableFuture<Void> saveWorld = new CompletableFuture<>();
            Sponge.getScheduler().createTaskBuilder()
                .execute(() -> {
                    try {
                        // save and disable external save
                        map.getWorld().setSerializationBehavior(SerializationBehaviors.MANUAL);
                        map.getWorld().save();
                        map.getWorld().setSerializationBehavior(SerializationBehaviors.NONE);

                        saveWorld.complete(null);
                    } catch (Exception e) {
                        saveWorld.completeExceptionally(e);
                    }
                })
                .submit(this.miniboi);

            saveWorld.join();
        }).thenApply(v -> this.handler.save(map).join()).thenAccept(md -> {
            try {
                MapData previous = null;
                String id = md.getId().orElseThrow(() -> new MapException(Language.MAPMAN_SAVE_ERROR_HANDLER));
                map.setId(id);

                md.internalInit(); // this must happen first, if exception happens then the rest doesn't
                this.storeLock.writeLock().lock(); // queues if reloading, can't conflict with delete

                try {
                    if (this.maps.containsKey(id)) {
                        ManagedMap data = this.maps.get(id);
                        previous = data.getData();
                        data.setData(md);
                    } else this.maps.put(id, new ManagedMap(md));
                } finally {
                    this.saving.remove(map);
                    Sponge.getScheduler().createTaskBuilder()
                        .execute(() -> map.getWorld().setSerializationBehavior(SerializationBehaviors.AUTOMATIC))
                        .submit(this.miniboi);

                    this.storeLock.writeLock().unlock();
                }

                if (previous != null) this.safeCleanup(previous);
            } catch (Exception e) {
                Exceptions.sneakyThrow(e);
            }
        }).whenComplete((v, err) -> {
            if (unlock) map.getId().map(this.maps::get).ifPresent(ManagedMap::unlock);
        });
    }

    /**
     * Deletes the known {@link MapData} with the provided ID's relevant files from the current {@link MapHandler}'s
     * storage unit and from memory.
     *
     * @param id the ID of the data to delete
     */
    public CompletableFuture<Void> delete(String id) {
        return CompletableFuture.supplyAsync(() -> {
            this.storeLock.writeLock().lock(); // cant conflict with save / reload

            ManagedMap data = this.maps.get(id);
            if (data == null) throw new MapException(Language.MAPMAN_ERROR_UNKNOWNMAP);

            data.lock(MapState.DELETING);
            this.maps.remove(id);
            return data;
        }).thenAccept(data -> {
            try {
                this.handler.delete(data.getData()).join();
            } finally {
                data.unlock();
            }
        }).whenComplete((v, err) -> this.storeLock.writeLock().unlock());
    }

    /**
     * Unloads the provided {@link GameMap}'s world from Sponge and this {@link MapManager}.
     *
     * @param map the map to unload
     */
    public CompletableFuture<Void> unload(GameMap map) {
        CompletableFuture<Void> future = new CompletableFuture<>();

        // unload world from main thread first
        Sponge.getScheduler().createTaskBuilder().execute(() -> {
            try {
                this.loaded.remove(map.getWorld());
                if (map.getWorld().isLoaded()) {
                    if (!Sponge.getServer().unloadWorld(map.getWorld()))
                        throw new MapException(Language.MAPMAN_UNLOAD_ERROR_SPONGEFAIL);
                }

                future.complete(null);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        }).submit(this.miniboi);

        return future.thenRunAsync(() -> {
            try {
                Thread.sleep(1000); // give some small time to fully unload and release the world
                if (!Sponge.getServer().deleteWorld(map.getWorld().getProperties()).join())
                    throw new MapException(Language.MAPMAN_UNLOAD_ERROR_SPONGEFAIL2);
            } catch (MapException ex) {
                throw ex;
            } catch (Exception ex) {
                throw new MapException(Language.MAPMAN_UNLOAD_ERROR_DELETE, ex);
            }
        });
    }

    /**
     * Returns the {@link GameMap} instance that wraps the provided {@link World}.
     *
     * @param world the world to query with
     * @return the owning GameMap?
     */
    public Optional<GameMap> getMapOfWorld(World world) {
        return Optional.ofNullable(this.loaded.get(world));
    }

    //
    // events
    // #####
    //

    @Listener
    public void onUnload(UnloadWorldEvent e) {
        this.getMapOfWorld(e.getTargetWorld()).ifPresent(this::unload);
    }

    //
    // private methods
    // #####
    //

    /**
     * Internal method for loading {@link MapData} as new {@link GameMap}s.
     *
     * @param map the MapData to load
     * @return a GameMap
     */
    private CompletableFuture<GameMap> internalLoad(MapData map) {
        final CompletableFuture<GameMap> future = new CompletableFuture<>();
        ForkJoinPool.commonPool().execute(() -> {
            try {
                map.internalInit();
                if (!map.isValid()) throw new MapException(Language.MAPMAN_LOAD_ERROR_INVALID);

                // load and set up world directory async
                String worldName = FileUtil.getUnusedVariant(MB_NAME_PREFIX,
                    candidate -> !Files.exists(this.mapDir.resolve(candidate)))
                    .orElseThrow(() -> new MapException(Language.MAPMAN_LOAD_ERROR_DIRECTORYFAIL));
                Path worldDir = this.mapDir.resolve(worldName);
                map.load(worldDir);

                // change level name in nbt
                Path levelDat = worldDir.resolve("level.dat");
                try (NbtTagInputStream nis = new NbtTagInputStream(new GZIPInputStream(Files.newInputStream(levelDat)))) {
                    CompoundTag root = ((CompoundTag) nis.read());
                    ((StringTag) ((CompoundTag) root.get("Data")).get("LevelName")).set(worldName);

                    try (NbtTagOutputStream nos = new NbtTagOutputStream(new GZIPOutputStream(Files.newOutputStream(levelDat)))) {
                        nos.write(root);
                        nos.flush();
                    }
                } catch (Exception e) {
                    Exceptions.sneakyThrow(e);
                }

                // place marker file
                try {
                    Path markerFile = worldDir.resolve(MB_MARKER_NAME);
                    if (Files.notExists(markerFile)) Files.createFile(markerFile);
                } catch (Exception e) {
                    Exceptions.sneakyThrow(e);
                }

                // load world on main thread
                Sponge.getScheduler().createTaskBuilder().execute(() -> {
                    try {
                        World world = Sponge.getServer().loadWorld(worldName)
                            .orElseThrow(() -> new MapException(Language.MAPMAN_LOAD_ERROR_SPONGEFAIL));
                        GameMap loaded = new GameMap(world, map);

                        loaded.getWorld().setSerializationBehavior(SerializationBehaviors.MANUAL);
                        this.loaded.put(world, loaded);
                        future.complete(loaded);
                    } catch (Exception e) {
                        future.completeExceptionally(e);
                    }
                }).submit(this.miniboi);
            } catch (Exception e) {
                future.completeExceptionally(e);
            }
        });

        return future;
    }

    /**
     * Safely cleans and invalidates the provided {@link MapData}, logging any errors that occur.
     *
     * @param data the MapData to clean up
     */
    private void safeCleanup(MapData data) {
        try {
            data.internalCleanup();
        } catch (Exception e) {
            LOG.warn(String.format("Failed to clean up map data with the ID %s", data.getId().orElse("<no id>")), e);
        }
    }

    /**
     * Clean folders within the maps directory that contain the marker file.
     */
    public void cleanMaps() {
        try {
            if (Files.exists(this.mapDir)) {
                Files.list(this.mapDir).forEach(path -> {
                    try {
                        if (Files.exists(path.resolve(MB_MARKER_NAME))) FileUtil.deleteRecursive(path);
                    } catch (Exception e) {
                        LOG.warn(String.format("Map manager couldn't delete temporary map directory at %s", path.toAbsolutePath()), e);
                    }
                });
            }
        } catch (Exception e) {
            Exceptions.sneakyThrow(e);
        }
    }
}
