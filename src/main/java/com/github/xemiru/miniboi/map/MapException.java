package com.github.xemiru.miniboi.map;

import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.text.translation.locale.Locales;

import java.util.Optional;

public class MapException extends RuntimeException {

    private Translatable msg;

    public MapException(Translatable message) {
        super(message.getTranslation().get(Locales.EN_US));
        this.msg = message;
    }

    public MapException(Throwable cause) {
        super(cause);
    }

    public MapException(Translatable message, Throwable cause) {
        super(message.getTranslation().get(Locales.EN_US), cause);
        this.msg = message;
    }

    public Optional<Translatable> getTranslatableMessage() {
        return Optional.ofNullable(msg);
    }

}
