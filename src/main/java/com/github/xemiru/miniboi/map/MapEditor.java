package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.Miniboi;
import com.github.xemiru.miniboi.input.InputHandler;
import com.github.xemiru.miniboi.input.InputHandlers;
import com.github.xemiru.miniboi.language.ComposedTranslatable;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.language.LiteralTranslatable;
import com.github.xemiru.miniboi.textmenu.PageDisplayable;
import com.github.xemiru.miniboi.textmenu.TextMenu;
import com.github.xemiru.miniboi.util.Choice;
import com.github.xemiru.miniboi.util.Exceptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

/**
 * The map editor interface.
 */
public class MapEditor implements PageDisplayable {

    /**
     * The possible states of a map in the viewpoint of the map editor.
     */
    private enum MapStatus {
        /** The map is active and being edited. */
        EDITING,
        /** The map is currently being loaded. */
        LOADING,
        /** The map is currently being saved. */
        SAVING,
        /** The map is currently being deleted. */
        DELETING,
        /** The map is currently being unloaded. */
        UNLOADING,
        /** The map has been unloaded. */
        UNLOADED,
        /** The map is in an error state and shouldn't be interfaced with. */
        ERRORED
    }

    /**
     * Containing interface for maps loaded by the map editor.
     */
    private class EditedMap implements PageDisplayable {

        private GameMap map;
        private UUID editorUid;
        private MapStatus status;
        private MessageChannel channel = MessageChannel.TO_NONE;
        private TextMenu menu = TextMenu.builder()
            .setHeader(LanguageText.of(Language.MAPEDITOR_PANEL_LABEL, ": ", this.getMap()
                .map(GameMap::getMeta).flatMap(MapMeta::getName)
                .<Translatable>map(LiteralTranslatable::of).orElse(Language.MAPMETA_NAME_NOVALUE)))
            .configureBody(body -> body
                .addDynamicElements((player, page) -> {
                    List<LanguageText> elements = new ArrayList<>();
                    elements.addAll(MapEditor.this.generatePanel(MapEditor.this, this));
                    return elements;
                }))
            .build();

        /**
         * Instantiates an {@link EditedMap} with a {@link GameMap} in the process of loading.
         *
         * @param editor the {@link Player} opening the map
         * @param map a {@link CompletableFuture} promising a GameMap
         */
        EditedMap(Player editor, CompletableFuture<GameMap> map) {
            this.map = null;
            this.status = MapStatus.LOADING;
            this.editorUid = editor.getUniqueId();

            map.whenComplete((gm, err) -> {
                synchronized (this) {
                    this.map = gm;
                    this.status = gm == null ? MapStatus.ERRORED : MapStatus.EDITING;

                    if (gm != null) {
                        this.channel = MessageChannel.world(gm.getWorld());
                        MapEditor.this.mapRef.put(gm, this);
                    }
                }
            });
        }

        /**
         * Instantiates an {@link EditedMap} with an already-loaded {@link GameMap}.
         *
         * @param editor the {@link Player} opening the map
         * @param map the preloaded GameMap
         */
        EditedMap(Player editor, GameMap map) {
            this.map = requireNonNull(map);
            this.status = MapStatus.EDITING;
            this.editorUid = editor.getUniqueId();
            this.channel = MessageChannel.world(map.getWorld());
        }

        /**
         * @return the {@link User} who opened the map, if found
         */
        Optional<User> getRequestingEditor() {
            return Sponge.getServiceManager().provide(UserStorageService.class).flatMap(s -> s.get(this.editorUid));
        }

        /**
         * @return the {@link MessageChannel} involving all {@link Player}s currently within the map
         */
        MessageChannel getChannel() {
            return this.channel;
        }

        /**
         * @return the {@link MapStatus} of this {@link EditedMap}
         */
        synchronized MapStatus getStatus() {
            return this.status;
        }

        /**
         * Set the {@link MapStatus} of this {@link EditedMap}.
         *
         * @param status the new status
         */
        synchronized void setStatus(MapStatus status) {
            this.status = requireNonNull(status);
        }

        /**
         * @return the {@link GameMap} of this map?
         */
        synchronized Optional<GameMap> getMap() {
            return Optional.ofNullable(this.map);
        }

        /**
         * Saves this {@link EditedMap}.
         */
        synchronized CompletableFuture<Void> save() {
            try {
                this.checkStatus();
                this.setStatus(MapStatus.SAVING);

                boolean newMap = !this.map.getId().isPresent();
                return MapEditor.this.mapMan.save(this.map).whenComplete((v, err) -> {
                    synchronized (this) {
                        // error state doesn't matter here
                        // move to recognized map list if the map was new
                        if (newMap) map.getId().ifPresent(id -> {
                            MapEditor.this.newMaps.remove(this);
                            MapEditor.this.knownMaps.put(id, this);
                        });

                        this.setStatus(MapStatus.EDITING);
                    }
                });
            } catch (Exception e) {
                CompletableFuture<Void> ex = new CompletableFuture<>();
                ex.completeExceptionally(e);

                return ex;
            }
        }

        /**
         * Deletes this {@link EditedMap}.
         */
        synchronized CompletableFuture<Void> delete() {
            try {
                this.checkStatus();
                this.setStatus(MapStatus.DELETING);
                this.removePlayers();

                CompletableFuture<Void> proc;
                if (map.getId().isPresent()) proc = MapEditor.this.mapMan.delete(this.map.getId().get());
                else proc = MapEditor.this.mapMan.unload(this.map);

                return this.finalizeUnload(proc);
            } catch (Exception e) {
                CompletableFuture<Void> ex = new CompletableFuture<>();
                ex.completeExceptionally(e);

                return ex;
            }
        }

        /**
         * Unloads this {@link EditedMap}.
         *
         * @param save whether the map should be saved before unloading
         */
        synchronized CompletableFuture<Void> unload(boolean save) {
            try {
                this.checkStatus();
                this.setStatus(MapStatus.UNLOADING);
                this.removePlayers();

                if (save) return this.finalizeUnload(MapEditor.this.mapMan.save(this.map)
                    .thenCompose(v -> MapEditor.this.mapMan.unload(this.map)));
                else return this.finalizeUnload(MapEditor.this.mapMan.unload(this.map));
            } catch (Exception e) {
                CompletableFuture<Void> ex = new CompletableFuture<>();
                ex.completeExceptionally(e);

                return ex;
            }
        }

        /**
         * Ensures this {@link EditedMap} is in a proper state before being interfaced with.
         */
        private void checkStatus() {
            switch (this.getStatus()) {
                case SAVING:
                    throw new MapException(Language.MAPEDITOR_STATUSERROR_SAVE);
                case DELETING:
                    throw new MapException(Language.MAPEDITOR_STATUSERROR_DELETE);
                case UNLOADING:
                case UNLOADED:
                    throw new MapException(Language.MAPEDITOR_STATUSERROR_UNLOAD);
                case ERRORED:
                    throw new MapException(Language.MAPEDITOR_STATUSERROR_ERROR);
            }
        }

        /**
         * Correctly cleans up this {@link MapEditor} after a process that unloads the map.
         *
         * @param process the process unloading the map
         */
        private CompletableFuture<Void> finalizeUnload(CompletableFuture<Void> process) {
            return process.whenComplete((v, e) -> {
                synchronized (this) {
                    if (e != null) this.setStatus(MapStatus.ERRORED);
                    else this.setStatus(MapStatus.UNLOADED);

                    this.getMap().flatMap(GameMap::getId).ifPresent(MapEditor.this.knownMaps::remove);
                    MapEditor.this.mapRef.remove(this.map);
                    MapEditor.this.newMaps.remove(this);
                    this.map = null;
                }
            });
        }

        /**
         * Removes any {@link Player}s currently within this {@link EditedMap}.
         */
        private void removePlayers() {
            String defaultWorld = Sponge.getServer().getDefaultWorldName();
            Location<World> loc = Sponge.getServer().getWorld(defaultWorld)
                .orElseThrow(() -> new MapException(Language.MAPEDITOR_REMOVAL_ERROR_NODEFAULT))
                .getSpawnLocation();
            World gameWorld = this.getMap().map(GameMap::getWorld)
                .orElseThrow(() -> new MapException(Language.MAPEDITOR_REMOVAL_ERROR_NOWORLD));

            gameWorld.getPlayers().forEach(p -> {
                p.setLocation(loc);
                Language.msgInfo(Language.MAPEDITOR_REMOVAL_NOTICE).send(p);
            });
        }

        @Override
        public void display(PageDisplayable parent, Player to, int page) {
            this.menu.display(parent, to, page);
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(MapEditor.class);

    private TextMenu menu;
    private Miniboi miniboi;
    private MapManager mapMan;

    private Map<GameMap, EditedMap> mapRef = new WeakHashMap<>();
    private Map<String, EditedMap> knownMaps = new HashMap<>();
    private Set<EditedMap> newMaps = new HashSet<>();

    public MapEditor(Miniboi miniboi, MapManager mapMan) {
        this.mapMan = mapMan;
        this.miniboi = miniboi;
        this.menu = TextMenu.builder()
            .setHeader(LanguageText.of(Language.MAPEDITOR_LABEL))
            .configureBody(body -> body
                .addDynamicElements((player, page) ->
                    // panel for the current map, if any
                    this.mapMan.getMapOfWorld(player.getWorld()).map(this.mapRef::get).map(map -> {
                        List<LanguageText> elements = new ArrayList<>();
                        elements.add(Language.subtitle(Language.MAPEDITOR_PANEL_LABEL, ": ", map.getMap()
                            .map(GameMap::getMeta).flatMap(MapMeta::getName)
                            .<Translatable>map(LiteralTranslatable::of).orElse(Language.MAPMETA_NAME_NOVALUE)));

                        elements.addAll(this.generatePanel(this, map));
                        return elements;
                    }).orElse(Collections.emptyList()))
                .addDynamicElements((player, page) -> {
                    // list of currently open maps, if any
                    if (this.mapRef.isEmpty()) return Collections.emptyList();

                    List<LanguageText> elements = new ArrayList<>();
                    elements.add(Language.subtitle(Language.MAPEDITOR_LIST_ACTIVE_LABEL));

                    Consumer<EditedMap> func = map -> elements.add(LanguageText.builder()
                        .format(Language.FORMAT_ACTIVE)
                        .hover(Language.hover(Language.MAPEDITOR_LIST_ACTIVE_EDIT_HOVER, Language.MAPEDITOR_LIST_ACTIVE_EDIT_DESC))
                        .action(p -> map.getMap().map(GameMap::getWorld).ifPresent(w -> p.setLocation(w.getSpawnLocation())))
                        .append(Language.MAPEDITOR_LIST_ACTIVE_EDIT_BUTTON)

                        .reset()
                        .append(" ")

                        .format(Language.FORMAT_ACTIVE)
                        .hover(Language.hover(Language.MAPEDITOR_LIST_ACTIVE_PANEL_HOVER, Language.MAPEDITOR_LIST_ACTIVE_PANEL_DESC))
                        .action(p -> map.display(this, p, 0))
                        .append(Language.MAPEDITOR_LIST_ACTIVE_PANEL_BUTTON)

                        .reset()
                        .append(" ")

                        .format(Language.FORMAT_PROMPT)
                        .hover(map.getMap().map(GameMap::getMeta).map(meta -> LanguageText.builder(Language.MAX_WRAP_LENGTH)
                            .append(meta.getName().orElse(Language.MAPMETA_NAME_NOVALUE.get(player.getLocale())), " ")
                            .append(TextColors.GRAY, Language.MAPEDITOR_LIST_ACTIVE_OPENEDBY)
                            .append(TextColors.YELLOW, " ", map.getRequestingEditor().map(User::getName)
                                .orElse(ComposedTranslatable.of("<", Language.MAPEDITOR_LIST_ACTIVE_OPENEDBY_UNKNOWN, ">")
                                    .get(player.getLocale())))
                            .line().append(meta.generateDescription())
                            .build()).orElse(null))
                        .append(map.getMap().map(GameMap::getMeta).flatMap(MapMeta::getName)
                            .<Translatable>map(LiteralTranslatable::of).orElse(Language.MAPMETA_NAME_NOVALUE))
                        .build());

                    this.newMaps.forEach(func);
                    this.knownMaps.values().forEach(func);

                    return elements;
                })
                .addDynamicElements((player, page) -> {
                    List<LanguageText> elements = new ArrayList<>();
                    elements.add(Language.subtitle(Language.MAPEDITOR_LIST_STORE_LABEL));

                    try {
                        Collection<MapData> maps = this.mapMan.getKnownMaps().get(1000, TimeUnit.MILLISECONDS);

                        if (maps.isEmpty()) elements.add(Language.info(Language.MAPEDITOR_LIST_STORE_EMPTY));
                        else maps.forEach(map -> {
                            if (!map.getId().isPresent()) return;
                            String id = map.getId().get();
                            boolean present = this.knownMaps.containsKey(id);
                            boolean active = present && this.knownMaps.get(id).getStatus() == MapStatus.EDITING;
                            Translatable mapName = map.getMeta().getName()
                                .<Translatable>map(LiteralTranslatable::of).orElse(Language.MAPMETA_NAME_NOVALUE);

                            elements.add(LanguageText.builder()
                                .format(present ? active ? Language.FORMAT_ACTIVE : Language.FORMAT_INACTIVE : Language.FORMAT_ACTIVE)
                                .hover(Language.hover(Language.MAPEDITOR_LIST_STORE_EDIT_HOVER, present ? active ? Language.MAPEDITOR_LIST_STORE_EDIT_DESC_EXISTING
                                    : Language.MAPEDITOR_LIST_STORE_EDIT_DESC_BUSY : Language.MAPEDITOR_LIST_STORE_EDIT_DESC_NEW))
                                .action(p -> {
                                    if (present)
                                        if (active) this.knownMaps.get(id).getMap()
                                            .map(GameMap::getWorld)
                                            .ifPresent(w -> p.setLocation(w.getSpawnLocation()));
                                        else Language.msgError(Language.MAPEDITOR_LIST_STORE_EDIT_ERROR_BUSY).send(p);
                                    else {
                                        Language.msgInfo(Language.MAPEDITOR_LIST_STORE_EDIT_NOTICE).send(p);
                                        this.knownMaps.put(id, new EditedMap(p, this.mapMan.load(id)
                                            .handle((gm, err) -> {
                                                this.onMain(() -> {
                                                    if (err != null) {
                                                        if (Exceptions.displayError(p, err, Language.MAPEDITOR_LIST_STORE_EDIT_ERROR_FAILURE))
                                                            LOG.error(String.format("Couldn't load map with id %s from storage for editing", id), err);
                                                        this.knownMaps.remove(id);
                                                    } else {
                                                        Language.msgInfo(Language.MAPEDITOR_LIST_STORE_EDIT_SUCCESS).send(p);
                                                        p.setLocation(gm.getWorld().getSpawnLocation());
                                                    }
                                                });

                                                return gm;
                                            })));
                                    }
                                })
                                .append(Language.MAPEDITOR_LIST_STORE_EDIT_BUTTON)

                                .reset()
                                .append(" ")
                                .format(Language.FORMAT_ACTIVE)
                                .hover(Language.hover(present ? Language.MAPEDITOR_LIST_STORE_PANEL_HOVER : Language.MAPEDITOR_LIST_STORE_DELETE_HOVER,
                                    present ? Language.MAPEDITOR_LIST_STORE_PANEL_DESC : Language.MAPEDITOR_LIST_STORE_DELETE_DESC))
                                .action(p -> {
                                    if (present) this.knownMaps.get(id).display(this, p, 0);
                                    else this.miniboi.getInputManager().query(p,
                                        InputHandlers.newChoiceHandler(
                                            Choice.of(true, Language.GENERIC_YES, Language.MAPEDITOR_PANEL_DELETE_CONFIRM_HOVER))
                                            .setPrompt(Language.MAPEDITOR_PANEL_DELETE_CONFIRM_PROMPT)
                                            .setCancelHandler(reason -> {
                                                if (reason != InputHandler.CancelReason.QUIT)
                                                    Language.msgInfo(Language.MAPEDITOR_PANEL_DELETE_CONFIRM_CANCEL).send(p);
                                            })
                                            .setConsumer(b -> {
                                                if (b) {
                                                    Language.msgPrompt(Language.MAPEDITOR_PANEL_DELETE_NOTICE_ONE).send(p);
                                                    this.mapMan.delete(id).handle((v, err) -> {
                                                        this.onMain(() -> {
                                                            if (err != null) {
                                                                if (Exceptions.displayError(p, err, Language.MAPEDITOR_PANEL_DELETE_ERROR))
                                                                    LOG.error(String.format("Map editor failed to delete map with id %s", id), err);
                                                            } else
                                                                Language.msgInfo(Language.MAPEDITOR_PANEL_DELETE_SUCCESS).send(p);
                                                        });

                                                        return v;
                                                    });
                                                }

                                                return Collections.emptyList();
                                            }));
                                })
                                .append(present ? Language.MAPEDITOR_LIST_STORE_PANEL_BUTTON : Language.MAPEDITOR_LIST_STORE_DELETE_BUTTON)

                                .reset()
                                .append(" ")

                                .hover(Language.hover(mapName, map.getMeta().generateDescription()))
                                .append(mapName)
                                .build());
                        });
                    } catch (Exception e) {
                        Translatable msg;
                        if (e instanceof TimeoutException) msg = Language.MAPEDITOR_LIST_STORE_RETRIEVEERR_BUSY;
                        else {
                            msg = Language.MAPEDITOR_LIST_STORE_RETRIEVEERR_FAIL;
                            LOG.error("Map editor couldn't retrieve maps from storage", e);
                        }

                        elements.add(Language.error(msg));
                    }

                    return elements;
                }))
            .configureFooter(footer -> footer.addDynamicElements((p, page) -> {
                List<LanguageText> elements = new ArrayList<>();

                elements.add(LanguageText.builder()
                    .format(Language.FORMAT_ACTIVE)
                    .hover(Language.hover(Language.MAPEDITOR_LIST_IMPORT_HOVER, Language.MAPEDITOR_LIST_IMPORT_DESC))
                    .action(pl -> {
                        Language.msgInfo(Language.MAPEDITOR_LIST_IMPORT_SAVE_NOTICE).send(p);
                        try {
                            if (!p.getWorld().save())
                                Language.msgError(Language.MAPEDITOR_LIST_IMPORT_SAVE_REFUSE).send(p);
                        } catch (Exception e) {
                            if (Exceptions.displayError(p, e, Language.MAPEDITOR_LIST_IMPORT_SAVE_FAIL))
                                LOG.error("Failed to save world before importing as a map", e);
                        }

                        Language.msgInfo(Language.MAPEDITOR_LIST_IMPORT_NOTICE).send(p);
                        this.mapMan.load(MapDatas.importedMap(pl.getWorld())).handle((map, err) -> {
                            this.onMain(() -> {
                                if (err != null) {
                                    if (Exceptions.displayError(p, err, Language.MAPEDITOR_LIST_IMPORT_FAIL))
                                        LOG.error("Failed to import a world as a map", err);
                                } else {
                                    Language.msgPrompt(Language.MAPEDITOR_LIST_IMPORT_SUCCESS).send(p);
                                    p.setLocationSafely(map.getWorld().getLocation(p.getLocation().getPosition()));

                                    EditedMap emap = new EditedMap(p, map);
                                    this.mapRef.put(map, emap);
                                    this.newMaps.add(emap);
                                }
                            });

                            return map;
                        });
                    })
                    .append("[+]")

                    .reset()
                    .format(Language.FORMAT_INFO)
                    .append(" ", Language.MAPEDITOR_LIST_IMPORT_LABEL)
                    .build());

                return elements;
            }))
            .build();
    }

    @Override
    public void display(PageDisplayable parent, Player to, int page) {
        this.menu.display(parent, to, page);
    }

    private List<LanguageText> generatePanel(PageDisplayable parent, EditedMap map) {
        List<LanguageText> elements = new ArrayList<>();

        // save
        // delete
        // unload
        // config meta
        // config game

        Collections.addAll(elements,
            LanguageText.builder()
                .format(Language.FORMAT_SUBTITLE)
                .hover(Language.hover(Language.MAPEDITOR_PANEL_SAVE_HOVER, Language.MAPEDITOR_PANEL_SAVE_DESC))
                .action(p -> {
                    Runnable saveAction = () -> {
                        Language.msgPrompt(Language.MAPEDITOR_PANEL_SAVE_NOTICE.withParameters(p.getName())).send(map.getChannel());
                        map.save().handle((v, err) -> {
                            this.onMain(() -> {
                                if (err != null) {
                                    if (Exceptions.displayError(p, err, Language.MAPEDITOR_PANEL_SAVE_ERROR))
                                        LOG.error(String.format("Map editor failed to save map with id %s",
                                            map.getMap().flatMap(GameMap::getId).orElse("<no id>")), err);
                                } else Language.msgPrompt(Language.MAPEDITOR_PANEL_SAVE_SUCCESS).send(map.getChannel());
                            });

                            return v;
                        });
                    };

                    if (!map.getMap().map(GameMap::getMeta).flatMap(MapMeta::getName).isPresent()) {
                        this.miniboi.getInputManager().query(p, InputHandlers.newChatHandler()
                            .setPrompt(Language.prompt(Language.MAPEDITOR_PANEL_SAVE_NAME_PROMPT))
                            .setCancelHandler(reason -> Language.msgInfo(Language.MAPEDITOR_PANEL_SAVE_NAME_CANCEL).send(p))
                            .setConsumer(name -> {
                                map.getMap().map(GameMap::getMeta).ifPresent(meta -> meta.setName(name));
                                saveAction.run();

                                return Collections.emptyList();
                            }));
                    } else saveAction.run();
                })
                .append("*) ")
                .append(TextColors.GRAY, Language.MAPEDITOR_PANEL_SAVE_LABEL)

                .line()
                .hover(Language.hover(Language.MAPEDITOR_PANEL_DELETE_HOVER, Language.MAPEDITOR_PANEL_DELETE_DESC))
                .action(p -> this.miniboi.getInputManager().query(p,
                    InputHandlers.newChoiceHandler(
                        Choice.of(true, Language.GENERIC_YES, Language.MAPEDITOR_PANEL_DELETE_CONFIRM_HOVER))
                        .setPrompt(Language.MAPEDITOR_PANEL_DELETE_CONFIRM_PROMPT)
                        .setCancelHandler(reason -> {
                            if (reason != InputHandler.CancelReason.QUIT)
                                Language.msgInfo(Language.MAPEDITOR_PANEL_DELETE_CONFIRM_CANCEL).send(p);
                        })
                        .setConsumer(b -> {
                            if (b) {
                                Language.msgPrompt(Language.MAPEDITOR_PANEL_DELETE_NOTICE.withParameters(p.getName())).send(map.getChannel());
                                map.delete().handle((v, err) -> {
                                    this.onMain(() -> {
                                        if (err != null) {
                                            if (Exceptions.displayError(p, err, Language.MAPEDITOR_PANEL_DELETE_ERROR))
                                                LOG.error(String.format("Map editor failed to delete map with id %s",
                                                    map.getMap().flatMap(GameMap::getId).orElse("<no id>")), err);
                                        } else Language.msgInfo(Language.MAPEDITOR_PANEL_DELETE_SUCCESS).send(p);
                                    });

                                    return v;
                                });
                            }

                            return Collections.emptyList();
                        })))
                .append("*) ")
                .append(TextColors.GRAY, Language.MAPEDITOR_PANEL_DELETE_LABEL)

                .line()
                .hover(Language.hover(Language.MAPEDITOR_PANEL_UNLOAD_HOVER, Language.MAPEDITOR_PANEL_UNLOAD_DESC))
                .action(p -> this.miniboi.getInputManager().query(p,
                    InputHandlers.newChoiceHandler(
                        Choice.of(true, Language.MAPEDITOR_PANEL_UNLOAD_CONFIRM_SAVE, Language.MAPEDITOR_PANEL_UNLOAD_CONFIRM_SAVE_DESC),
                        Choice.of(false, Language.MAPEDITOR_PANEL_UNLOAD_CONFIRM_NOSAVE, Language.MAPEDITOR_PANEL_UNLOAD_CONFIRM_NOSAVE_DESC))
                        .setPrompt(Language.MAPEDITOR_PANEL_UNLOAD_CONFIRM_PROMPT)
                        .setCancelHandler(reason -> {
                            if (reason != InputHandler.CancelReason.QUIT)
                                Language.msgInfo(Language.MAPEDITOR_PANEL_UNLOAD_CONFIRM_CANCEL).send(p);
                        })
                        .setConsumer(b -> {
                            map.unload(b).handle((v, err) -> {
                                this.onMain(() -> {
                                    if (err != null) {
                                        if (Exceptions.displayError(p, err, Language.MAPEDITOR_PANEL_UNLOAD_ERROR))
                                            LOG.error(String.format("Map editor failed to cleanly unload map with id %s",
                                                map.getMap().flatMap(GameMap::getId).orElse("<no id>")), err);
                                    } else Language.msgInfo(Language.MAPEDITOR_PANEL_UNLOAD_SUCCESS).send(p);
                                });

                                return v;
                            });

                            return Collections.emptyList();
                        })))
                .append("*) ")
                .append(TextColors.GRAY, Language.MAPEDITOR_PANEL_UNLOAD_LABEL)

                .line()
                .hover(Language.hover(Language.MAPEDITOR_PANEL_CFGMETA_HOVER, Language.MAPEDITOR_PANEL_CFGMETA_DESC))
                .action(p -> map.getMap().ifPresent(gm -> gm.getMeta().getRoot().display(parent, p, 0)))
                .append("*) ")
                .append(TextColors.GRAY, Language.MAPEDITOR_PANEL_CFGMETA_LABEL)

                .line()
                .hover(Language.hover(Language.MAPEDITOR_PANEL_CFGGAME_HOVER, Language.MAPEDITOR_PANEL_CFGGAME_DESC))
                .action(p -> {
                    // TODO this when minigame list is implemented
                    Language.msgError("unimplemented").send(p);
                })
                .append("*) ")
                .append(TextColors.GRAY, Language.MAPEDITOR_PANEL_CFGGAME_LABEL)
                .build());

        return elements;
    }

    /**
     * Runs a task on the main thread.
     *
     * @param r the task to run
     * @return the scheduled {@link Task}
     */
    @SuppressWarnings("UnusedReturnValue")
    private Task onMain(Runnable r) {
        return Sponge.getScheduler().createTaskBuilder().execute(r).submit(this.miniboi);
    }

}
