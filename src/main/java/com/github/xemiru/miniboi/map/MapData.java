package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.game.Minigame;
import ninja.leaping.configurate.ConfigurationNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spongepowered.api.world.World;

import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;

/**
 * Information describing a {@link GameMap} template.
 */
public abstract class MapData {

    private String id;
    private boolean initialized = false;
    private boolean valid = true;

    /**
     * Returns the ID of the map represented by this {@link MapData}.
     *
     * <p>The ID is the identifier of the map in internal storage. This value must be kept up-to-date whenever a save or
     * load operation occurs involving the map represented by this MapData. Should the former have yet to be saved to
     * a storage unit, this should return empty.</p>
     *
     * @return the ID of this MapData?
     */
    public final Optional<String> getId() {
        return Optional.ofNullable(this.id);
    }

    /**
     * Sets the ID of the map represented by this {@link MapData}.
     *
     * @param id the new ID
     * @see #getId()
     */
    protected final void setId(String id) {
        this.id = id;
    }

    /**
     * @return the {@link MapMeta} describing the map represented by this {@link MapData}.
     */
    public abstract MapMeta getMeta();

    /**
     * Returns a mapping of {@link Minigame} IDs to their configurations for the map represented by this {@link MapData}
     * stored as {@link ConfigurationNode}s, or an empty Optional if this MapData does not support configurations.
     *
     * @return a mapping of Minigame IDs to their configurations?
     */
    public abstract Optional<Map<String, ConfigurationNode>> getGameConfigs();

    /**
     * Returns whether or not this {@link MapData} supports the configuration of {@link Minigame}s that try to use it.
     *
     * @return if this MapData supports Minigame configurations
     */
    public boolean isSupportingConfigurations() {
        return this.getGameConfigs().isPresent();
    }

    /**
     * Returns a {@link ConfigurationNode} holding a configuration structure for the {@link Minigame} of the requested
     * ID.
     *
     * <p>If {@link #isSupportingConfigurations()} returns false, this method always returns empty.</p>
     *
     * @param id the ID of the Minigame to query
     * @return the Minigame's configuration?
     */
    public Optional<ConfigurationNode> getGameConfig(String id) {
        return this.getGameConfigs().map(it -> it.get(id));
    }

    /**
     * Sets a {@link ConfigurationNode} holding a configuration structure for the {@link Minigame} of the given ID.
     *
     * <p>If {@link #isSupportingConfigurations()} returns false, this method is a no-op.</p>
     *
     * @param id the id of the Minigame to configure
     * @param config the Minigame's new configuration, or null to remove
     */
    public void setGameConfig(String id, ConfigurationNode config) {
        this.getGameConfigs().ifPresent(it -> {
            if (config == null) it.remove(id);
            else it.put(id, config);
        });
    }

    /**
     * @return whether or not maps loaded through this {@link MapData} can be saved.
     */
    protected abstract boolean isSaveable();

    /**
     * Prepares this {@link MapData} for loading instances of the {@link World} it represents.
     */
    protected abstract void init();

    /**
     * Calls {@link #init()} if it has yet to be called.
     */
    void internalInit() {
        if (!this.initialized) {
            this.init();
            this.initialized = true;
        }
    }

    /**
     * Loads this {@link MapData}.
     *
     * <p>This method is called by a {@link MapManager} when a request to load this MapData is passed. When called, this
     * method will receive a path to a folder in which this MapData should deposit the files of the {@link World} it
     * represents. The created World folder does not need to have any plugin-related files.</p>
     *
     * <p>This method can be called multiple times in a row to load extra instances of the same map.</p>
     *
     * <p>This method will NEVER be called without {@link #init()} being called prior, or after a call to
     * {@link #cleanup()}.</p>
     *
     * @param destination the destination of the world files
     */
    protected abstract void load(Path destination);

    /**
     * @return whether or not this map has been initialized and has yet to be cleaned up
     */
    public boolean isValid() {
        return this.initialized && this.valid;
    }

    /**
     * Cleans up resources for this {@link MapData}.
     *
     * <p><b>This is NOT called for cleaning up {@link World} instances loaded through this {@link MapData}.</b> Should
     * MapData need to write extra data to make itself loadable, this method is called after {@link #load(Path)} to
     * allow a chance to clean up that extra data when the map in its entirety is no longer being used.</p>
     *
     * <p>This method will NEVER be called without {@link #init()} being called prior.</p>
     */
    protected abstract void cleanup();

    /**
     * Calls {@link #cleanup()} and updates the return value of {@link #isValid()}.
     */
    void internalCleanup() {
        if (this.isValid()) {
            this.valid = false;
            this.cleanup();
        }
    }

    /**
     * Copies data from a {@link GameMap}.
     *
     * <p>This method is called in the process of overwriting a saved map. Changes denoted in a GameMap involving meta
     * and game configurations are transferred to the source {@link MapData} before saving, allowing changes to be
     * written to disk and memory to make sure all subsequent loads come with updated information.</p>
     *
     * @param map the GameMap to copy from
     */
    protected final void copyFrom(GameMap map) {
        this.getMeta().copyFrom(map.getMeta());
        this.getGameConfigs().ifPresent(dataConfigs -> {
            dataConfigs.clear();

            for (String key : map.getGameConfigs().keySet())
                map.getGameConfig(key).ifPresent(it -> dataConfigs.put(key, it.copy()));
        });
    }

}
