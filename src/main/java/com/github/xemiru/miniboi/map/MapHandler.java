package com.github.xemiru.miniboi.map;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Handles interfacing with the storage of {@link MapData}.
 */
public interface MapHandler<T extends MapData> {

    /**
     * Attempts to save a provided {@link GameMap} into the storage unit of this {@link MapHandler} and returns the
     * representative {@link MapData} object.
     *
     * <p>The identifying token of the map within the storage unit should always be the ID returned by the source
     * {@link MapData}'s {@link MapData#getId()} field. It is possible to receive data where this field is empty, in
     * which case it is considered a new map and should be assigned a generated ID per the handler's discretion. If the
     * manager receives data without an ID from this method, it will throw an exception.</p>
     *
     * @param map the map to save
     * @return a new MapData instance representing the saved map
     */
    CompletableFuture<T> save(GameMap map);

    /**
     * Attempts to delete the files relevant to the provided {@link MapData} instance.
     *
     * @param data the data to delete
     */
    CompletableFuture<Void> delete(MapData data);

    /**
     * Returns a list of {@link MapData} instances linked to the storage unit of this {@link MapHandler}.
     *
     * <p>This method is called at least once per session to initialize the list of maps known by this MapHandler, but
     * can be called any number of times thereafter by user command should a reload of maps from disk is deemed
     * necessary.</p>
     *
     * <p>Should loading be unsuccessful, this operation should fail with a {@link MapException}.</p>
     *
     * @return a list of MapData instances
     */
    CompletableFuture<List<T>> loadMaps();

}
