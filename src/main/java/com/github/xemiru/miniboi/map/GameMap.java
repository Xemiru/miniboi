package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.game.Minigame;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.world.World;

import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * A map to be used by a {@link Minigame}, loaded by a {@link MapManager} with information from a {@link MapData}
 * instance.
 */
public class GameMap {

    private String id;
    private World world;
    private MapMeta metaCopy;
    private boolean saveable;
    private Map<String, ConfigurationNode> configCopy = new HashMap<>();

    GameMap(World world, MapData sourceData) {
        this.world = world;
        this.metaCopy = new MapMeta();
        this.metaCopy.copyFrom(sourceData.getMeta());
        this.saveable = sourceData.isSaveable();

        sourceData.getId().ifPresent(it -> this.id = it);
        sourceData.getGameConfigs().ifPresent(map -> {
            for (String key : map.keySet()) this.configCopy.put(key, map.get(key).copy());
        });
    }

    /**
     * Returns the {@link World} loaded by this {@link GameMap}.
     *
     * <p>{@link MapHandler}s intending to perform a save operation on this map should not necessarily look for this
     * world's directory, but instead look to {@link #getWorldFiles()}.</p>
     *
     * @return the World loaded by this GameMap
     * @see #getWorldFiles()
     */
    public World getWorld() {
        return this.world;
    }

    /**
     * Returns the {@link MapMeta} assigned to the {@link MapData} this {@link GameMap} was loaded from.
     *
     * <p>The MapMeta returned by this GameMap is a copy of the one held by its source MapData. Modifying the meta
     * through this GameMap will not affect other GameMaps loaded from the same source data unless this GameMap is
     * saved, in which subsequent loads of the MapData will reflect the changes.</p>
     *
     * @return the MapMeta describing this GameMap
     */
    public MapMeta getMeta() {
        return this.metaCopy;
    }

    /**
     * @return whether or not this {@link GameMap} is saveable as per its source {@link MapData}
     */
    public boolean isSaveable() {
        return this.saveable;
    }

    /**
     * @return the ID assigned to this {@link GameMap}?
     */
    public Optional<String> getId() {
        return Optional.ofNullable(this.id);
    }

    /**
     * Sets the ID to assign to this {@link GameMap}.
     *
     * @param id the ID to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Returns a mapping of {@link Minigame} IDs to their configurations for the map represented by this {@link MapData}
     * stored as {@link ConfigurationNode}s, or an empty Optional if this MapData does not support configurations.
     *
     * <p>The configurations this {@link GameMap} holds are copies of the ones held by its source data. Modifying
     * configurations through this GameMap will not affect other GameMaps loaded from the same source data unless this
     * GameMap is saved, in which subsequent loads of the MapData will reflect the changes.</p>
     *
     * @see MapData#getGameConfigs()
     */
    public Map<String, ConfigurationNode> getGameConfigs() {
        return this.configCopy;
    }

    /**
     * Returns a {@link ConfigurationNode} holding a configuration structure for the {@link Minigame} of the requested
     * ID.
     *
     * @param id the ID of the Minigame to query
     * @return the Minigame's configuration?
     *
     * @see MapData#getGameConfig(String)
     */
    public Optional<ConfigurationNode> getGameConfig(String id) {
        return Optional.ofNullable(this.getGameConfigs().get(id));
    }

    /**
     * Sets a {@link ConfigurationNode} holding a configuration structure for the {@link Minigame} of the given ID.
     *
     * @param id the id of the Minigame to configure
     * @param config the Minigame's new configuration, or null to remove
     * @see MapData#setGameConfig(String, ConfigurationNode)
     */
    public void setGameConfig(String id, ConfigurationNode config) {
        if (config == null) this.configCopy.remove(id);
        else this.configCopy.put(id, config);
    }

    /**
     * Returns a set of {@link Path}s pointing to all files and folders relevant to a Minecraft {@link World}.
     *
     * <p>For the case of constantly-reloaded worlds, only a few files and folders are relevant to the saving of a
     * world in the context of this plugin. The following is what files and folders can be expected from this method,
     * starting from the directory returned by {@link World#getDirectory()}.</p>
     *
     * <ul>
     * <li>data</li>
     * <li>region</li>
     * <li>level.dat</li>
     * </ul>
     *
     * @return a set of Paths
     */
    public Set<Path> getWorldFiles() {
        Set<Path> paths = new HashSet<>();
        Path worldDir = this.getWorld().getDirectory();

        Collections.addAll(paths,
            worldDir.resolve("data"),
            worldDir.resolve("region"),
            worldDir.resolve("level.dat"));
        return paths;
    }

}
