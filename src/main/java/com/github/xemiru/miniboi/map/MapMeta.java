package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.config.ListNode;
import com.github.xemiru.miniboi.config.NodeTypes;
import com.github.xemiru.miniboi.config.SectionNode;
import com.github.xemiru.miniboi.config.ValueNode;
import com.github.xemiru.miniboi.language.ComposedTranslatable;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.language.LiteralTranslatable;
import org.spongepowered.api.text.translation.Translatable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class MapMeta implements Cloneable {

    private static final DateFormat DATE_FORMAT = SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.SHORT, SimpleDateFormat.FULL);

    // region node defs

    private SectionNode root = SectionNode.builder()
        .setId("meta")
        .setName(Language.MAPMETA_META_LABEL)
        .setDescription(Language.MAPMETA_META_DESC)
        .build();

    private ValueNode<String> name = ValueNode.builder(root, NodeTypes.NULLABLE(NodeTypes.STRING))
        .setId("name")
        .setName(Language.MAPMETA_NAME_LABEL)
        .setDescription(Language.MAPMETA_NAME_DESC)
        .setValidationSimple(value -> {
            if (value == null || value.trim().isEmpty()) return Optional.of(Language.MAPMETA_NAME_VERIFY_NONEMPTY);
            return Optional.empty();
        })
        .setReadTransform(name -> name == null ? Language.MAPMETA_NAME_NOVALUE : LiteralTranslatable.of(name))
        .setInitialValue(null)
        .build();

    private ValueNode<String> description = ValueNode.builder(root, NodeTypes.NULLABLE(NodeTypes.STRING))
        .setId("desc")
        .setName(Language.MAPMETA_DESC_LABEL)
        .setDescription(Language.MAPMETA_DESC_DESC)
        .setReadTransform(desc -> desc == null ? Language.GENERIC_NODESC : LiteralTranslatable.of(desc))
        .setInitialValue(null)
        .build();

    private ListNode<String> authors = ListNode.builder(root, NodeTypes.STRING)
        .setId("authors")
        .setName(Language.MAPMETA_AUTHORS_LABEL)
        .setDescription(Language.MAPMETA_AUTHORS_DESC)
        .build();

    private ValueNode<Long> created = ValueNode.builder(root, NodeTypes.LONG)
        .setId("creationDate")
        .setName(Language.MAPMETA_CREATION_LABEL)
        .setDescription(Language.MAPMETA_CREATION_DESC)
        .setReadTransform(l -> LiteralTranslatable.of(DateFormat.getDateInstance().format(new Date(l))))
        .setEditable(false)
        .setInitialValue(System.currentTimeMillis())
        .build();

    private ValueNode<Long> lastEdited = ValueNode.builder(root, NodeTypes.LONG)
        .setId("editDate")
        .setName(Language.MAPMETA_EDITED_LABEL)
        .setDescription(Language.MAPMETA_EDITED_DESC)
        .setReadTransform(l -> LiteralTranslatable.of(DateFormat.getDateInstance().format(new Date(l))))
        .setInitialValue(created.getValue())
        .setEditable(false)
        .build();

    // endregion

    /**
     * Returns whether or not the configuration held by this {@link MapMeta} is considered valid.
     *
     * @return if the configuration is valid
     */
    public boolean isValid() {
        return this.root.isValid();
    }

    /**
     * @return the root {@link SectionNode} of this {@link MapMeta}
     */
    public SectionNode getRoot() {
        return this.root;
    }

    /**
     * Returns the name of the map owning this {@link MapMeta}.
     *
     * @return the name of the map owning this MapMeta
     */
    public Optional<String> getName() {
        return Optional.ofNullable(this.name.getValue());
    }

    /**
     * Sets the name of the map owning this {@link MapMeta}.
     *
     * @param name the new name to set
     */
    public void setName(String name) {
        this.name.setValue(name);
    }

    /**
     * @return the description of the map owning this {@link MapMeta}?
     */
    public Optional<String> getDescription() {
        return Optional.ofNullable(this.description.getValue());
    }

    /**
     * Sets the description of the map owning this {@link MapMeta}.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description.setValue(description);
    }

    /**
     * @return the authors of the map owning this {@link MapMeta}
     */
    public List<String> getAuthors() {
        return this.authors.getValue();
    }

    /**
     * @return the date at which the map owning this {@link MapMeta} was created
     */
    public long getCreationTime() {
        return this.created.getValue();
    }

    /**
     * @return the date at which the map owning this {@link MapMeta} was last edited?
     */
    public long getLastEditTime() {
        return this.lastEdited.getValue();
    }

    /**
     * Sets the time at which the map owning this {@link MapMeta} was last edited.
     *
     * @param time the time at which the map owning this MapMeta was last edited
     */
    public void setLastEditTime(long time) {
        this.lastEdited.setValue(time);
    }

    /**
     * @return a description text
     */
    public LanguageText generateDescription() {
        return LanguageText.builder(Language.MAX_WRAP_LENGTH)
            .format(Language.FORMAT_INFO)
            .append(this.getDescription().<Translatable>map(LiteralTranslatable::of).orElse(Language.GENERIC_NODESC))
            .line(2)
            .format(Language.FORMAT_SUBTITLE)
            .append(Language.MAPMETA_AUTHORS_LABEL, ": ")
            .adaptive(() -> {
                Translatable authors = ComposedTranslatable.of("<", Language.NODE_GENERIC_NOVALUE, ">");
                if (this.getAuthors().size() > 0) {
                    StringBuilder sb = new StringBuilder();
                    this.getAuthors().forEach(author -> sb.append(author).append(", "));
                    sb.setLength(sb.length() - 2);

                    authors = LiteralTranslatable.of(sb.toString());
                }

                return LanguageText.withFormat(Language.FORMAT_INFO, authors);
            })
            .line()

            .append(Language.MAPMETA_CREATION_LABEL, ": ")
            .adaptive(() ->
                LanguageText.withFormat(Language.FORMAT_INFO, DATE_FORMAT.format(new Date(this.created.getValue()))))
            .line()

            .append(Language.MAPMETA_EDITED_LABEL, ": ")
            .adaptive(() ->
                LanguageText.withFormat(Language.FORMAT_INFO, DATE_FORMAT.format(new Date(this.lastEdited.getValue()))))
            .build();
    }

    /**
     * Copies values from another {@link MapMeta} instance.
     *
     * @param model the MapMeta instance to copy from
     */
    public void copyFrom(MapMeta model) {
        this.name.setValue(model.name.getValue());
        this.description.setValue(model.description.getValue());
        this.authors.setValue(new ArrayList<>(model.authors.getValue()));
        this.created.setValue(model.created.getValue());
        this.lastEdited.setValue(model.lastEdited.getValue());
    }

}
