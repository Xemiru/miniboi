package com.github.xemiru.miniboi.map;

import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.WorldArchetype;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;

/**
 * {@link MapData} created from a generated world.
 *
 * <p>Note that each call to {@link #load(Path)} will generate a new world.</p>
 */
public class GeneratedMapData extends MapData {

    private MapMeta meta;

    private WorldArchetype arch;

    public GeneratedMapData(WorldArchetype arch) {
        this.arch = arch;
        this.meta = new MapMeta();
    }

    @Override
    public MapMeta getMeta() {
        return this.meta;
    }

    @Override
    public Optional<Map<String, ConfigurationNode>> getGameConfigs() {
        return Optional.empty();
    }

    @Override
    protected boolean isSaveable() {
        return false;
    }

    @Override
    protected void init() {
        // no initialize behavior
    }

    @Override
    protected void load(Path destination) {
        try {
            Sponge.getServer().createWorldProperties(destination.getFileName().toString(), this.arch);
        } catch (IOException e) {
            throw new IllegalStateException("World creation error", e);
        }
    }

    @Override
    protected void cleanup() {
        // generated world will get deleted on its own
    }

}
