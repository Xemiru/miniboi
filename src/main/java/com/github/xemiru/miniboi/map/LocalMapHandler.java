package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.Miniboi;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.util.Exceptions;
import com.github.xemiru.miniboi.util.FileUtil;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.gson.GsonConfigurationLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * A {@link MapHandler} implementation that stores its maps locally in the configuration folder of the plugin.
 */
public class LocalMapHandler implements MapHandler<LocalMapData> {

    private static final Logger LOG = LoggerFactory.getLogger(LocalMapHandler.class);
    private static final String MAP_FOLDER = "maps";

    public static final String META_FILE_NAME = "miniboi.json";
    public static final String MAP_CONFIG_FOLDER = "mb_gameconfig";

    private Path mapDir;
    private Set<String> reservedIds = Collections.newSetFromMap(new ConcurrentHashMap<>());

    public LocalMapHandler(Miniboi miniboi) {
        this.mapDir = miniboi.getConfigFolder().resolve(MAP_FOLDER);
        if (!Files.exists(this.mapDir))
            try {
                Files.createDirectories(this.mapDir);
                FileUtil.forEachFile(this.mapDir, file -> {
                    try {
                        if (file.getFileName().toString().toLowerCase().endsWith(".saving")) Files.deleteIfExists(file);
                    } catch (Exception e) {
                        throw new IllegalStateException("Could not set up map directory");
                    }
                });
            } catch (RuntimeException rethrow) {
                throw rethrow;
            } catch (Exception e) {
                throw new IllegalStateException("Could not set up map directory");
            }
    }

    @Override
    public CompletableFuture<LocalMapData> save(GameMap map) {
        return CompletableFuture.supplyAsync(() -> {
            if (!map.isSaveable()) throw new MapException(Language.MAPMAN_SAVE_ERROR_UNSAVEABLE);
            if (!map.getMeta().isValid()) throw new MapException(Language.MAPMAN_SAVE_ERROR_INVALID);

            String id = map.getId().orElse(this.generateId(map.getMeta()));

            Path saveDest = FileUtil.reserveTempPath();
            Path backDest = this.mapDir.resolve(String.format("%s.backup", id));
            Path realDest = this.mapDir.resolve(String.format("%s.zip", id));

            // Start writing to output file
            ZipOutputStream zos = null;
            try {
                Files.createFile(saveDest);
                zos = new ZipOutputStream(Files.newOutputStream(saveDest));

                // Save all world files
                Path worldDir = map.getWorld().getDirectory();
                final ZipOutputStream fZos = zos;

                map.getWorldFiles().forEach(f -> {
                    try {
                        FileUtil.forEachFile(f, file -> {
                            Path relativeDir = worldDir.relativize(file);

                            try {
                                fZos.putNextEntry(new ZipEntry(relativeDir.toString()));

                                byte[] buf = new byte[4096];
                                InputStream bis = Files.newInputStream(file);

                                int read;
                                while ((read = bis.read(buf)) > 0) fZos.write(buf, 0, read);
                                fZos.closeEntry();
                            } catch (Exception e) {
                                Exceptions.sneakyThrow(e);
                            }
                        });
                    } catch (Exception e) {
                        Exceptions.sneakyThrow(e);
                    }
                });

                // Save configuration files
                GsonConfigurationLoader gcl = GsonConfigurationLoader.builder()
                    .setSink(() -> new BufferedWriter(new OutputStreamWriter(new OutputStream() {
                        @Override
                        public void write(int b) throws IOException {
                            fZos.write(b);
                        }

                        @Override
                        public void flush() throws IOException {
                            fZos.flush();
                        }
                    })))
                    .build();

                map.getGameConfigs().keySet().forEach(key -> {
                    try {
                        fZos.putNextEntry(new ZipEntry(String.format("%s/%s.json", MAP_CONFIG_FOLDER, key)));
                        gcl.save(map.getGameConfigs().get(key));
                        fZos.closeEntry();
                    } catch (Exception e) {
                        Exceptions.sneakyThrow(e);
                    }
                });

                // Save meta file
                ConfigurationNode node = gcl.createEmptyNode();
                zos.putNextEntry(new ZipEntry(META_FILE_NAME));
                map.getMeta().getRoot().save(node);
                gcl.save(node);
                zos.closeEntry();

                // Finalize
                zos.close();

                if (Files.exists(realDest)) {
                    Files.deleteIfExists(backDest);
                    Files.move(realDest, backDest);
                }

                Files.move(saveDest, realDest);
            } catch (Exception e) {
                Exceptions.sneakyThrow(e);
            } finally {
                try {
                    if (zos != null) zos.close();
                } catch (Exception e) {
                    Exceptions.sneakyThrow(e);
                }
            }

            return new LocalMapData(id, () -> {
                try {
                    return new ZipFile(realDest.toFile());
                } catch (IOException e) {
                    Exceptions.sneakyThrow(e);
                    return null;
                }
            });
        });
    }

    @Override
    public CompletableFuture<Void> delete(MapData data) {
        Path zipFile = this.getMapPath(data.getId().orElseThrow(() -> new MapException(Language.MAPMAN_ERROR_MAPINVALID_ID)));
        if (Files.notExists(zipFile)) throw new MapException(Language.MAPMAN_ERROR_UNKNOWNMAP);
        return CompletableFuture.runAsync(() -> {
            try {
                Files.deleteIfExists(zipFile);
            } catch (Exception e) {
                Exceptions.sneakyThrow(e);
            }
        });
    }

    @Override
    public CompletableFuture<List<LocalMapData>> loadMaps() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return Files.list(this.mapDir)
                    .filter(it -> it.getFileName().toString().toLowerCase().endsWith(".zip"))
                    .map(it -> {
                        String name = it.getFileName().toString();
                        Optional<LocalMapData> opt = this.loadById(name.substring(0, name.lastIndexOf('.')));
                        if (!opt.isPresent()) LOG.warn(String.format("Could not load map at %s", it.toAbsolutePath()));
                        return opt;
                    }).filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(Collectors.toList());
            } catch (IOException e) {
                Exceptions.sneakyThrow(e);
                return null;
            }
        });
    }

    private Optional<LocalMapData> loadById(String id) {
        Path zipFile = this.getMapPath(id);
        if (Files.exists(zipFile)) {
            return Optional.of(new LocalMapData(id, () -> {
                try {
                    return new ZipFile(zipFile.toFile());
                } catch (Exception e) {
                    Exceptions.sneakyThrow(e);
                    return null;
                }
            }));
        }

        return Optional.empty();
    }

    private String generateId(MapMeta meta) {
        String id = meta.getName().orElseThrow(() -> new MapException(Language.MAPMAN_ERROR_MAPINVALID_META))
            .toLowerCase().replace(' ', '_');
        String newId = FileUtil.getUnusedVariant(id, candidate ->
            !this.reservedIds.contains(candidate) && Files.notExists(this.mapDir.resolve(String.format("%s.zip", candidate))))
            .orElseThrow(() -> new MapException(Language.MAPMAN_INTERNAL_IDFAIL));

        this.reservedIds.add(newId);
        return newId;
    }

    private Path getMapPath(String id) {
        return this.mapDir.resolve(String.format("%s.zip", id));
    }

}
