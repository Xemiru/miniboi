package com.github.xemiru.miniboi.map;

import com.github.xemiru.miniboi.Miniboi;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.EventContext;
import org.spongepowered.api.event.impl.AbstractEvent;

import static java.util.Objects.requireNonNull;

/**
 * A configuration event, permitting external plugins to change the {@link MapHandler} used by the plugin.
 */
public class MapManagerEvent extends AbstractEvent {

    private Cause cause;
    private MapHandler<? extends MapData> handler;

    public MapManagerEvent(Miniboi miniboi) {
        this.handler = new LocalMapHandler(miniboi);
        this.cause = Cause.of(EventContext.empty(), miniboi);
    }

    /**
     * @return the handler to be used by the {@link MapManager} requesting configuration
     */
    public MapHandler<? extends MapData> getHandler() {
        return this.handler;
    }

    /**
     * Set the {@link MapHandler} to be used by the {@link MapManager} requesting configuration.
     *
     * @param handler the MapHandler for the MapManager to use
     */
    public void setHandler(MapHandler<? extends MapData> handler) {
        this.handler = requireNonNull(handler);
    }

    @Override
    public Cause getCause() {
        return this.cause;
    }

}
