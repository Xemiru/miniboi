package com.github.xemiru.miniboi;

import com.github.xemiru.miniboi.input.InputManager;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.map.MapEditor;
import com.github.xemiru.miniboi.map.MapManager;
import com.github.xemiru.miniboi.map.MapManagerEvent;
import com.github.xemiru.miniboi.util.ChatFreezeManager;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameAboutToStartServerEvent;
import org.spongepowered.api.event.game.state.GameConstructionEvent;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.plugin.Plugin;

import java.nio.file.Path;

@Plugin(
    id = "miniboi",
    name = "Miniboi",
    description = "A minigame framework.",
    version = "$VERSION$",
    authors = { "Xemiru" })
public class Miniboi {

    private static Miniboi INSTANCE;

    public static Miniboi getInstance() {
        if (INSTANCE == null) throw new IllegalStateException("Plugin not initialized");
        return INSTANCE;
    }

    private static void setInstance(Miniboi boi) {
        if (INSTANCE != null) throw new IllegalStateException("Plugin already initialized");
        Miniboi.INSTANCE = boi;
    }

    private ChatFreezeManager cfMan;
    private InputManager inMan;
    private MapManager mapMan;
    private MapEditor editor;
    private Config config;

    private boolean initSuccess = true;

    @Inject
    @ConfigDir(sharedRoot = false)
    private Path configFolder;

    @Inject
    private Logger log;

    public Miniboi() {
    }

    public Path getConfigFolder() {
        return this.configFolder;
    }

    public InputManager getInputManager() {
        return this.inMan;
    }

    public MapManager getMapManager() {
        return this.mapMan;
    }

    @Listener
    public void onConstruct(GameConstructionEvent e) {
        Miniboi.setInstance(this);
    }

    @Listener
    public void onInit(GameInitializationEvent ev) {
        this.inMan = new InputManager(this);
        this.cfMan = new ChatFreezeManager(this);

        // load en_US language to set initial config labels
        try {
            Language.initLanguages();
        } catch (Exception e) {
            this.initSuccess = false;
            log.error("Failed to initialize internal language; the plugin cannot continue.", e);
        }

        // initialize config
        try {
            this.config = new Config(this);
        } catch (Exception e) {
            this.initSuccess = false;
            log.error("Failed to initialize configuration; the plugin cannot continue.", e);
        }
    }

    @Listener
    public void onPostInit(GameAboutToStartServerEvent ev) {
        if (this.initSuccess) {
            MapManagerEvent mme = new MapManagerEvent(this);
            Sponge.getEventManager().post(mme);

            try {
                if (mme.getHandler() == null)
                    throw new IllegalStateException("No map handler was set; the plugin cannot continue");

                log.info("Initializing map manager");
                this.mapMan = new MapManager(this, mme.getHandler());
                this.editor = new MapEditor(this, this.mapMan);
                Sponge.getCommandManager().register(this, CommandSpec.builder()
                    .executor((src, ctx) -> {
                        if (src instanceof Player) this.editor.display(null, ((Player) src), 0);
                        return CommandResult.success();
                    })
                    .build(), "map");

                log.info("Successfully started map manager");
            } catch (Exception e) {
                log.error("Failed to initialize map manager; the plugin cannot continue", e);
                this.mapMan = null;
                this.editor = null;
            }
        }
    }

}
