package com.github.xemiru.miniboi.config;

import com.github.xemiru.miniboi.input.InputHandler;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.language.LiteralTranslatable;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.translation.Translatable;

import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * A type descriptor for a kind of value.
 *
 * @param <T> the kind of value to represent
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class NodeType<T> {

    /**
     * Functional interface used as a response to a {@link Player}'s request to edit a {@link Node}.
     *
     * @param <T> the type of the value
     */
    public interface EditAction<T> {

        /**
         * Requests to edit a value through the provided {@link Player}.
         *
         * <p>The success callback should take a proposed value and check it for errors. The value is accepted if the
         * callback returns an empty list denoting no errors found with the value; it otherwise returns a list of issues
         * found to be returned to the editing player.</p>
         *
         * @param editor the Player requesting the edit
         * @param successCallback the function to call when the edit happens successfully
         * @param cancelCallback the function to call when the edit is cancelled for any reason
         */
        void edit(Player editor, Function<T, List<Translatable>> successCallback, Consumer<InputHandler.CancelReason> cancelCallback);

    }

    /**
     * Returns a new {@link Builder} to create a {@link NodeType} with.
     *
     * @param <T> the type to represent
     * @return a Builder
     */
    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    /**
     * Builder class for {@link NodeType}s.
     */
    public static class Builder<T> {

        private NodeType<T> obj = new NodeType<>();

        private Builder() {
        }

        /**
         * Copies all properties of another {@link NodeType} into this {@link Builder}.
         *
         * @param model the pre-existing NodeType to copy from
         * @return this Builder
         */
        public Builder<T> copyFrom(NodeType<T> model) {
            obj.name = model.name;
            obj.serializer = model.serializer;
            obj.deserializer = model.deserializer;
            obj.description = model.description;
            obj.readTransform = model.readTransform;
            obj.validation = model.validation;
            obj.editAct = model.editAct;
            obj.visualizeAct = model.visualizeAct;
            return this;
        }

        /**
         * @see NodeType#getName()
         */
        public Builder<T> setName(Translatable name) {
            obj.name = requireNonNull(name);
            return this;
        }

        /**
         * @see NodeType#getSerializer()
         */
        public Builder<T> setSerializer(Function<T, Object> serializer) {
            obj.serializer = serializer;
            return this;
        }

        /**
         * @see NodeType#isNullable()
         */
        public Builder<T> setNullable(boolean state) {
            obj.nullable = state;
            return this;
        }

        /**
         * @see NodeType#getDeserializer()
         */
        public Builder<T> setDeserializer(Function<Object, T> deserializer) {
            obj.deserializer = deserializer;
            return this;
        }

        /**
         * @see NodeType#getDescription()
         */
        public Builder<T> setDescription(Translatable desc) {
            obj.description = desc;
            return this;
        }

        /**
         * @see NodeType#getReadTransform()
         */
        public Builder<T> setReadableTransform(Function<T, Translatable> readTransform) {
            obj.readTransform = requireNonNull(readTransform);
            return this;
        }

        /**
         * @see NodeType#getValidation()
         */
        public Builder<T> setValidation(Function<T, Optional<Translatable>> validation) {
            obj.validation = requireNonNull(validation);
            return this;
        }

        /**
         * @see NodeType#getEditAction()
         */
        public Builder<T> setEditAction(EditAction<T> editAct) {
            obj.editAct = editAct;
            return this;
        }

        /**
         * @see NodeType#getVisualizeAction()
         */
        public Builder<T> setVisualizeAction(BiConsumer<Player, T> visualizeAct) {
            obj.visualizeAct = visualizeAct;
            return this;
        }

        /**
         * @return a new {@link NodeType} with the properties described in this {@link Builder}
         */
        public NodeType<T> build() {
            // verify the obj
            requireNonNull(obj.name, "name");
            requireNonNull(obj.serializer, "serializer");
            requireNonNull(obj.deserializer, "deserializer");

            this.obj.hover = Language.hover(obj.name, obj.getDescription().orElse(Language.GENERIC_NODESC));
            return this.obj;
        }

    }

    // required
    private Translatable name;
    private Function<T, Object> serializer;
    private Function<Object, T> deserializer;

    // optional
    private boolean nullable = false;
    private Translatable description = null;
    private Function<T, Translatable> readTransform = it -> LiteralTranslatable.of(it == null ? "null" : it.toString());
    private Function<T, Optional<Translatable>> validation = it -> Optional.empty();
    private BiConsumer<Player, T> visualizeAct = null;
    private EditAction<T> editAct = null;

    // internal
    private LanguageText hover;

    private NodeType() {
    }

    // required

    /**
     * @return the human-readable name of this {@link NodeType}
     */
    public Translatable getName() {
        return this.name;
    }

    /**
     * Returns the serializing function for this {@link NodeType}.
     *
     * <p>The serializing function should return an object that can be acceptably stored by a {@link ConfigurationNode}.
     * Should this serializing function fail, that is -- it returns a value that is considered not acceptable by a
     * ConfigurationNode through throwing an {@link ObjectMappingException}, a {@link SerializationException} will be
     * thrown.</p>
     *
     * <p>The serializing function will receive the value of the node requesting serialization; <b>the function should
     * correctly handle null values for nullable types where it is possible for nodes to hold them.</b></p>
     *
     * @return the serializing function for this NodeType
     */
    // TODO correctly call de/serializer
    public Function<T, Object> getSerializer() {
        return this.serializer;
    }

    /**
     * Returns the deserializing function for this {@link NodeType}.
     *
     * <p>The serializing function will receive an object returned by the {@link ConfigurationNode#getValue()}
     * method.</p>
     *
     * <p>Should the deserializer fail to produce a good value, the received object should be considered invalid and the
     * function itself should throw a {@link SerializationException}.</p>
     *
     * @return the deserializing function for this NodeType
     */
    public Function<Object, T> getDeserializer() {
        return this.deserializer;
    }

    // optional

    /**
     * Returns whether or not {@link Node}s of this {@link NodeType} should accept null values.
     *
     * <p>By default, NodeTypes are <b>non-nullable.</b></p>
     *
     * @return whether or not Nodes of this NodeType should accept null values
     */
    public boolean isNullable() {
        return this.nullable;
    }

    /**
     * @return the description for the possible values of this {@link NodeType}
     */
    public Optional<Translatable> getDescription() {
        return Optional.ofNullable(this.description);
    }

    /**
     * Returns a transform function converting a possible value of this {@link NodeType} into a human-readable form.
     *
     * <p>The transform function should be careful about null values when {@link #isNullable()} permits a {@link Node}
     * to hold one.</p>
     *
     * @return the transform function
     */
    public Function<T, Translatable> getReadTransform() {
        return this.readTransform;
    }

    /**
     * Returns the validation function ensuring a value is an acceptable representative of this {@link NodeType}.
     *
     * <p>The validation function takes a value represented by this NodeType, and should return an empty Optional if the
     * value is considered acceptable. The Optional otherwise contains an error message describing the issue associated
     * with the value in question.</p>
     *
     * <p>The validation function is the first gatekeeper to the functionality of a {@link Node}, which cannot hold any
     * values deemed invalid by a {@link NodeType}. As an example, if the validation function always prevents null
     * values, a node of the same type cannot ever hold null values.</p>
     *
     * <p>The validation functions of {@link NodeType}s are fail-fast; a validation error being detected quickly results
     * in an error message returning to the source of the validation attempt.</p>
     *
     * @return the validation function ensuring a value is an acceptable representative of this NodeType
     */
    public Function<T, Optional<Translatable>> getValidation() {
        return this.validation;
    }

    /**
     * Returns the action to take should something identifying with this {@link NodeType} request a new value.
     *
     * @return the action to take if a value identifying with this NodeType requests a new value
     * @see EditAction#edit(Player, Function, Consumer)
     */
    public Optional<EditAction<T>> getEditAction() {
        return Optional.ofNullable(this.editAct);
    }

    /**
     * Returns the action to take should something identifying with this {@link NodeType} request to be shown to a
     * {@link Player}.
     *
     * <p>The function takes the {@link Player} to show the value to, as well as the current value to be visualized.</p>
     *
     * <p>The visualization function should be careful about null values when {@link #isNullable()} permits a
     * {@link Node} to hold one.</p>
     *
     * @return the action to take if a value identifying with this NodeType requests to be shown to a Player
     */
    public Optional<BiConsumer<Player, T>> getVisualizeAction() {
        return Optional.ofNullable(this.visualizeAct);
    }

    // etc

    /**
     * @return the {@link LanguageText} that should be used when representing this {@link NodeType} as {@link Text}
     */
    public LanguageText getHover() {
        return this.hover;
    }

}
