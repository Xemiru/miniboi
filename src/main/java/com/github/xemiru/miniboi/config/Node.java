package com.github.xemiru.miniboi.config;

import com.github.xemiru.miniboi.textmenu.PageDisplayable;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.text.translation.locale.Locales;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * A part of a configuration structure.
 *
 * @param <T> the value held by the Node
 */
public abstract class Node<T> implements PageDisplayable {

    // required
    String id;
    Translatable name;
    NodeType<T> type;

    // optional
    Translatable description = null;
    Function<T, Translatable> readTransform = null; // can depend on type; set in init
    Function<T, List<Translatable>> validation = obj -> Collections.emptyList();
    boolean editable = true;

    // internal
    private T value = null;
    private boolean isDefault = true;
    private SectionNode parent = null;
    private boolean initializing = true;

    Node() {
    }

    //
    // region internal api
    //

    /**
     * Ensures this {@link Node} is correctly built. Called upon by builders of implementing subclasses.
     */
    void initialize() {
        requireNonNull(id, "id");
        requireNonNull(name, "name");
        requireNonNull(type, "type");

        if (this.parent != null) {
            if (this.parent.children.containsKey(this.getId()))
                throw new IllegalStateException(String.format("Conflicting node ID \"%s\" in parent node \"%s\"",
                    this.getId(), this.parent.getId()));
            this.parent.children.put(this.getId(), this);
        }

        if (this.readTransform == null) this.readTransform = obj -> this.type.getReadTransform().apply(obj);
        if (!this.type.isNullable() && this.value == null)
            throw new IllegalStateException("Initial value for non-nullable node type cannot be null");

        this.type.getValidation().apply(this.getValue()).ifPresent(err -> {
            throw new IllegalStateException(String.format("Initial value must be type-valid: %s",
                err.getTranslation().get(Locales.EN_US)));
        });

        this.initializing = false;
    }

    /**
     * Returns a transform function converting the value currently held by this {@link Node} into a human-readable form.
     *
     * <p>When using a nullable type, the transform function should be careful about null values.</p>
     *
     * @return a transform function
     */
    Function<T, Translatable> getReadTransform() {
        return this.readTransform;
    }

    /**
     * Returns the validation function ensuring a value is acceptable by this {@link Node}.
     *
     * <p>The validation function of a Node differs from that of a {@link NodeType} in that the errors are held along
     * various others as an overview as to why a specific value isn't applicable (warnings versus errors). This is
     * useful for Node types that have children values, allowing the parent to flag itself for invalidation and state
     * all issues with its children on its own.</p>
     *
     * <p>When using a nullable type, validations should be careful about null values.</p>
     *
     * @return the validation function ensuring a value is acceptable by this Node
     */
    Function<T, List<Translatable>> getValidation() {
        return this.validation;
    }

    /**
     * @return the full path to this {@link Node} within a configuration file
     */
    String[] getPath() {
        Node<?> current = this;
        List<String> nodes = new ArrayList<>();
        nodes.add(this.getId());

        while (current.getParent().isPresent()) {
            current = current.getParent().get();
            nodes.add(0, current.getId());
        }

        return nodes.toArray(new String[0]);
    }

    /**
     * Sets the parent {@link SectionNode} of this {@link Node}
     *
     * @param node the new parent SectionNode
     */
    void setParent(SectionNode node) {
        this.parent = node;
    }

    /**
     * @return known issues with the current value of this {@link Node} as a result of validation
     */
    List<Translatable> getIssues() {
        return this.getValidation().apply(this.value);
    }

    // endregion

    //
    // public api
    //

    // required

    /**
     * @return the id representing this {@link Node} in a configuration file
     */
    public String getId() {
        return this.id;
    }

    /**
     * @return the {@link NodeType} that the value of this {@link Node} identifies with
     */
    public NodeType<T> getType() {
        return this.type;
    }

    /**
     * @return the human-readable name of this {@link Node}
     */
    public Translatable getName() {
        return this.name;
    }

    // optional

    /**
     * @return the description of the value held by this {@link Node}
     */
    public Optional<Translatable> getDescription() {
        return Optional.ofNullable(this.description);
    }

    /**
     * Returns whether or not the value of this {@link Node} is changeable through {@link Player} interaction.
     *
     * <p>By default, nodes are editable.</p>
     *
     * @return whether or not the value of this Node is changeable through Player interaction
     */
    public boolean isEditable() {
        return this.editable;
    }

    /**
     * @return the parent {@link SectionNode} of this Node
     */
    public Optional<SectionNode> getParent() {
        return Optional.ofNullable(this.parent);
    }

    // other

    /**
     * @return the current value held by this {@link Node}
     */
    T getValue() {
        return this.value;
    }

    /**
     * Sets the value currently held by this {@link Node}.
     *
     * @param value the new value
     */
    void setValue(T value) {
        if (!this.initializing) {
            if (this.type.isNullable() || this.value != null) {
                Translatable err = this.type.getValidation().apply(value).orElse(null);
                if (err != null) throw new IllegalArgumentException(String.format("Value must be type-valid: %s",
                    err.getTranslation().get(Locales.EN_US)));
            }

            this.isDefault = false;
        }

        this.value = value;
    }

    /**
     * @return whether or not this {@link Node} is currently holding its default value (specifically, if
     *     {@link #setValue(Object)} has yet to be called on it)
     */
    public boolean isDefault() {
        return this.isDefault;
    }

    /**
     * @return whether this {@link Node} is considered valid -- specifically, if {@link #getIssues()} is empty
     */
    public boolean isValid() {
        return this.getIssues().isEmpty();
    }

    //
    // implemented methods
    //

    /**
     * Attempts to load a value into this {@link Node} from a given {@link ConfigurationNode}.
     *
     * <p>Should loading fail, a {@link ConfigurationException} is thrown.</p>
     *
     * @param from the ConfigurationNode to load from
     * @throws ConfigurationException if loading fails
     */
    public abstract void load(ConfigurationNode from);

    /**
     * Saves the contents of this {@link Node} to a given {@link ConfigurationNode}.
     *
     * @param to the ConfigurationNode to write to
     */
    public abstract void save(ConfigurationNode to);

}
