package com.github.xemiru.miniboi.config;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.language.LiteralTranslatable;
import com.github.xemiru.miniboi.language.TextBuilder;
import com.github.xemiru.miniboi.textmenu.PageDisplayable;
import com.github.xemiru.miniboi.textmenu.TextMenu;
import com.github.xemiru.miniboi.util.TextUtil;
import ninja.leaping.configurate.ConfigurationNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.text.translation.locale.Locales;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * A {@link Node} containing a List value.
 *
 * @param <T> the type of the elements contained by the ListNode
 */
public final class ListNode<T> extends Node<List<T>> {

    private static final Logger LOG = LoggerFactory.getLogger(ListNode.class);

    // internal

    /**
     * Creates a {@link NodeType} specifically identifying a {@link ListNode} instance.
     *
     * @param node the ListNode to create the type for
     * @param <T> the type of the elements to be contained by the ListNode
     * @return a NodeType
     */
    private static <T> NodeType<List<T>> createListType(ListNode<T> node) {
        return NodeType.<List<T>>builder()
            .setName(node.name)
            .setDescription(node.description)
            .setSerializer(obj -> null)
            .setDeserializer(obj -> null)
            .setValidation(list -> {
                // could only really happen in code, not through user; don't need to translate
                for (T element : list) {
                    if (node.elementType.isNullable() || element != null) {
                        Translatable err = node.elementType.getValidation().apply(element).orElse(null);
                        if (err != null)
                            return Optional.of(LiteralTranslatable.of(
                                String.format("All elements of list must be type-valid: %s",
                                    err.getTranslation().get(Locales.EN_US))));
                    }
                }

                return Optional.empty();
            })
            .build();
    }

    // public api

    /**
     * Returns a builder used to create instances of {@link ListNode}s.
     *
     * @param parent a parent {@link SectionNode} to attach to
     * @param elementType the type of the elements to be contained by the new ListNode
     * @param <T> the type of the elements to be contained by the new ListNode
     * @return a builder
     */
    public static <T> Builder<T> builder(SectionNode parent, NodeType<T> elementType) {
        return new Builder<>(parent, requireNonNull(elementType, "element type"));
    }

    /**
     * Returns a builder used to create instances of {@link ListNode}s.
     *
     * @param elementType the type of the elements to be contained by the ListNode
     * @param <T> the type of the elements to be contained by the ListNode
     * @return a builder
     */
    public static <T> Builder<T> builder(NodeType<T> elementType) {
        return builder(null, requireNonNull(elementType, "element type"));
    }

    /**
     * Builder class for {@link ListNode}s.
     */
    public static class Builder<T> {

        private ListNode<T> obj;

        private Builder(SectionNode parent, NodeType<T> type) {
            this.obj = new ListNode<>();
            obj.setParent(parent);

            obj.setValue(new ArrayList<>());
            obj.elementType = requireNonNull(type, "type");
        }

        // node's required stuff

        /**
         * @see Node#getId()
         */
        public Builder<T> setId(String id) {
            obj.id = requireNonNull(id);
            return this;
        }

        /**
         * @see Node#getName()
         */
        public Builder<T> setName(Translatable name) {
            obj.name = requireNonNull(name);
            return this;
        }

        // node's optional stuff

        /**
         * @see Node#getDescription()
         */
        public Builder<T> setDescription(Translatable description) {
            obj.description = description;
            return this;
        }

        /***
         * @see Node#getValidation()
         */
        public Builder<T> setValidation(Function<List<T>, List<Translatable>> validation) {
            obj.validation = requireNonNull(validation);
            return this;
        }

        /**
         * A shortcut method allowing one to set a validation function that produces a single error instead of a list.
         *
         * @see Node#getValidation()
         */
        public Builder<T> setValidationSimple(Function<List<T>, Optional<Translatable>> validation) {
            requireNonNull(validation);
            obj.validation = obj -> validation.apply(obj)
                .map(Collections::singletonList)
                .orElse(Collections.emptyList());
            return this;
        }

        /***
         * @see Node#isEditable()
         */
        public Builder<T> setEditable(boolean flag) {
            obj.editable = flag;
            return this;
        }

        /**
         * @see Node#getValue()
         * @see Node#isDefault()
         */
        public Builder<T> setInitialValue(List<T> value) {
            obj.setValue(requireNonNull(value));
            return this;
        }

        // actual stuff

        /**
         * Sets the transform function used to convert elements of this {@link ListNode} into a readable form.
         *
         * @param readTransform the read transform function
         * @return this Builder for chaining
         */
        public Builder<T> setElementReadTransform(Function<T, Translatable> readTransform) {
            obj.elementReadTransform = requireNonNull(readTransform);
            return this;
        }

        /**
         * Sets the validation function used for elements stored in this {@link ListNode}.
         *
         * @param validation the element validation function
         * @return this Builder, for chaining
         */
        public Builder<T> setElementValidation(Function<T, List<Translatable>> validation) {
            obj.elementValidation = requireNonNull(validation);
            return this;
        }

        /**
         * Sets the validation function used for elements stored in this {@link ListNode}.
         *
         * @param validation the element validation function
         * @return this Builder, for chaining
         */
        public Builder<T> setElementValidationSimple(Function<T, Optional<Translatable>> validation) {
            requireNonNull(validation);
            obj.elementValidation = list -> validation.apply(list)
                .map(Collections::singletonList)
                .orElse(Collections.emptyList());
            return this;
        }

        /**
         * Returns a new {@link ListNode} with the properties described in this {@link Builder}.
         *
         * @return the new ListNode
         */
        public ListNode<T> build() {
            obj.type = ListNode.createListType(obj);
            obj.hover = LanguageText.builder(Language.MAX_WRAP_LENGTH)
                .append("[", obj.getName(), "]")

                .line().format(Language.FORMAT_INFO)
                .append(obj.getDescription().orElse(Language.GENERIC_NODESC))

                .line(2).reset()
                .adaptive(() -> LanguageText.builder().translated(Language.NODE_LIST_HOLDING, obj.getValue().size()).build())
                .line()
                .adaptive(() -> {
                    List<Translatable> issues = obj.getIssues();
                    if (issues.isEmpty()) return LanguageText.of(Language.NODE_GENERIC_ISSUES_NONE);
                    else {
                        TextBuilder tb = LanguageText.builder()
                            .format(Language.FORMAT_ERROR)
                            .translated(Language.NODE_LIST_ISSUES, issues.size());

                        issues.forEach(issue -> tb.line().append(issue));
                        return tb.build();
                    }
                }).build();

            obj.initMenu();
            obj.initialize();
            return this.obj;
        }

    }

    LanguageText hover;
    private TextMenu menu;
    private NodeType<T> elementType;
    private Function<T, Translatable> elementReadTransform = obj -> this.elementType.getReadTransform().apply(obj);
    private Function<T, List<Translatable>> elementValidation = obj -> Collections.emptyList();

    private ListNode() {
    }

    private void initMenu() {
        this.menu = TextMenu.builder()
            .setHeader(() -> LanguageText.builder()
                .hover(this.hover)
                .translated(this.getName())
                .append(" <")
                .translated(this.getElementType().getName())
                .append(" / ", super.getValue().size(), ">")
                .build())
            .configureBody(body -> body.addDynamicElements((player, page) -> {
                List<LanguageText> elements = new ArrayList<>();
                if (super.getValue().isEmpty()) elements.add(Language.info(Language.NODE_LIST_EMPTY));
                else super.getValue().forEach(item -> {
                    List<Translatable> issues = this.elementValidation.apply(item);
                    LanguageText issueHover = TextUtil.createIssueHover(issues);
                    boolean visualizable = this.elementType.getVisualizeAction().isPresent();

                    elements.add(LanguageText.builder()
                        .format(Language.FORMAT_ACTIVE)
                        .hover(Language.hover(Language.NODE_LIST_DELETE_LABEL, Language.NODE_LIST_DELETE_DESC))
                        .action(p -> {
                            if (super.getValue().remove(item))
                                Language.msgPrompt(Language.NODE_LIST_DELETE_SUCCESS).send(p);
                            else Language.msgError(Language.NODE_LIST_DELETE_FAILURE).send(p);
                        })
                        .translated(Language.NODE_LIST_DELETE_BUTTON)

                        .reset()
                        .append(" ")

                        .format(issues.isEmpty() ? Language.FORMAT_INFO : Language.FORMAT_ERROR)
                        .hover(LanguageText.builder(40)
                            .translated(visualizable ? Language.NODE_GENERIC_VISUALIZE_AVAILABLE : Language.NODE_GENERIC_VISUALIZE_UNAVAILABLE)
                            .line(2)
                            .append(issueHover)
                            .build())
                        .action(p -> {
                            try {
                                this.elementType.getVisualizeAction().ifPresent(it -> it.accept(p, item));
                            } catch(Exception e) {
                                LOG.error("Failed to execute visualize action for list node element", e);
                                Language.msgError(Language.NODE_GENERIC_VISUALIZE_INTERNALERR).send(p);
                            }
                        })
                        .append(this.elementReadTransform.apply(item))
                        .build());
                });

                return elements;
            }))
            .configureFooter(footer -> footer.addDynamicElements((player, page) -> {
                return Collections.singletonList(LanguageText.builder()
                    .format(this.isEditable() ? Language.FORMAT_ACTIVE : Language.FORMAT_INACTIVE)
                    .hover(Language.hover(Language.NODE_LIST_ADD_LABEL,
                        this.isEditable() ? Language.NODE_LIST_ADD_DESC_EDITABLE : Language.NODE_LIST_ADD_DESC_NONEDITABLE))
                    .action(this.isEditable() ? p ->
                        this.elementType.getEditAction().ifPresent(it -> it.edit(p, item -> {
                            try {
                                if (item == null && !this.elementType.isNullable())
                                    throw new IllegalArgumentException("Element type is non-nullable");
                                Optional<Translatable> typeErr = this.elementType.getValidation().apply(item);
                                if (typeErr.isPresent()) return Collections.singletonList(typeErr.get());

                                List<Translatable> elemErr = this.elementValidation.apply(item);
                                if (!elemErr.isEmpty()) return elemErr;
                                super.getValue().add(item);

                                // using a positive oob page will default to showing the last valid page
                                this.display(p, Integer.MAX_VALUE);
                                return Collections.emptyList();
                            } catch(Exception e) {
                                LOG.error("Failed to execute edit action for list node", e);
                                Language.msgError(Language.NODE_GENERIC_EDIT_INTERNALERR).send(p);
                                return Collections.emptyList();
                            }
                        }, cancelReason -> {
                            this.display(p, page);
                        })) : null)
                    .append("[+]")

                    .reset()
                    .append(" ")
                    .format(Language.FORMAT_INFO)
                    .append(Language.NODE_LIST_ADD_TEXT)
                    .build());
            }))
            .build();
    }

    /**
     * @return the type of the elements contained by this {@link ListNode}
     */
    public NodeType<T> getElementType() {
        return this.elementType;
    }

    /**
     * Returns the backing list of this {@link ListNode}.
     *
     * <p>Note that this method returns a copy of the backing list, as it should not be modified directly. Changes to
     * the list should be sent back to the value through a call to {@link #setValue(Object)}.</p>
     *
     * @return a copy of this ListNode's backing List
     */
    @Override
    public List<T> getValue() {
        return new ArrayList<>(super.getValue());
    }

    @Override
    public void setValue(List<T> value) {
        super.setValue(value);
    }

    @Override
    public List<Translatable> getIssues() {
        List<T> items = super.getValue();
        List<Translatable> issues = new ArrayList<>(this.getValidation().apply(items));

        int itemIssues = 0;
        for (T item : items)
            if (!this.elementValidation.apply(item).isEmpty()) itemIssues++;

        if (itemIssues > 0) issues.add(Language.NODE_LIST_ISSUES.withParameters(itemIssues));
        return issues;
    }

    @Override
    public boolean isEditable() {
        return this.elementType.getEditAction().isPresent() && super.isEditable();
    }

    @Override
    public void load(ConfigurationNode from) {
        if (from.isVirtual()) return;
        if (from.hasListChildren()) {
            List<T> newList = new ArrayList<>();
            from.getChildrenList().forEach(element ->
                newList.add(this.getElementType().getDeserializer().apply(element.getValue())));

            this.setValue(newList);
        } else throw new SerializationException("ConfigurationNode was not a list type");
    }

    @Override
    public void save(ConfigurationNode to) {
        List<Object> saved = new ArrayList<>();
        super.getValue().forEach(element -> saved.add(this.getElementType().getSerializer().apply(element)));
        to.setValue(saved);
    }

    @Override
    public void display(PageDisplayable parent, Player to, int page) {
        this.menu.display(parent, to, page);
    }

}
