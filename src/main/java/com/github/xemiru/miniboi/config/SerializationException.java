package com.github.xemiru.miniboi.config;

/**
 * Exception denoting an error occurring during the serialization/deserialization process of a {@link NodeType}'s
 * value.
 */
public class SerializationException extends ConfigurationException {

    public SerializationException(String message) {
        super(message);
    }

}
