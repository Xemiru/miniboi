package com.github.xemiru.miniboi.config;

import com.github.xemiru.miniboi.Miniboi;
import com.github.xemiru.miniboi.input.ChoiceInputHandler;
import com.github.xemiru.miniboi.input.InputHandlers;
import com.github.xemiru.miniboi.language.ComposedTranslatable;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LiteralTranslatable;
import com.github.xemiru.miniboi.util.Choice;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.translation.Translatable;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * A collection of stock {@link NodeType}s.
 *
 * <p>By default, all NodeTypes declared or generated through the contents of this class are non-nullable. They can be
 * made nullable by being passed through the {@link #NULLABLE(NodeType)} generator.</p>
 */
@SuppressWarnings({ "unused", "WeakerAccess" })
public class NodeTypes {

    /**
     * Copies the properties of the provided model {@link NodeType} into a {@link NodeType.Builder} instance.
     *
     * @param model the NodeType to model
     * @param <T> the value type
     * @return a NodeType.Builder
     */
    private static <T> NodeType.Builder<T> copy(NodeType<T> model) {
        return NodeType.<T>builder()
            .setName(model.getName())
            .setDescription(model.getDescription().orElse(Language.GENERIC_NODESC))
            .setSerializer(model.getSerializer())
            .setDeserializer(model.getDeserializer())
            .setReadableTransform(model.getReadTransform())
            .setVisualizeAction(model.getVisualizeAction().orElse(null))
            .setEditAction(model.getEditAction().orElse(null))
            .setNullable(model.isNullable());
    }

    /**
     * Creates a nullable version of the provided model {@link NodeType}.
     *
     * @param model the NodeType to model
     * @param <T> the value type
     * @return a NodeType
     */
    public static <T> NodeType<T> NULLABLE(NodeType<T> model) {
        return copy(model)
            .setSerializer(obj -> {
                if (obj != null) return model.getSerializer().apply(obj);
                return null;
            })
            .setReadableTransform(obj -> {
                if (obj != null) return model.getReadTransform().apply(obj);
                return ComposedTranslatable.of("<", Language.NODE_GENERIC_NOVALUE, ">");
            })
            .setVisualizeAction(model.getVisualizeAction().map(vis -> (BiConsumer<Player, T>) (p, obj) -> {
                if (obj != null) vis.accept(p, obj);
                else Language.msgError(Language.NODE_GENERIC_NULLVISUALIZATION).send(p);
            }).orElse(null))
            .setNullable(true)
            .build();
    }

    /**
     * Creates a {@link NodeType} based off of the provided model type, restricted to a provided set of choices.
     *
     * @param model the NodeType to model
     * @param choices the set of choices
     * @param <T> the value type
     * @return a NodeType
     */
    public static <T> NodeType<T> CHOICE(NodeType<T> model, Choice<T>... choices) {
        List<Choice<T>> choiceList = Arrays.asList(choices);
        return NodeTypes.CHOICE(model, () -> choiceList);
    }

    /**
     * Creates a {@link NodeType} based off of the provided model type, restricted to a dynamically-generated set of
     * choices.
     *
     * @param model the NodeType to model
     * @param choices the function generating a selection of possible values
     * @param <T> the value type
     * @return a NodeType
     */
    public static <T> NodeType<T> CHOICE(NodeType<T> model, Supplier<Collection<Choice<T>>> choices) {
        return copy(model)
            .setEditAction((p, submit, cancel) ->
                Miniboi.getInstance().getInputManager().query(p, InputHandlers.<T>newChoiceHandler()
                    .addChoices(choices.get())
                    .setConsumer(submit)
                    .setCancelHandler(cancel)))
            .build();
    }

    /**
     * Creates a {@link NodeType} based on the possible values of the provided enum type.
     *
     * <p>The type name will become the class name of the provided type, with no description.</p>
     *
     * @param enumType the enum type to base off of
     * @param <T> the enum type to base off of
     * @return a NodeType of the provided enum type
     */
    public static <T extends Enum<?>> NodeType<T> ENUM(Class<T> enumType) {
        return NodeTypes.ENUM(enumType, LiteralTranslatable.of(enumType.getSimpleName()));
    }

    /**
     * Creates a {@link NodeType} based on the possible values of the provided enum type.
     *
     * <p>The description of the type will be set to "No description."</p>
     *
     * @param enumType the enum type to base off of
     * @param title the name of the type
     * @param <T> the enum type to base off of
     * @return a NodeType of the provided enum type
     */
    public static <T extends Enum<?>> NodeType<T> ENUM(Class<T> enumType, Translatable title) {
        return NodeTypes.ENUM(enumType, title, Language.GENERIC_NODESC);
    }

    /**
     * Creates a {@link NodeType} based on the possible values of the provided enum type.
     *
     * @param enumType the enum type to base off of
     * @param title the name of the type
     * @param description the description of the type
     * @param <T> the enum type to base off of
     * @return a NodeType of the provided enum type
     */
    public static <T extends Enum<?>> NodeType<T> ENUM(Class<T> enumType, Translatable title, Translatable description) {
        return NodeTypes.ENUM(enumType, title, description, member ->
            new Translatable[] { LiteralTranslatable.of(member.name()), Language.GENERIC_NODESC });
    }

    /**
     * Creates a {@link NodeType} based on the possible values of the provided enum type.
     *
     * <p>The meta-generating function should return a Translatable array of length 2, containing the name and the
     * description of the enum value provided.</p>
     *
     * @param enumType the enum type to base off of
     * @param title the name of the type
     * @param description the description of the type
     * @param metaGen a function generating the name and description for each value of the enum type
     * @param <T> the enum type to base off of
     * @return a NodeType of the provided enum type
     */
    public static <T extends Enum<?>> NodeType<T> ENUM(Class<T> enumType, Translatable title, Translatable description, Function<T, Translatable[]> metaGen) {
        return NodeType.<T>builder()
            .setName(title)
            .setDescription(description)
            .setSerializer(member -> member.name())
            .setDeserializer(name -> {
                for (T element : enumType.getEnumConstants())
                    if (element.name().equals(name)) return element;

                throw new SerializationException(String.format("Unknown enum value for enum class %s: %s", enumType.getSimpleName(), name));
            })
            .setReadableTransform(element -> metaGen.apply(element)[0])
            .setEditAction((player, submit, cancel) -> {
                ChoiceInputHandler<T> choice = InputHandlers.newChoiceHandler();
                for (T element : enumType.getEnumConstants()) {
                    Translatable[] meta = metaGen.apply(element);
                    choice.addChoices(Choice.of(element, meta[0], meta[1]));
                }

                choice.setConsumer(submit)
                    .setCancelHandler(cancel);
            })
            .build();
    }

    public static final NodeType<String> STRING = NodeType.<String>builder()
        .setName(Language.NODETYPE_TEXT_LABEL)
        .setDescription(Language.NODETYPE_TEXT_DESC)
        .setSerializer(obj -> obj)
        .setDeserializer(Object::toString)
        .setReadableTransform(LiteralTranslatable::of)
        .setVisualizeAction(null)
        .setEditAction((p, submit, cancel) ->
            Miniboi.getInstance().getInputManager().query(p, InputHandlers.newChatHandler()
                .setPrompt(Language.prompt(Language.NODETYPE_TEXT_PROMPT))
                .setConsumer(submit)
                .setCancelHandler(cancel)))
        .build();

    public static final NodeType<Integer> INTEGER = NodeType.<Integer>builder()
        .setName(Language.NODETYPE_INT_LABEL)
        .setDescription(Language.NODETYPE_INT_DESC)
        .setSerializer(obj -> obj)
        .setDeserializer(obj -> (Integer) obj)
        .setReadableTransform(obj -> LiteralTranslatable.of(obj.toString()))
        .setVisualizeAction(null)
        .setEditAction((p, submit, cancel) ->
            Miniboi.getInstance().getInputManager().query(p, InputHandlers.newChatHandler(Integer::parseInt)
                .withInputValidation(input -> {
                    try {
                        Integer.parseInt(input);
                        return Optional.empty();
                    } catch (NumberFormatException e) {
                        return Optional.of(Language.GENERIC_NAN.withParameters(input));
                    }
                })
                .setPrompt(Language.prompt(Language.NODETYPE_INT_PROMPT))
                .setConsumer(submit)
                .setCancelHandler(cancel)))
        .build();

    public static NodeType<Integer> BOUNDED_INTEGER(int lower, int upper) {
        if (lower > upper) throw new IllegalArgumentException("lower must be lower than upper");

        return NodeType.<Integer>builder()
            .setName(Language.NODETYPE_BOUNDINT_LABEL)
            .setDescription(Language.NODETYPE_BOUNDINT_DESC.withParameters(lower, upper))
            .setSerializer(obj -> obj)
            .setDeserializer(obj -> (int) obj)
            .setReadableTransform(obj -> LiteralTranslatable.of(obj.toString()))
            .setVisualizeAction(null)
            .setValidation(i -> {
                if (i < lower || i > upper)
                    return Optional.of(Language.NODETYPE_BOUNDINT_VALIDATION_OOB.withParameters(lower, upper));
                return Optional.empty();
            })
            .setEditAction((p, submit, cancel) ->
                Miniboi.getInstance().getInputManager().query(p, InputHandlers.newChatHandler(Integer::parseInt)
                    .withInputValidation(input -> {
                        try {
                            Integer.parseInt(input);
                            return Optional.empty();
                        } catch (NumberFormatException e) {
                            return Optional.of(Language.GENERIC_NAN.withParameters(input));
                        }
                    })
                    .setPrompt(Language.prompt(Language.NODETYPE_BOUNDINT_PROMPT.withParameters(lower, upper)))
                    .setConsumer(submit)
                    .setCancelHandler(cancel)))
            .build();
    }

    public static final NodeType<Double> DOUBLE = NodeType.<Double>builder()
        .setName(Language.NODETYPE_DOUBLE_LABEL)
        .setDescription(Language.NODETYPE_DOUBLE_DESC)
        .setSerializer(obj -> obj)
        .setDeserializer(obj -> (double) obj)
        .setReadableTransform(obj -> LiteralTranslatable.of(obj.toString()))
        .setVisualizeAction(null)
        .setEditAction((p, submit, cancel) ->
            Miniboi.getInstance().getInputManager().query(p, InputHandlers.newChatHandler(Double::parseDouble)
                .withInputValidation(input -> {
                    try {
                        Double.parseDouble(input);
                        return Optional.empty();
                    } catch (NumberFormatException e) {
                        return Optional.of(Language.GENERIC_NANDOUBLE.withParameters(input));
                    }
                })
                .setPrompt(Language.prompt(Language.NODETYPE_DOUBLE_PROMPT))
                .setConsumer(submit)
                .setCancelHandler(cancel)))
        .build();

    public static NodeType<Double> BOUNDED_DOUBLE(double lower, double upper) {
        if (lower > upper) throw new IllegalArgumentException("lower must be lower than upper");

        return NodeType.<Double>builder()
            .setName(Language.NODETYPE_BOUNDDOUBLE_LABEL)
            .setDescription(Language.NODETYPE_BOUNDDOUBLE_DESC.withParameters(lower, upper))
            .setSerializer(obj -> obj)
            .setDeserializer(obj -> (double) obj)
            .setReadableTransform(obj -> LiteralTranslatable.of(obj.toString()))
            .setVisualizeAction(null)
            .setValidation(num -> {
                if (num < lower || num > upper)
                    return Optional.of(Language.NODETYPE_BOUNDDOUBLE_VALIDATION_OOB.withParameters(lower, upper));
                return Optional.empty();
            })
            .setEditAction((p, submit, cancel) ->
                Miniboi.getInstance().getInputManager().query(p, InputHandlers.newChatHandler(Double::parseDouble)
                    .withInputValidation(input -> {
                        try {
                            Double.parseDouble(input);
                            return Optional.empty();
                        } catch (NumberFormatException e) {
                            return Optional.of(Language.GENERIC_NANDOUBLE.withParameters(input));
                        }
                    })
                    .setPrompt(Language.prompt(Language.NODETYPE_BOUNDDOUBLE_PROMPT.withParameters(lower, upper)))
                    .setConsumer(submit)
                    .setCancelHandler(cancel)))
            .build();
    }

    public static final NodeType<Long> LONG = NodeType.<Long>builder()
        .setName(Language.NODETYPE_INT_LABEL)
        .setDescription(Language.NODETYPE_INT_DESC)
        .setSerializer(obj -> obj)
        .setDeserializer(obj -> (long) obj)
        .setReadableTransform(obj -> LiteralTranslatable.of(obj.toString()))
        .setVisualizeAction(null)
        .setEditAction((p, submit, cancel) ->
            Miniboi.getInstance().getInputManager().query(p, InputHandlers.newChatHandler(Long::parseLong)
                .withInputValidation(input -> {
                    try {
                        Integer.parseInt(input);
                        return Optional.empty();
                    } catch (NumberFormatException e) {
                        return Optional.of(Language.GENERIC_NAN.withParameters(input));
                    }
                })
                .setPrompt(Language.prompt(Language.NODETYPE_INT_PROMPT))
                .setConsumer(submit)
                .setCancelHandler(cancel)))
        .build();

    public static NodeType<Long> BOUNDED_LONG(long lower, long upper) {
        if (lower > upper) throw new IllegalArgumentException("lower must be lower than upper");

        return NodeType.<Long>builder()
            .setName(Language.NODETYPE_BOUNDINT_LABEL)
            .setDescription(Language.NODETYPE_BOUNDINT_DESC.withParameters(lower, upper))
            .setSerializer(obj -> obj)
            .setDeserializer(obj -> (long) obj)
            .setReadableTransform(obj -> LiteralTranslatable.of(obj.toString()))
            .setVisualizeAction(null)
            .setValidation(i -> {
                if (i < lower || i > upper)
                    return Optional.of(Language.NODETYPE_BOUNDINT_VALIDATION_OOB.withParameters(lower, upper));
                return Optional.empty();
            })
            .setEditAction((p, submit, cancel) ->
                Miniboi.getInstance().getInputManager().query(p, InputHandlers.newChatHandler(Long::parseLong)
                    .withInputValidation(input -> {
                        try {
                            Integer.parseInt(input);
                            return Optional.empty();
                        } catch (NumberFormatException e) {
                            return Optional.of(Language.GENERIC_NAN.withParameters(input));
                        }
                    })
                    .setPrompt(Language.prompt(Language.NODETYPE_BOUNDINT_PROMPT.withParameters(lower, upper)))
                    .setConsumer(submit)
                    .setCancelHandler(cancel)))
            .build();
    }

    public static NodeType<Boolean> BOOLEAN = NodeType.<Boolean>builder()
        .setName(Language.NODETYPE_BOOLEAN_LABEL)
        .setDescription(Language.NODETYPE_BOOLEAN_DESC)
        .setSerializer(obj -> obj)
        .setDeserializer(obj -> (boolean) obj)
        .setReadableTransform(bool -> bool ? Language.NODETYPE_BOOLEAN_VALUE_TRUE : Language.NODETYPE_BOOLEAN_VALUE_FALSE)
        .setVisualizeAction(null)
        .setEditAction((p, submit, cancel) ->
            Miniboi.getInstance().getInputManager().query(p, InputHandlers.<Boolean>newChoiceHandler()
                .addChoices(
                    Choice.of(true, Language.NODETYPE_BOOLEAN_VALUE_TRUE),
                    Choice.of(false, Language.NODETYPE_BOOLEAN_VALUE_FALSE))
                .setConsumer(submit)
                .setCancelHandler(cancel)))
        .build();
}
