package com.github.xemiru.miniboi.config;

import com.github.xemiru.miniboi.input.InputHandler;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.textmenu.PageDisplayable;
import com.github.xemiru.miniboi.textmenu.TextMenu;
import com.github.xemiru.miniboi.util.TextUtil;
import ninja.leaping.configurate.ConfigurationNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;
import org.spongepowered.api.text.translation.Translatable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

/**
 * A {@link Node} holding a single value.
 *
 * @param <T> the type of the held value
 */
public final class ValueNode<T> extends Node<T> {

    private static final Logger LOG = LoggerFactory.getLogger(ValueNode.class);

    /**
     * Returns a builder used to create instances of {@link ValueNode}s.
     *
     * <p>Value nodes will by default copy the name and description of the {@link NodeType} they're assigned.</p>
     *
     * @param valueType the type of the value to be held by the ValueNode
     * @param <T> the type of the value to be held by the ValueNode
     * @return a builder
     */
    public static <T> Builder<T> builder(NodeType<T> valueType) {
        return builder(null, valueType);
    }

    /**
     * Returns a builder used to create instances of {@link ValueNode}s.
     *
     * <p>Value nodes will by default copy the name and description of the {@link NodeType} they're assigned.</p>
     *
     * @param parent a parent {@link SectionNode} to attach to
     * @param valueType the type of the value to be held by the ValueNode
     * @param <T> the type of the value to be held by the ValueNode
     * @return a builder
     */
    public static <T> Builder<T> builder(SectionNode parent, NodeType<T> valueType) {
        return new Builder<>(parent, requireNonNull(valueType, "valueType"));
    }

    public static class Builder<T> {

        private ValueNode<T> obj;

        private Builder(SectionNode parent, NodeType<T> type) {
            this.obj = new ValueNode<>();
            this.obj.setParent(parent);

            obj.type = type;
            obj.setValue(null);

            this.setName(type.getName());
            type.getDescription().ifPresent(this::setDescription);
        }

        // node's required stuff

        /**
         * @see Node#getId()
         */
        public Builder<T> setId(String id) {
            obj.id = requireNonNull(id);
            return this;
        }

        /**
         * @see Node#getName()
         */
        public Builder<T> setName(Translatable name) {
            obj.name = requireNonNull(name);
            return this;
        }

        // node's optional stuff

        /**
         * @see Node#getDescription()
         */
        public Builder<T> setDescription(Translatable description) {
            obj.description = description;
            return this;
        }

        /***
         * @see Node#getReadTransform()
         */
        public Builder<T> setReadTransform(Function<T, Translatable> readTransform) {
            obj.readTransform = requireNonNull(readTransform);
            return this;
        }

        /***
         * @see Node#getValidation()
         */
        public Builder<T> setValidation(Function<T, List<Translatable>> validation) {
            obj.validation = requireNonNull(validation);
            return this;
        }

        /**
         * A shortcut method allowing one to set a validation function that produces a single error instead of a list.
         *
         * @see Node#getValidation()
         */
        public Builder<T> setValidationSimple(Function<T, Optional<Translatable>> validation) {
            requireNonNull(validation);
            obj.validation = obj -> validation.apply(obj)
                .map(Collections::singletonList)
                .orElse(Collections.emptyList());
            return this;
        }

        /**
         * @see Node#isEditable()
         */
        public Builder<T> setEditable(boolean flag) {
            obj.editable = flag;
            return this;
        }

        /**
         * @see Node#getValue()
         * @see Node#isDefault()
         */
        public Builder<T> setInitialValue(T value) {
            obj.setValue(value);
            return this;
        }

        /**
         * Returns a new {@link ValueNode} with the properties described in this {@link Builder}.
         *
         * @return the new ValueNode
         */
        public ValueNode<T> build() {
            obj.hover = LanguageText.builder(Language.MAX_WRAP_LENGTH)
                .append("[", obj.getName(), "]")
                .line()
                .format(Language.FORMAT_INFO)
                .translated(obj.getDescription().orElse(Language.GENERIC_NODESC))
                .line(2)
                .adaptive(() -> TextUtil.createIssueHover(obj.getIssues()))
                .build();
            obj.menu = TextMenu.builder()
                .setParent(obj.getParent().orElse(null))
                .setHeader(LanguageText.builder()
                    .hover(obj.hover)
                    .append("[", Language.NODE_VALUE_LABEL, "] ", obj.getName())
                    .build())
                .configureBody(body -> body
                    .addStaticElements(LanguageText.builder()
                        .format(Language.FORMAT_INFO)
                        .append(obj.getDescription().orElse(Language.GENERIC_NODESC))
                        .build())
                    .addDynamicElements((player, page) -> {
                        List<LanguageText> list = new ArrayList<>();
                        LanguageText issueHover = TextUtil.createIssueHover(obj.getIssues());
                        boolean visualizable = obj.getType().getVisualizeAction().isPresent();

                        Collections.addAll(list,
                            LanguageText.builder() // Editable
                                .format(Language.FORMAT_SUBTITLE)
                                .append(Language.NODE_VALUE_EDITABLE_LABEL, ": ")

                                .color(obj.isEditable() ? TextColors.GREEN : TextColors.DARK_GRAY)
                                .style(TextStyles.ITALIC)
                                .hover(Language.hover(Language.NODE_GENERIC_EDIT_LABEL, obj.isEditable() ?
                                    Language.NODE_GENERIC_EDIT_DESC_EDITABLE : Language.NODE_GENERIC_EDIT_DESC_NONEDITABLE))
                                .action(obj.isEditable() ? p -> obj.performEdit(p, value -> obj.display(p, page), reason -> obj.display(p, page)) : null)
                                .append(obj.isEditable() ? Language.NODE_VALUE_CLICKABLEYES : Language.GENERIC_NO)
                                .build(),
                            LanguageText.builder() // Visualizable
                                .format(Language.FORMAT_SUBTITLE)
                                .append(Language.NODE_VALUE_VISUALIZE_LABEL, ": ")

                                .color(obj.isEditable() ? TextColors.GREEN : TextColors.DARK_GRAY)
                                .style(TextStyles.ITALIC)
                                .hover(Language.hover(Language.NODE_VALUE_VISUALIZE_LABEL, visualizable ?
                                    Language.NODE_GENERIC_VISUALIZE_AVAILABLE : Language.NODE_GENERIC_VISUALIZE_UNAVAILABLE))
                                .action(visualizable ? obj::visualize : null)
                                .append(visualizable ? Language.NODE_VALUE_CLICKABLEYES : Language.GENERIC_NO)
                                .build(),
                            LanguageText.builder().build(), // spacer
                            LanguageText.builder() // id
                                .format(Language.FORMAT_SUBTITLE)
                                .append(Language.NODE_VALUE_ID_LABEL, ": ")

                                .format(Language.FORMAT_INFO)
                                .style(TextStyles.ITALIC)
                                .append(obj.getId())
                                .build(),
                            LanguageText.builder() // type
                                .format(Language.FORMAT_SUBTITLE)
                                .append(Language.NODE_VALUE_TYPE_LABEL, ": ")

                                .format(Language.FORMAT_INFO)
                                .style(TextStyles.ITALIC)
                                .hover(obj.getType().getHover())
                                .append(obj.getType().getName())
                                .build(),
                            LanguageText.builder() // current val
                                .format(Language.FORMAT_SUBTITLE)
                                .append(Language.NODE_VALUE_CURRENT_LABEL, ": ")

                                .format(obj.isValid() ? Language.FORMAT_INFO : Language.FORMAT_ERROR)
                                .style(TextStyles.ITALIC)
                                .hover(issueHover)
                                .append(obj.getReadTransform().apply(obj.getValue()))
                                .build());

                        return list;
                    }))
                .build();

            obj.initialize();
            return this.obj;
        }

    }

    LanguageText hover;
    private TextMenu menu;

    private ValueNode() {
    }

    void performEdit(Player editor, Consumer<T> onSuccess, Consumer<InputHandler.CancelReason> onCancel) {
        this.getType().getEditAction().ifPresent(it -> it.edit(editor, newValue -> {
            try {
                Optional<Translatable> err = this.getType().getValidation().apply(newValue);
                if (err.isPresent()) return Collections.singletonList(err.get());

                List<Translatable> err2 = this.getValidation().apply(newValue);
                if (err2 != null && !err2.isEmpty()) return err2;

                this.setValue(newValue);
                onSuccess.accept(newValue);
                return Collections.emptyList();
            } catch (Exception e) {
                LOG.error("Failed to execute edit action for value node", e);
                Language.msgError(Language.NODE_GENERIC_EDIT_INTERNALERR).send(editor);
                return Collections.emptyList();
            }
        }, onCancel));
    }

    void visualize(Player p) {
        try {
            this.getType().getVisualizeAction().ifPresent(vis -> vis.accept(p, this.getValue()));
        } catch (Exception e) {
            LOG.error("Failed to execute visualize action for value node", e);
            Language.msgError(Language.NODE_GENERIC_VISUALIZE_INTERNALERR).send(p);
        }
    }

    @Override
    public T getValue() {
        return super.getValue();
    }

    @Override
    public void setValue(T value) {
        super.setValue(value);
    }

    @Override
    public boolean isEditable() {
        return this.type.getEditAction().isPresent() && super.isEditable();
    }

    @Override
    public void load(ConfigurationNode from) {
        if (!from.isVirtual())
            this.setValue(this.getType().getDeserializer().apply(from.getValue()));
    }

    @Override
    public void save(ConfigurationNode to) {
        if (this.getValue() != null)
            to.setValue(this.getType().getSerializer().apply(this.getValue()));
    }

    @Override
    public void display(PageDisplayable parent, Player to, int page) {
        this.menu.display(parent, to, page);
    }

}
