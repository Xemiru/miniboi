package com.github.xemiru.miniboi.config;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.textmenu.PageDisplayable;
import com.github.xemiru.miniboi.textmenu.TextMenu;
import com.github.xemiru.miniboi.util.TextUtil;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextFormat;
import org.spongepowered.api.text.format.TextStyles;
import org.spongepowered.api.text.translation.Translatable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * A {@link Node} containing a structure of keys and other {@link Node}s.
 */
public final class SectionNode extends Node<Object> {

    private static final Object DUMMY_VALUE = new Object();
    private static final TextFormat FORMAT_VALID = TextFormat.of(TextColors.WHITE);

    /**
     * Returns a builder used to create instances of {@link SectionNode}s.
     *
     * @return a builder
     */
    public static Builder builder() {
        return builder(null);
    }

    /**
     * Returns a builder used to create instances of {@link SectionNode}s.
     *
     * @param parent a parent {@link SectionNode} to attach to
     * @return a builder
     */
    public static Builder builder(SectionNode parent) {
        return new Builder(parent);
    }

    /**
     * Creates a {@link NodeType} specifically identifying a {@link SectionNode} instance.
     *
     * @param node the SectionNode to create the type for
     * @return a NodeType
     */
    private static NodeType<Object> createSectionType(SectionNode node) {
        return NodeType.builder()
            .setName(node.name)
            .setDescription(node.description)
            .setSerializer(obj -> null)
            .setDeserializer(obj -> null)
            .build();
    }

    /**
     * Builder class for {@link SectionNode}s.
     */
    public static class Builder {

        private SectionNode obj;

        private Builder(SectionNode parent) {
            this.obj = new SectionNode();

            this.obj.setParent(parent);
            this.obj.setValue(SectionNode.DUMMY_VALUE);
        }

        // node's required stuff

        /**
         * @see Node#getId()
         */
        public Builder setId(String id) {
            obj.id = requireNonNull(id);
            return this;
        }

        /**
         * @see Node#getName()
         */
        public Builder setName(Translatable name) {
            obj.name = requireNonNull(name);
            return this;
        }

        // node's optional stuff

        /**
         * @see Node#getDescription()
         */
        public Builder setDescription(Translatable description) {
            obj.description = description;
            return this;
        }

        // actual stuff

        /**
         * Returns a new {@link ValueNode} with the properties described in this {@link ValueNode.Builder}.
         *
         * @return the new ValueNode
         */
        public SectionNode build() {
            obj.type = SectionNode.createSectionType(this.obj);
            obj.hover = LanguageText.builder(Language.MAX_WRAP_LENGTH)
                .append("[", obj.getName(), "]")
                .line()
                .format(Language.FORMAT_INFO)
                .translated(obj.getDescription().orElse(Language.GENERIC_NODESC))
                .line(2)
                .adaptive(() -> TextUtil.createIssueHover(obj.getIssues()))
                .build();
            obj.menu = TextMenu.builder()
                .setParent(obj.getParent().orElse(null))
                .setHeader(LanguageText.builder()
                    .hover(obj.hover)
                    .append(obj.getName())
                    .build())
                .configureBody(body -> body.addDynamicElements((player, page) -> {
                    List<LanguageText> elements = new ArrayList<>();
                    if (obj.children.isEmpty()) elements.add(Language.info(Language.NODE_SECTION_EMPTY));
                    else {
                        obj.children.forEach((key, node) -> {
                            if (node instanceof SectionNode)
                                elements.add(LanguageText.builder()
                                    .format(Language.FORMAT_ACTIVE)
                                    .hover(Language.hover(Language.NODE_GENERIC_EDIT_LABEL,
                                        Language.NODE_GENERIC_EDIT_DESC_EDITABLE))
                                    .action(p -> node.display(p, 0))
                                    .append(Language.NODE_SECTION_EDIT_BUTTON)

                                    .reset()
                                    .append(" ")

                                    .format(Language.FORMAT_SUBTITLE)
                                    .append(node.getName(), ": ")

                                    .format(node.isValid() ? FORMAT_VALID : Language.FORMAT_ERROR)
                                    .hover(((SectionNode) node).hover)
                                    .action(p -> node.display(p, 0))
                                    .append("<", Language.NODE_SECTION_LABEL, ">")
                                    .build());
                        });

                        // then lists
                        obj.children.forEach((key, node) -> {
                            if (node instanceof ListNode) {
                                ListNode<?> list = (ListNode<?>) node;
                                elements.add(LanguageText.builder()
                                    .format(Language.FORMAT_ACTIVE)
                                    .hover(Language.hover(Language.NODE_GENERIC_EDIT_LABEL,
                                        Language.NODE_GENERIC_EDIT_DESC_EDITABLE))
                                    .action(p -> node.display(p, 0))
                                    .append(Language.NODE_SECTION_EDIT_BUTTON)

                                    .reset()
                                    .append(" ")

                                    .format(Language.FORMAT_SUBTITLE)
                                    .append(node.getName(), ": ")

                                    .format(node.isValid() ? FORMAT_VALID : Language.FORMAT_ERROR)
                                    .hover(((ListNode) node).hover)
                                    .action(p -> node.display(p, 0))
                                    .append(Language.NODE_LIST_LABEL, " <", list.getElementType().getName(), " / ",
                                        list.getValue().size(), ">")
                                    .build());
                            }
                        });

                        // then values
                        obj.children.forEach((key, node) -> {
                            if (node instanceof ValueNode) {
                                ValueNode value = (ValueNode) node;
                                boolean visualizable = value.getType().getVisualizeAction().isPresent();
                                elements.add(LanguageText.builder()
                                    .format(node.isEditable() ? Language.FORMAT_ACTIVE : Language.FORMAT_INACTIVE)
                                    .hover(Language.hover(Language.NODE_GENERIC_EDIT_LABEL, node.isEditable() ?
                                        Language.NODE_GENERIC_EDIT_DESC_EDITABLE : Language.NODE_GENERIC_EDIT_DESC_NONEDITABLE))
                                    .action(node.isEditable() ? p -> value.performEdit(p, v -> obj.display(p, page), r -> obj.display(p, page)) : null)
                                    .append(Language.NODE_SECTION_EDIT_BUTTON)

                                    .reset()
                                    .append(" ")

                                    .format(Language.FORMAT_SUBTITLE)
                                    .append(node.getName(), ": ")

                                    .format(node.isValid() ? Language.FORMAT_SUBTITLE : Language.FORMAT_ERROR)
                                    .hover(((ValueNode) node).hover)

                                    .format(Language.FORMAT_INFO)
                                    .style(TextStyles.ITALIC)
                                    .action(visualizable ? value::visualize : null)
                                    .append(value.getReadTransform().apply(value.getValue()))
                                    .build());
                            }
                        });
                    }

                    return elements;
                })).build();

            obj.initialize();
            return this.obj;
        }

    }

    private TextMenu menu;
    private LanguageText hover;
    Map<String, Node<?>> children = new LinkedHashMap<>();

    private SectionNode() {
    }

    /**
     * @return an immutable list of this {@link SectionNode}'s child {@link Node}s
     */
    public Map<String, Node<?>> getChildren() {
        return Collections.unmodifiableMap(this.children);
    }

    @Override
    public List<Translatable> getIssues() {
        Map<String, Node<?>> items = this.getChildren();
        List<Translatable> issues = new ArrayList<>();
        int itemIssues = 0;

        for (Node<?> item : items.values())
            if (!item.isValid()) itemIssues++;

        if (itemIssues > 0) issues.add(Language.NODE_LIST_ISSUES.withParameters(itemIssues));
        return issues;
    }

    @Override
    public boolean isDefault() {
        for (Node<?> child : this.children.values()) {
            if (!child.isDefault()) return false;
        }

        return true;
    }

    @Override
    public void load(ConfigurationNode from) {
        if (from.isVirtual()) return;
        if (from.hasMapChildren()) {
            from.getChildrenMap().forEach((rkey, value) -> {
                String key = rkey.toString();
                if (this.getChildren().containsKey(key)) this.getChildren().get(key).load(value);
            });
        } else throw new SerializationException("ConfigurationNode was not a section type");
    }

    @Override
    public void save(ConfigurationNode to) {
        this.getChildren().forEach((key, value) -> value.save(to.getNode(key)));
    }

    @Override
    public void display(PageDisplayable parent, Player to, int page) {
        this.menu.display(parent, to, page);
    }

}
