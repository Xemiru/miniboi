package com.github.xemiru.miniboi.config;

/**
 * Exception denoting a generic error occuring during a process involving a {@link Node}.
 */
public class ConfigurationException extends RuntimeException {

    public ConfigurationException(String message) {
        super(message);
    }

}
