package com.github.xemiru.miniboi.language;

import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.text.translation.Translation;

import java.util.Locale;

/**
 * A {@link Translatable} comprised of multiple others.
 */
public class ComposedTranslatable implements Translatable, Translation {

    /**
     * Creates a new {@link ComposedTranslatable} from the given parameters.
     *
     * <p>{@link Translatable} objects are appended to the internal array as-is. Any other types are converted through
     * toString and then appended as {@link LiteralTranslatable}s.</p>
     *
     * @param args the parts of the composed translatable
     * @return a ComposedTranslatable
     */
    public static ComposedTranslatable of(Object... args) {
        Translatable[] arr = new Translatable[args.length];
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            if (arg instanceof Translatable) arr[i] = (Translatable) arg;
            else arr[i] = LiteralTranslatable.of(arg.toString());
        }

        return new ComposedTranslatable(arr);
    }

    private Translatable[] transl;

    private ComposedTranslatable(Translatable[] transl) {
        this.transl = transl;
    }

    @Override
    public Translation getTranslation() {
        return this;
    }

    @Override
    public String getId() {
        return "";
    }

    @Override
    public String get(Locale locale) {
        StringBuilder sb = new StringBuilder();
        for (Translatable t : this.transl) sb.append(t.getTranslation().get(locale));

        return sb.toString();
    }

    @Override
    public String get(Locale locale, Object... args) {
        return this.get(locale);
    }
}
