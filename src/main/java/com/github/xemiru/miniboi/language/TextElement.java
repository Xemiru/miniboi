package com.github.xemiru.miniboi.language;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.format.TextFormat;

import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * A component of a {@link LanguageText}.
 */
abstract class TextElement {

    private TextFormat format;
    private LanguageText hover;
    private Consumer<Player> action;

    TextElement(TextFormat format, LanguageText hover, Consumer<Player> action) {
        this.format = requireNonNull(format);
        this.hover = hover;
        this.action = action;
    }

    /**
     * @return the format set for this {@link TextElement}
     */
    TextFormat getFormat() {
        return this.format;
    }

    /**
     * @return the hover set for this {@link TextElement}
     */
    Optional<LanguageText> getHover() {
        return Optional.ofNullable(this.hover);
    }

    /**
     * @return the click action set for this {@link TextElement}
     */
    Optional<Consumer<Player>> getAction() {
        return Optional.ofNullable(this.action);
    }

    /**
     * Returns the raw content of this {@link TextElement} under the provided {@link Locale}
     *
     * @param locale the locale to read with
     * @return the content of this TextElement
     */
    abstract String forLocale(Locale locale);

    /** A {@link TextElement} signifying a line break. */
    static final Fixed LINE = fixed(TextFormat.NONE, null, null, "\n");

    /**
     * Create a {@link TextElement} holding static text.
     *
     * @param format the format of the element
     * @param hover the hover action of the element
     * @param action the click action of the element
     * @param text the static text
     * @return a fixed TextElement
     */
    static Fixed fixed(TextFormat format, LanguageText hover, Consumer<Player> action, String text) {
        return new Fixed(format, hover, action, text);
    }

    /**
     * Create a {@link TextElement} holding translatable text.
     *
     * @param format the format of the element
     * @param hover the hover action of the element
     * @param action the click action of the element
     * @param translatable the translatable object
     * @param args parameters for the translatable
     * @return a translatable TextElement
     */
    static TextElement.Translatable translatable(TextFormat format,
                                                 LanguageText hover,
                                                 Consumer<Player> action,
                                                 org.spongepowered.api.text.translation.Translatable translatable,
                                                 Object... args) {
        return new Translatable(format, hover, action, translatable, args);
    }

    /**
     * Create a {@link TextElement} holding another {@link LanguageText}.
     *
     * @param inherited the inherited format
     * @param text the other LanguageText
     * @return a prebuilt TextElement
     */
    static Text text(TextFormat inherited, LanguageText text) {
        return new Text(inherited, text);
    }

    /**
     * Create a {@link TextElement} holding a variable {@link LanguageText}.
     *
     * @param inherited the inherited format
     * @param text the supplying function
     * @return an adaptive TextElement
     */
    static Adaptive adaptive(TextFormat inherited, Supplier<LanguageText> text) {
        return new Adaptive(inherited, text);
    }

    /**
     * @see #fixed(TextFormat, LanguageText, Consumer, String)
     */
    static class Fixed extends TextElement {

        private String text;

        private Fixed(TextFormat format, LanguageText hover, Consumer<Player> action, String text) {
            super(format, hover, action);
            this.text = requireNonNull(text);
        }

        @Override
        String forLocale(Locale locale) {
            return this.text;
        }
    }

    /**
     * @see #translatable(TextFormat, LanguageText, Consumer, org.spongepowered.api.text.translation.Translatable, Object...)
     */
    static class Translatable extends TextElement {

        private org.spongepowered.api.text.translation.Translatable translatable;
        private Object[] args;

        private Translatable(TextFormat format,
                             LanguageText hover,
                             Consumer<Player> action,
                             org.spongepowered.api.text.translation.Translatable translatable,
                             Object... args) {
            super(format, hover, action);

            this.translatable = requireNonNull(translatable);
            this.args = args;
        }

        @Override
        String forLocale(Locale locale) {
            return this.translatable.getTranslation().get(locale, this.args);
        }

    }

    /**
     * @see #text(TextFormat, LanguageText)
     */
    static class Text extends TextElement {

        private LanguageText text;

        private Text(TextFormat inheritedFormat, LanguageText text) {
            super(inheritedFormat.merge(text.rootFormat), null, null);
            this.text = text.copy();

            this.text.rootFormat = this.getFormat();
            text.elements.forEach(element -> element.format = this.text.rootFormat.merge(element.format));
        }

        @Override
        String forLocale(Locale locale) {
            return null;
        }

        public LanguageText getText() {
            return text;
        }

    }

    /**
     * @see #adaptive(TextFormat, Supplier)
     */
    static class Adaptive extends TextElement {

        private Supplier<LanguageText> supp;

        public Adaptive(TextFormat inheritedFormat, Supplier<LanguageText> supp) {
            super(inheritedFormat, null, null);
            this.supp = supp;
        }

        @Override
        String forLocale(Locale locale) {
            return null;
        }

        public LanguageText getCurrent() {
            LanguageText text = supp.get();
            text.rootFormat = this.getFormat().merge(text.rootFormat);
            text.elements.forEach(element -> element.format = text.rootFormat.merge(element.format));

            return text;
        }
    }

}
