package com.github.xemiru.miniboi.language;

import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.text.translation.Translation;

import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

/**
 * Holds a translation based on its ID.
 */
public class LanguageKey implements Translatable, Translation {

    private static final String FORMAT_REGEX = "(?<!\\$)\\$";
    private static BiFunction<Locale, String, String> func = null;

    private static void checkFunc() {
        if (func == null)
            throw new IllegalStateException("Translation function not set.");
    }

    /**
     * Creates a new {@link LanguageKey} with the provided translation ID.
     *
     * @param id the ID
     */
    public static LanguageKey of(String id) {
        return new LanguageKey(id);
    }

    /**
     * Creates a new {@link LanguageKey} with the provided translation ID and forced parameters.
     *
     * @param id the ID
     * @param args the parameters
     */
    public static LanguageKey of(String id, Object... args) {
        return new LanguageKey(id, args);
    }

    /**
     * Sets the function used to convert a given {@link Locale} and ID to a translation.
     *
     * <p>{@link LanguageKey}s will refuse to work if this is not set.</p>
     *
     * @param func the translation function
     */
    public static void setTranslationFunction(BiFunction<Locale, String, String> func) {
        LanguageKey.func = requireNonNull(func, "function");
    }

    private String id;
    private Object[] args = null;
    private Function<String, String> transform = str -> str;

    /**
     * Creates a new {@link LanguageKey} with the provided translation ID.
     *
     * @param id the ID
     */
    public LanguageKey(String id) {
        this.id = id;
    }

    /**
     * Creates a new {@link LanguageKey} with the provided translation ID and forced parameters.
     *
     * @param id the ID
     * @param args the parameters
     */
    public LanguageKey(String id, Object... args) {
        this.id = id;
        this.args = args;
    }

    /**
     * Creates a copy of this {@link LanguageKey} that use the provided parameters by default.
     *
     * @param args the parameters to use
     * @return a new LanguageKey
     */
    public LanguageKey withParameters(Object... args) {
        return new LanguageKey(this.id, args);
    }

    public LanguageKey withTransform(Function<String, String> transform) {
        LanguageKey dupeKey = new LanguageKey(this.id, this.args);
        dupeKey.transform = requireNonNull(transform);

        return dupeKey;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String get(Locale locale) {
        checkFunc();
        requireNonNull(locale, "locale");

        if (this.args != null) return this.get(locale, this.args);
        return this.transform.apply(func.apply(locale, this.id));
    }

    @Override
    public String get(Locale locale, Object... params) {
        checkFunc();
        requireNonNull(locale, "locale");

        String result = func.apply(locale, this.id);
        for (int i = 0; i < params.length; i++)
            result = result.replaceAll(FORMAT_REGEX + (i + 1), Matcher.quoteReplacement(params[i].toString()));

        return this.transform.apply(result.replaceAll(Pattern.quote("$$"), Matcher.quoteReplacement("$")));
    }

    @Override
    public Translation getTranslation() {
        return this;
    }

}
