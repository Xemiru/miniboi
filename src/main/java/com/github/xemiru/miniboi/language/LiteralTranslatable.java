package com.github.xemiru.miniboi.language;

import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.text.translation.Translation;

import java.util.Locale;

/**
 * A {@link Translatable} object holding static text.
 */
public class LiteralTranslatable implements Translatable, Translation {

    /**
     * Create a new {@link LiteralTranslatable} of the provided text.
     *
     * @param literal the static text
     * @return a new LiteralTranslatable
     */
    public static LiteralTranslatable of(String literal) {
        return new LiteralTranslatable(literal);
    }

    private String literal;

    /**
     * Create a new {@link LiteralTranslatable} of the provided text.
     *
     * @param literal the static text
     */
    public LiteralTranslatable(String literal) {
        this.literal = literal;
    }

    @Override
    public Translation getTranslation() {
        return this;
    }

    @Override
    public String getId() {
        return "";
    }

    @Override
    public String get(Locale locale) {
        return this.literal;
    }

    @Override
    public String get(Locale locale, Object... args) {
        return this.literal;
    }
}
