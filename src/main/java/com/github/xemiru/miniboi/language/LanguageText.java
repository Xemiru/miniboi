package com.github.xemiru.miniboi.language;

import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.channel.MessageChannel;
import org.spongepowered.api.text.channel.MessageReceiver;
import org.spongepowered.api.text.chat.ChatType;
import org.spongepowered.api.text.chat.ChatTypes;
import org.spongepowered.api.text.format.TextFormat;
import org.spongepowered.api.text.translation.locale.Locales;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

/**
 * Holds {@link TextElement}s in order to create {@link Text} objects that change based on the {@link Locale} of the
 * receiver.
 */
public class LanguageText {

    /**
     * @return a builder for {@link LanguageText}
     */
    public static TextBuilder builder() {
        return builder(0);
    }

    /**
     * @param wrap the maximum length of each line
     * @return a builder for {@link LanguageText}
     */
    public static TextBuilder builder(int wrap) {
        return new TextBuilder(wrap);
    }

    /**
     * Quickly create {@link LanguageText} of the provided objects.
     *
     * @param text the text to create
     * @return a LanguageText
     *
     * @see TextBuilder#append(TextFormat, Object...)
     */
    public static LanguageText of(Object... text) {
        return LanguageText.builder().append(text).build();
    }

    /**
     * Quickly create and format {@link LanguageText} of the provided objects.
     *
     * @param format the format to use
     * @param text the text to create
     * @return a LanguageText
     *
     * @see TextBuilder#append(TextFormat, Object...)
     */
    public static LanguageText withFormat(TextFormat format, Object... text) {
        return LanguageText.withFormat(0, format, text);
    }

    /**
     * Quickly create and format {@link LanguageText} of the provided objects.
     *
     * @param wrap the maximum length of each line
     * @param format the format to use
     * @param text the text to create
     * @return a LanguageText
     *
     * @see TextBuilder#append(TextFormat, Object...)
     */
    public static LanguageText withFormat(int wrap, TextFormat format, Object... text) {
        return LanguageText.builder(wrap).format(format).append(text).build();
    }

    /**
     * Helper class for generating {@link Text} from {@link LanguageText} {@link TextElement}s.
     */
    private class TextHelper {

        private Locale locale;
        private int lineLength = 0;
        private int wrap = LanguageText.this.wrap;
        private List<Text> line = new ArrayList<>();
        private List<Text> lines = new ArrayList<>();

        TextHelper(Locale locale) {
            this.locale = locale;
        }

        public void add(TextElement element) {
            if (element == TextElement.LINE) {
                this.finishLine();
                return;
            }

            if (element instanceof TextElement.Text) {
                ((TextElement.Text) element).getText().elements.forEach(this::add);
                return;
            }

            if (element instanceof TextElement.Adaptive) {
                ((TextElement.Adaptive) element).getCurrent().elements.forEach(this::add);
                return;
            }

            String content = element.forLocale(this.locale);
            if (content.isEmpty()) return;

            if (wrap > 0) {
                String part = content;
                int maxLength = wrap - lineLength;

                while (true) {
                    if (part.length() > maxLength) {
                        // find the right-most space in the next <wrap> characters
                        int lastSpace = part.lastIndexOf(' ', maxLength);

                        if (lastSpace <= -1) {
                            lastSpace = part.lastIndexOf(' ', wrap);
                            if (lastSpace <= -1) {
                                // does not fit on any line; break apart
                                this.finishElement(element, part.substring(0, maxLength));
                                part = this.trimLeft(part.substring(maxLength));
                            }

                            // does not fit on current line; put on next line
                        } else {
                            // fits on current line
                            this.finishElement(element, part.substring(0, lastSpace));
                            part = this.trimLeft(part.substring(lastSpace));
                        }

                        this.finishLine();
                    } else {
                        this.finishElement(element, part);
                        break;
                    }
                }
            } else this.finishElement(element, content);
        }

        private void finishLine() {
            lineLength = 0;
            lines.add(Text.join(line));
            line.clear();
        }

        private List<Text> finishLines() {
            if (!line.isEmpty()) this.finishLine();
            return this.lines;
        }

        private void finishElement(TextElement element, String content) {
            this.lineLength += content.length();

            Text.Builder builder = Text.builder(content).format(element.getFormat());
            element.getHover()
                .map(it -> it.toText(this.locale))
                .map(TextActions::showText)
                .ifPresent(builder::onHover);
            element.getAction()
                .<Consumer<CommandSource>>map(it -> src -> {
                    if (src instanceof Player) it.accept((Player) src);
                }).map(TextActions::executeCallback)
                .ifPresent(builder::onClick);

            line.add(builder.build());
        }

        private String trimLeft(String str) {
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isWhitespace(str.charAt(i))) return str.substring(i);
            }

            return str;
        }

    }

    int wrap = 0;
    boolean variable = false;
    TextFormat rootFormat = TextFormat.NONE;
    List<TextElement> elements = new ArrayList<>();

    // only if there're no translatable elements
    private Text cache;
    private List<Text> cacheList;

    LanguageText() {
    }

    /**
     * Creates each line from the current set of elements.
     *
     * @param locale the locale to create with
     * @return text lines
     */
    private List<Text> rawLines(Locale locale) {
        if (this.cacheList != null) return this.cacheList;
        TextHelper helper = new TextHelper(locale);
        this.elements.forEach(helper::add);
        return helper.finishLines();
    }

    /**
     * Creates each line from this {@link LanguageText}'s set of elements.
     *
     * @param locale the locale to create with
     * @return lines of {@link Text}
     */
    public List<Text> lines(Locale locale) {
        if (this.cacheList != null) return this.cacheList;
        List<Text> rawLines = this.rawLines(locale);
        List<Text> lines = new ArrayList<>();
        rawLines.forEach(line -> lines.add(line.toBuilder().format(this.rootFormat).build()));

        if (!variable) this.cacheList = lines;
        return lines;
    }

    /**
     * Creates a single {@link Text} object from this {@link LanguageText}'s set of elements.
     *
     * @param locale the locale to create with
     * @return a Text
     */
    public Text toText(Locale locale) {
        if (this.cache != null) return this.cache;

        Text text = Text.joinWith(Text.NEW_LINE, this.rawLines(locale)).toBuilder().format(this.rootFormat).build();
        if (!variable) this.cache = text;
        return text;
    }

    /**
     * Sends the text of this {@link LanguageText} to the provided {@link MessageReceiver}.
     *
     * @param receiver the receiver to send to
     */
    public void send(MessageReceiver receiver) {
        Locale locale = Locales.EN_US;
        if (receiver instanceof CommandSource) locale = ((CommandSource) receiver).getLocale();
        receiver.sendMessage(this.toText(locale));
    }

    /**
     * Sends the text of this {@link LanguageText} to all members of the provided {@link MessageChannel}.
     *
     * @param channel the channel to send to
     */
    public void send(MessageChannel channel) {
        this.send(null, channel);
    }

    /**
     * Sends the text of this {@link LanguageText} to all members of the provided {@link MessageChannel}, tagging the
     * provided object as the sender.
     *
     * @param sender the object to provide as the sender
     * @param channel the channel to send to
     * @see MessageChannel#transformMessage(Object, MessageReceiver, Text, ChatType)
     */
    public void send(Object sender, MessageChannel channel) {
        channel.getMembers().forEach(receiver -> {
            Locale locale = Locales.EN_US;
            if (receiver instanceof CommandSource) locale = ((CommandSource) receiver).getLocale();

            Text message = this.toText(locale);
            channel.transformMessage(sender, receiver, message, ChatTypes.CHAT);
        });
    }

    /**
     * @return a copy of this {@link LanguageText}.
     */
    LanguageText copy() {
        LanguageText clone = new LanguageText();

        clone.wrap = this.wrap;
        clone.rootFormat = this.rootFormat;
        clone.variable = this.variable;
        clone.elements = new ArrayList<>(this.elements);

        return clone;
    }

}
