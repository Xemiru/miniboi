package com.github.xemiru.miniboi.language;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColor;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextFormat;
import org.spongepowered.api.text.format.TextStyle;
import org.spongepowered.api.text.format.TextStyles;
import org.spongepowered.api.text.translation.Translatable;

import java.util.function.Consumer;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * Builder for {@link LanguageText} objects.
 */
public class TextBuilder {

    private LanguageText hover = null;
    private Consumer<Player> action = null;
    private TextFormat format = TextFormat.NONE;
    private LanguageText obj = new LanguageText();

    /**
     * Creates a new {@link TextBuilder} with text wrapping set.
     *
     * @param wrap the wrap length
     */
    TextBuilder(int wrap) {
        if (wrap < 0) throw new IllegalArgumentException("Wrap length cannot be negative");
        this.obj.wrap = wrap;
    }

    /**
     * Sets the hover text of the next appended elements.
     *
     * <p>The set hover text will inherit the current set format for any elements choosing to inherit.</p>
     *
     * @param hover the hover text, or null to remove
     * @return this TextBuilder
     */
    public TextBuilder hover(LanguageText hover) {
        if (hover == null) this.hover = null;
        else {
            this.hover = hover.copy();
            this.hover.rootFormat = this.format.merge(this.hover.rootFormat);
        }

        return this;
    }

    /**
     * Sets the action of the next appended elements.
     *
     * @param action the action, or null to remove
     * @return this TextBuilder
     */
    public TextBuilder action(Consumer<Player> action) {
        this.action = action;
        return this;
    }

    /**
     * Sets the color of the next appended elements.
     *
     * @param color the new color
     * @return this TextBuilder
     */
    public TextBuilder color(TextColor color) {
        this.format(TextFormat.of(requireNonNull(color), this.format.getStyle()));
        return this;
    }

    /**
     * Sets the style of the next appended elements.
     *
     * @param style the new style
     * @return this TextBuilder
     */
    public TextBuilder style(TextStyle style) {
        this.format(TextFormat.of(this.format.getColor(), requireNonNull(style)));
        return this;
    }

    /**
     * Sets the format of the next appended elements.
     *
     * @param format the new format
     * @return this TextBuilder
     */
    public TextBuilder format(TextFormat format) {
        this.format = requireNonNull(format);
        return this;
    }

    /**
     * Sets the next appended elements' format to inherit from a parent format and removes the current hover/action
     * properties.
     *
     * <p>Specifically, if the result of this builder is appended to another builder, elements set to inherit will
     * receive the builder's current format.</p>
     *
     * @return this TextBuilder
     */
    public TextBuilder inherit() {
        this.hover = null;
        this.action = null;
        this.format(TextFormat.NONE);
        return this;
    }

    /**
     * Forces the next appended elements' format to use Minecraft's defaults and removes the current hover/action
     * properties.
     *
     * <p>If the proceeding elements need to inherit from a parent format (i.e. format received from being appended to
     * another builder), use {@link #inherit()}.</p>
     *
     * @return this TextBuilder
     */
    public TextBuilder reset() {
        this.hover = null;
        this.action = null;
        this.format(TextFormat.of(TextColors.RESET, TextStyles.RESET));
        return this;
    }

    /**
     * Starts a new line.
     *
     * <p>Equivalent to calling {@link #line(int)} and passing 1.</p>
     *
     * @return this TextBuilder
     */
    public TextBuilder line() {
        return this.line(1);
    }

    /**
     * Starts a new line with empty lines prior (if more than one).
     *
     * @param count the amount of new lines to add
     * @return this TextBuilder
     */
    public TextBuilder line(int count) {
        if (count < 1) throw new IllegalArgumentException("Line count must be 1 or greater");
        for (int i = 0; i < count; i++) this.obj.elements.add(TextElement.LINE);
        return this;
    }

    /**
     * Appends the provided objects as text with the options set on this {@link TextBuilder}.
     *
     * <p>See {@link #append(TextFormat, Object...)} for rules used when encountering specific types.</p>
     *
     * @param text the text objects to add
     * @return this TextBuilder
     *
     * @see #append(TextFormat, Object...)
     */
    public TextBuilder append(Object... text) {
        return this.append(this.format, text);
    }

    /**
     * Appends the provided objects as text with the properties set on this {@link TextBuilder} and a color override.
     *
     * <p>See {@link #append(TextFormat, Object...)} for rules used when encountering specific types.</p>
     *
     * @param color the color override
     * @param text the text objects to add
     * @return this TextBuilder
     */
    public TextBuilder append(TextColor color, Object... text) {
        return this.append(TextFormat.of(color, this.format.getStyle()), text);
    }

    /**
     * Appends the provided objects as text with the properties set on this {@link TextBuilder} and a style override.
     *
     * <p>See {@link #append(TextFormat, Object...)} for rules used when encountering specific types.</p>
     *
     * @param style the style override
     * @param text the text objects to add
     * @return this TextBuilder
     */
    public TextBuilder append(TextStyle style, Object... text) {
        return this.append(TextFormat.of(this.format.getColor(), style), text);
    }

    /**
     * Appends the provided objects as text with the properties set on this {@link TextBuilder} and a format override.
     *
     * <p>For all unlisted object types, the object is converted to a string using its own toString method and then
     * appended normally.</p>
     * <ul>
     * <li>{@link Translatable} objects are provided to the {@link #translated(TextFormat, Translatable, Object...)}
     * with no parameters.</li>
     * <li>Other {@link LanguageText} objects' elements are appended to the end of this TextBuilder. Various rules are
     * applied when appending another LanguageText.
     * <ul><li>The provided LanguageText will inherit the format currently set. This TextBuilder's format will be set to
     * that of the last appended element.</li>
     * <li>The appended LanguageText will follow the wrapping rules of this TextBuilder and disregard its own. This will
     * not affect forced line breaks.</li>
     * </ul></li>
     * </ul>
     *
     * @param format the format override
     * @param text the text objects to add
     * @return this TextBuilder
     */
    public TextBuilder append(TextFormat format, Object... text) {
        StringBuilder sb = new StringBuilder();
        for (Object obj : text) {
            if (obj instanceof Translatable) {
                this.consume(format, sb);
                this.translated(format, (Translatable) obj);
            } else if (obj instanceof LanguageText) {
                this.consume(format, sb);
                this.appendText((LanguageText) obj);
            } else sb.append(obj);
        }

        this.consume(format, sb);
        return this;
    }

    /**
     * Appends the provided Translatable object as text with the provided parameters and the properties set on this
     * {@link TextBuilder}.
     *
     * @param text the Translatable
     * @param args parameters for the Translatable
     * @return this TextBuilder
     */
    public TextBuilder translated(Translatable text, Object... args) {
        return this.translated(this.format, text, args);
    }

    /**
     * Appends the provided Translatable object as text with the provided parameters, the properties set on this
     * {@link TextBuilder} and a color override.
     *
     * @param color the color override
     * @param text the Translatable
     * @param args parameters for the Translatable
     * @return this TextBuilder
     */
    public TextBuilder translated(TextColor color, Translatable text, Object... args) {
        return this.translated(TextFormat.of(color, this.format.getStyle()), text, args);
    }

    /**
     * Appends the provided Translatable object as text with the provided parameters, the properties set on this
     * {@link TextBuilder} and a style override.
     *
     * @param style the style override
     * @param text the Translatable
     * @param args parameters for the Translatable
     * @return this TextBuilder
     */
    public TextBuilder translated(TextStyle style, Translatable text, Object... args) {
        return this.translated(TextFormat.of(this.format.getColor(), style), text, args);
    }

    /**
     * Appends the provided Translatable object as text with the provided parameters, the properties set on this
     * {@link TextBuilder} and a format override.
     *
     * @param format the format override
     * @param text the Translatable
     * @param args parameters for the Translatable
     * @return this TextBuilder
     */
    public TextBuilder translated(TextFormat format, Translatable text, Object... args) {
        this.obj.variable = true;
        return this.addElement(TextElement.translatable(format, this.hover, this.action, text, args));
    }

    /**
     * Appends an element that may change every time the resulting {@link LanguageText} gets turned into {@link Text}.
     *
     * @param supp the supplier of the adaptive element
     * @return this TextBuilder
     */
    public TextBuilder adaptive(Supplier<LanguageText> supp) {
        this.obj.variable = true;
        return this.addElement(TextElement.adaptive(this.format, supp));
    }

    /**
     * Creates the {@link LanguageText} built using this {@link TextBuilder}.
     *
     * @return a new LanguageText
     */
    public LanguageText build() {
        return this.obj.copy();
    }

    private void consume(TextFormat format, StringBuilder text) {
        if (text.length() > 0) {
            this.addElement(TextElement.fixed(format, this.hover, this.action, text.toString()));
            text.setLength(0);
        }
    }

    private void appendText(LanguageText text) {
        this.addElement(TextElement.text(this.format, text));
        this.format = this.format.merge(text.elements.get(text.elements.size() - 1).getFormat());
    }

    private TextBuilder addElement(TextElement element) {
        if (this.obj.rootFormat == null) this.obj.rootFormat = this.format;
        this.obj.elements.add(element);
        return this;
    }

}
