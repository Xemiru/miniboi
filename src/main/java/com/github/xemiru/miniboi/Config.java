package com.github.xemiru.miniboi;

import com.github.xemiru.miniboi.config.NodeTypes;
import com.github.xemiru.miniboi.config.SectionNode;
import com.github.xemiru.miniboi.config.ValueNode;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LiteralTranslatable;
import com.github.xemiru.miniboi.util.Choice;
import com.github.xemiru.miniboi.util.Exceptions;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.gson.GsonConfigurationLoader;
import org.spongepowered.api.text.translation.Translatable;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Config {

    public enum OperationMode {
        NOP(Language.CONFIG_OPERATION_OPMODE_VALUE_NOP_LABEL, Language.CONFIG_OPERATION_OPMODE_VALUE_NOP_DESC),
        MANUAL(Language.CONFIG_OPERATION_OPMODE_VALUE_MANUAL_LABEL, Language.CONFIG_OPERATION_OPMODE_VALUE_MANUAL_DESC),
        AUTOMATIC(Language.CONFIG_OPERATION_OPMODE_VALUE_AUTOMATIC_LABEL, Language.CONFIG_OPERATION_OPMODE_VALUE_AUTOMATIC_DESC);

        private Translatable[] meta;

        OperationMode(Translatable label, Translatable desc) {
            this.meta = new Translatable[] { label, desc };
        }

        public Translatable[] getMeta() {
            return this.meta;
        }
    }

    // region config

    private SectionNode ROOT = SectionNode.builder()
        .setId("config")
        .setName(Language.CONFIG_LABEL)
        .setDescription(Language.CONFIG_DESC)
        .build();

    // region config: operation

    private SectionNode OPERATION = SectionNode.builder(ROOT)
        .setId("operation")
        .setName(Language.CONFIG_OPERATION_LABEL)
        .setDescription(Language.CONFIG_OPERATION_DESC)
        .build();

    private ValueNode<OperationMode> OPERATION_MODE = ValueNode.builder(OPERATION,
        NodeTypes.ENUM(OperationMode.class, Language.CONFIG_OPERATION_OPMODE_LABEL,
            Language.CONFIG_OPERATION_OPMODE_DESC,
            OperationMode::getMeta))
        .setId("opmode")
        .setInitialValue(OperationMode.MANUAL)
        .build();

    private ValueNode<String> OPERATION_GAME_SELECTION = ValueNode.builder(OPERATION,
        NodeTypes.CHOICE(NodeTypes.NULLABLE(NodeTypes.STRING), () -> {
            List<Choice<String>> choices = new ArrayList<>();
            choices.add(Choice.of(null, Language.CONFIG_OPERATION_OPMODEGAME_VALUE_RANDOM_LABEL,
                Language.CONFIG_OPERATION_OPMODEGAME_VALUE_RANDOM_DESC));
            return choices;
        }))
        .setId("opmode_game")
        .setName(Language.CONFIG_OPERATION_OPMODEGAME_LABEL)
        .setDescription(Language.CONFIG_OPERATION_OPMODEGAME_DESC)
        .setInitialValue(null)
        .setReadTransform(map -> {
            if (map == null) return Language.CONFIG_OPERATION_OPMODEGAME_VALUE_RANDOM_LABEL;

            // TODO compare with known game list
            return LiteralTranslatable.of("<unimplemented>");
        })
        .build();

    private ValueNode<String> OPERATION_MAP_SELECTION = ValueNode.builder(OPERATION,
        NodeTypes.CHOICE(NodeTypes.NULLABLE(NodeTypes.STRING), () -> {
            List<Choice<String>> choices = new ArrayList<>();
            choices.add(Choice.of(null, Language.CONFIG_OPERATION_OPMODEMAP_VALUE_RANDOM_LABEL,
                Language.CONFIG_OPERATION_OPMODEMAP_VALUE_RANDOM_DESC));
            return choices;
        }))
        .setId("opmode_map")
        .setName(Language.CONFIG_OPERATION_OPMODEMAP_LABEL)
        .setDescription(Language.CONFIG_OPERATION_OPMODEMAP_DESC)
        .setInitialValue(null)
        .setReadTransform(map -> {
            if (map == null) return Language.CONFIG_OPERATION_OPMODEMAP_VALUE_RANDOM_LABEL;

            try {
                return LiteralTranslatable.of(this.miniboi.getMapManager().getKnownMap(map).get()
                    .flatMap(it -> it.getMeta().getName())
                    .orElse("<unknown map>"));
            } catch (Exception e) {
                Exceptions.sneakyThrow(e);
                return null;
            }
        })
        .build();

    // endregion

    private ValueNode<Boolean> EDITOR_ENABLED = ValueNode.builder(ROOT, NodeTypes.BOOLEAN)
        .setId("editor")
        .setName(Language.CONFIG_EDITOR_LABEL)
        .setDescription(Language.CONFIG_EDITOR_DESC)
        .setInitialValue(true)
        .build();

    // endregion

    private static final String CONFIG_FILE = "config.json";

    private Miniboi miniboi;
    private Path configFile;
    private GsonConfigurationLoader gcl = GsonConfigurationLoader.builder()
        .setSource(() -> {
            try {
                return Files.newBufferedReader(this.configFile);
            } catch (Exception e) {
                Exceptions.sneakyThrow(e);
                return null;
            }
        })
        .setSink(() -> {
            try {
                return Files.newBufferedWriter(this.configFile);
            } catch (Exception e) {
                Exceptions.sneakyThrow(e);
                return null;
            }
        })
        .build();

    public Config(Miniboi miniboi) {
        this.miniboi = miniboi;
        this.configFile = this.miniboi.getConfigFolder().resolve(CONFIG_FILE);

        this.reload();
    }

    public void reload() {
        try {
            if (Files.exists(this.configFile))
                this.ROOT.load(gcl.load());
            else this.save();
        } catch (Exception e) {
            Exceptions.sneakyThrow(e);
        }
    }

    public void save() {
        try {
            if (Files.notExists(this.configFile)) {
                Files.createDirectories(this.configFile.getParent());
                Files.createFile(this.configFile);
            }

            ConfigurationNode node = gcl.createEmptyNode();
            this.ROOT.save(node);
            gcl.save(node);
        } catch (Exception e) {
            Exceptions.sneakyThrow(e);
        }
    }

}
