package com.github.xemiru.miniboi;

/**
 * A minigame.
 */
public interface Game {

    void init();

    void clean();

}
