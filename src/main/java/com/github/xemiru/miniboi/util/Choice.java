package com.github.xemiru.miniboi.util;

import org.spongepowered.api.text.translation.Translatable;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * A labeled choice.
 *
 * @param <T> the value type held by the Choice
 */
public class Choice<T> {

    /**
     * Creates a {@link Choice} holding the provided value.
     *
     * @param value the value to hold
     * @param label the label of the Choice
     * @param <T> the value type
     * @return a Choice
     */
    public static <T> Choice<T> of(T value, Translatable label) {
        return Choice.of(value, label, null);
    }

    /**
     * Creates a {@link Choice} holding the provided value.
     *
     * @param value the value to hold
     * @param label the label of the Choice
     * @param description the description of the Choice
     * @param <T> the value type
     * @return a Choice
     */
    public static <T> Choice<T> of(T value, Translatable label, Translatable description) {
        Choice<T> obj = new Choice<>();
        obj.value = value;
        obj.label = requireNonNull(label);
        obj.description = description;

        return obj;
    }

    private T value;
    private Translatable label, description;

    private Choice() {
    }

    /**
     * @return the value held by this {@link Choice}
     */
    public T getValue() {
        return this.value;
    }

    /**
     * @return the label of this {@link Choice}
     */
    public Translatable getLabel() {
        return this.label;
    }

    /**
     * @return the description of this {@link Choice}?
     */
    public Optional<Translatable> getDescription() {
        return Optional.ofNullable(this.description);
    }

}
