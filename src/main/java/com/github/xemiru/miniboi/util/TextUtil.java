package com.github.xemiru.miniboi.util;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import com.github.xemiru.miniboi.language.TextBuilder;
import org.spongepowered.api.text.translation.Translatable;

import java.util.List;

/**
 * Utility class for text creation.
 */
public class TextUtil {

    public static LanguageText createIssueHover(List<Translatable> issues) {
        if (issues.isEmpty()) return Language.info(Language.NODE_GENERIC_ISSUES_NONE);
        else {
            TextBuilder tb = LanguageText.builder()
                .format(Language.FORMAT_ERROR)
                .translated(Language.NODE_GENERIC_ISSUES_SOME, issues.size());

            issues.forEach(issue -> tb.line().append("- ", issue));
            return tb.build();
        }
    }

}
