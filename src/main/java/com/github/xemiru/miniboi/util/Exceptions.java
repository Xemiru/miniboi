package com.github.xemiru.miniboi.util;

import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LiteralTranslatable;
import com.github.xemiru.miniboi.map.MapException;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.text.translation.Translatable;

import java.util.concurrent.CompletionException;

/**
 * Utility class for exception handling.
 */
public class Exceptions {

    public static <T extends Throwable> void sneakyThrow(Throwable t) throws T {
        throw (T) t;
    }

    public static <T extends Exception> void expected() throws T {
        // does nothing but forcibly make the compiler happy
    }

    /**
     * @return whether or not the error was unexpected and thus necessary to log extra information
     */
    public static boolean displayError(CommandSource src, Throwable e, Translatable msg) {
        if (e instanceof CompletionException && e.getCause() != null) e = e.getCause();
        Translatable extra = e instanceof MapException ?
            ((MapException) e).getTranslatableMessage().orElse(LiteralTranslatable.of(e.getMessage())) : Language.GENERIC_ERROR_UNKNOWN;
        Language.msgError(msg, ": ", extra).send(src);

        return !(e instanceof MapException);
    }

}
