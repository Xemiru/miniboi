package com.github.xemiru.miniboi.util;

import com.github.xemiru.miniboi.Miniboi;
import com.github.xemiru.miniboi.language.Language;
import com.github.xemiru.miniboi.language.LanguageText;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.channel.MutableMessageChannel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ChatFreezeManager {

    private Map<UUID, List<Text>> frozen = new HashMap<>();

    public ChatFreezeManager(Miniboi miniboi) {
        Sponge.getEventManager().registerListeners(miniboi, this);
    }

    @Listener(order = Order.LAST)
    public void onFrozenChat(MessageChannelEvent.Chat e) {
        if (!e.isMessageCancelled() && !e.isCancelled()) {
            e.getCause().first(Player.class).ifPresent(source -> {
                if (this.frozen.containsKey(source.getUniqueId())) {
                    Language.msgError(Language.CHATFREEZE_ERROR_NOCHAT).send(source);
                    e.setCancelled(true);
                }
            });
        }
    }

    @Listener(order = Order.POST)
    public void onChat(MessageChannelEvent.Chat e) {
        if (!e.isMessageCancelled() && !e.isCancelled()) {
            e.getChannel().ifPresent(chan -> {
                MutableMessageChannel mutable = chan.asMutable();
                chan.getMembers().forEach(member -> {
                    UUID uid = (member instanceof Player) ? ((Player) member).getUniqueId() : null;
                    boolean send = uid == null || !frozen.containsKey(uid);
                    if (!send) {
                        frozen.get(uid).add(e.getMessage());
                        mutable.removeMember(member);
                    }
                });

                e.setChannel(mutable);
            });
        }
    }

    @Listener
    public void onQuit(ClientConnectionEvent.Disconnect e) {
        this.frozen.remove(e.getTargetEntity().getUniqueId());
    }

    /**
     * @param p the {@link Player} to query
     * @return whether or not the given Player currently has chat messages frozen by this {@link ChatFreezeManager}
     */
    public boolean isFrozen(Player p) {
        return this.frozen.containsKey(p.getUniqueId());
    }

    /**
     * @param p the {@link Player} to change the state of
     * @param flag whether or not to freeze the given Player's chat messages
     */
    public void setFrozen(Player p, boolean flag) {
        if (flag != this.isFrozen(p)) {
            if (flag) {
                this.frozen.put(p.getUniqueId(), new ArrayList<>());
                Language.msgInfo(Language.CHATFREEZE_ENABLE).send(p);
            } else {
                List<Text> messages = this.frozen.get(p.getUniqueId());
                LanguageText.builder()
                    .append(Language.SYSTEM_HEADING)
                    .action(pl -> messages.forEach(pl::sendMessage))
                    .translated(Language.CHATFREEZE_MISSED, messages.size())
                    .build().send(p);

                this.frozen.remove(p.getUniqueId());
            }
        }
    }

}
