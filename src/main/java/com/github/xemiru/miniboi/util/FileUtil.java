package com.github.xemiru.miniboi.util;

import com.github.xemiru.miniboi.map.LocalMapHandler;
import com.github.xemiru.miniboi.map.MapFormatException;
import com.github.xemiru.miniboi.map.MapMeta;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.gson.GsonConfigurationLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * Utility class for file handling.
 */
public class FileUtil {

    private static final Logger LOG = LoggerFactory.getLogger(FileUtil.class);

    private static final int VARIATION_LIMIT = Short.MAX_VALUE;
    private static final Path TMP_FOLDER =
        Paths.get(new File(System.getProperty("java.io.tmpdir")).toURI()).resolve("miniboi");
    private static final String TMP_PREFIX = "miniboi_internal_";

    public static void copyRecursive(Path from, Path to) throws IOException {
        if (Files.exists(from)) Files.walk(from)
            .forEach(f -> {
                try {
                    Path dest = to.resolve(from.relativize(f));
                    if (Files.isDirectory(f)) Files.createDirectories(dest);
                    else Files.copy(f, dest);
                } catch (Exception e) {
                    Exceptions.sneakyThrow(e);
                }
            });
    }

    public static void deleteRecursive(Path dir) throws IOException {
        if (Files.exists(dir)) Files.walk(dir)
            .sorted(Comparator.reverseOrder())
            .forEach(f -> {
                try {
                    Files.delete(f);
                } catch (Exception e) {
                    Exceptions.sneakyThrow(e);
                }
            });
    }

    public static void forEachFile(Path dir, Consumer<Path> consumer) throws IOException {
        if (Files.isDirectory(dir)) {
            Iterator<Path> foreach = Files.list(dir).iterator();
            while (foreach.hasNext()) FileUtil.forEachFile(foreach.next(), consumer);
        } else consumer.accept(dir);
    }

    public static Optional<String> getUnusedVariant(String name, Predicate<String> check) {
        int i = 0;
        String current = name;
        while (i < VARIATION_LIMIT) {
            if (check.test(current)) return Optional.of(current);
            current = name + i++;
        }

        return Optional.empty();
    }

    public static void extract(ZipFile file, Path destination) throws IOException {
        Enumeration<? extends ZipEntry> entries = file.entries();
        while (entries.hasMoreElements()) {
            ZipEntry en = entries.nextElement();
            extract(en, file.getInputStream(en), destination);
        }
    }

    public static void extract(ZipInputStream stream, Path destination) throws IOException {
        ZipEntry en;
        while ((en = stream.getNextEntry()) != null) {
            extract(en, stream, destination);
        }
    }

    private static void extract(ZipEntry en, InputStream s, Path destination) throws IOException {
        Files.createDirectories(destination);
        byte[] buf = new byte[1024];

        Path entry = destination.resolve(en.getName()).toAbsolutePath();
        if (!entry.startsWith(destination)) throw new IllegalStateException("zip slip pls");

        if (en.isDirectory()) Files.createDirectories(entry);
        else {
            Files.createDirectories(entry.getParent());
            OutputStream fos = Files.newOutputStream(entry);
            int read;
            while ((read = s.read(buf)) > 0) fos.write(buf, 0, read);

            fos.close();
        }
    }

    public static Path reserveTempPath() {
        return FileUtil
            .getUnusedVariant(TMP_PREFIX, candidate -> {
                if (candidate.equals(TMP_PREFIX)) return false;
                return Files.notExists(TMP_FOLDER.resolve(candidate));
            }).map(TMP_FOLDER::resolve)
            .orElseThrow(() -> new IllegalStateException("Unable to reserve temporary directory"));
    }

    public static void cleanTempPaths() {
        try {
            FileUtil.deleteRecursive(TMP_FOLDER);
        } catch (Exception e) {
            LOG.warn("Could not clean temporary folder", e);
        }
    }

    public static MapMeta loadMetaFile(Path worldDir) {
        Path metaFile = worldDir.resolve(LocalMapHandler.META_FILE_NAME);
        if (Files.notExists(metaFile)) throw new MapFormatException("Invalid map zip file");

        MapMeta meta = new MapMeta();

        try {
            meta.getRoot().load(GsonConfigurationLoader.builder()
                .setPath(worldDir.resolve(metaFile))
                .build().load());
        } catch (Exception e) {
            Exceptions.sneakyThrow(e);
        }

        return meta;
    }

    public static Map<String, ConfigurationNode> loadGameConfigs(Path worldDir) {
        // load configs

        try {
            Map<String, ConfigurationNode> configs = new HashMap<>();
            Path configDir = worldDir.resolve(LocalMapHandler.MAP_CONFIG_FOLDER);
            if (Files.exists(configDir)) {
                Files.list(configDir)
                    .filter(it -> Files.isRegularFile(it))
                    .forEach(configFile -> {
                        try {
                            String id = configFile.getFileName().toString();
                            int lastDot = id.lastIndexOf('.');
                            if (lastDot > -1) id = id.substring(0, lastDot);

                            configs.put(id, GsonConfigurationLoader.builder()
                                .setPath(configFile)
                                .build().load());
                        } catch (IOException e) {
                            LOG.warn(String.format("Could not load configuration at %s", configFile), e);
                        }
                    });
            }

            return configs;
        } catch (Exception e) {
            Exceptions.sneakyThrow(e);
            return null;
        }
    }

}
