package com.github.xemiru.miniboi;

/**
 * Databag class containing meta information about a game.
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public class GameMeta {

    /*
    // required
    private PluginContainer plugin;
    private Class<? extends Game> gameCls;
    private Class<? extends Configuration> configCls;
    private Class<? extends Configuration> mapConfigCls;

    /**
     * Instantiates a new {@link GameMeta} owned by the provided plugin.
     *
     * @param plugin the plugin owning the GameMeta
     * @param gameCls the class of the {@link Game} described by this meta
     *
    public GameMeta(Object plugin, Class<? extends Game> gameCls) {
        this(plugin, gameCls, null);
    }

    /**
     * Instantiates a new {@link GameMeta} owned by the provided plugin.
     *
     * @param plugin the plugin owning the GameMeta
     * @param gameCls the class of the {@link Game} described by this meta
     * @param configCls the {@link Configuration} schema used by this meta's target game
     *
    public GameMeta(Object plugin, Class<? extends Game> gameCls, Class<? extends Configuration> configCls) {
        this(plugin, gameCls, configCls, null);
    }

    /**
     * Instantiates a new {@link GameMeta} owned by the provided plugin.
     *
     * @param plugin the plugin owning the GameMeta
     * @param gameCls the class of the {@link Game} described by this meta
     * @param configCls the {@link Configuration} schema used by this meta's target game
     * @param mapConfigCls the {@link Configuration} schema used by this meta's target game's maps
     *
    public GameMeta(Object plugin,
                    Class<? extends Game> gameCls,
                    Class<? extends Configuration> configCls,
                    Class<? extends Configuration> mapConfigCls) {
        this.plugin = Sponge.getPluginManager()
            .fromInstance(requireNonNull(plugin))
            .orElseThrow(() -> new IllegalArgumentException("Provided object was not a plugin"));

        this.gameCls = requireNonNull(gameCls);
        this.configCls = configCls;
        this.mapConfigCls = mapConfigCls;
    }

    /**
     * @return the plugin of the {@link Game} owning this {@link GameMeta}
     *
    public PluginContainer getPlugin() {
        return this.plugin;
    }

    /**
     * @return the class of the {@link Game} owning this {@link GameMeta}
     *
    public Class<? extends Game> getGameClass() {
        return this.gameCls;
    }

    /**
     * @return the configuration schema class associated with the {@link Game} owning this {@link GameMeta}?
     *
    public Optional<Class<? extends Configuration>> getConfigClass() {
        return Optional.ofNullable(this.configCls);
    }

    /**
     * @return the map configuration schema class associated with the {@link Game} owning this {@link GameMeta}?
     *
    public Optional<Class<? extends Configuration>> getMapConfigClass() {
        return Optional.ofNullable(this.mapConfigCls);
    }*/

}
