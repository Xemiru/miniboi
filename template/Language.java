package com.github.xemiru.miniboi.language;

import com.github.xemiru.miniboi.Miniboi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.asset.Asset;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextFormat;
import org.spongepowered.api.text.format.TextStyles;
import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.text.translation.locale.Locales;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Holds methods and constants for language consistency in messages and format.
 */
public class Language {

    private static final Logger LOG = LoggerFactory.getLogger(Language.class);
    private static final Map<String, Map<String, String>> LANGUAGES = new HashMap<>();

    /** An array of recognized language codes. */
    public static final String[] AVAILABLE_LANGUAGES = { /*LANG_CODES*/ };
    public static final int MAX_WRAP_LENGTH = 40;

    public static final TextFormat FORMAT_TITLE = TextFormat.of(TextColors.AQUA);
    public static final TextFormat FORMAT_SUBTITLE = TextFormat.of(TextColors.DARK_AQUA);
    public static final TextFormat FORMAT_ACTIVE = TextFormat.of(TextColors.LIGHT_PURPLE);
    public static final TextFormat FORMAT_INACTIVE = TextFormat.of(TextColors.DARK_GRAY);

    public static final TextFormat FORMAT_INFO = TextFormat.of(TextColors.GRAY);
    public static final TextFormat FORMAT_PROMPT = TextFormat.of(TextColors.YELLOW);
    public static final TextFormat FORMAT_ERROR = TextFormat.of(TextColors.RED);

    /** The heading used for message texts. */
    public static LanguageText SYSTEM_HEADING;

    /**
     * Creates hover text using the provided title and description objects.
     *
     * @param title the title
     * @param desc the description
     * @return hover LanguageText
     */
    public static LanguageText hover(String title, Object... desc) {
        return Language.hover(LiteralTranslatable.of(title), desc);
    }

    /**
     * Creates hover text using the provided title and description objects.
     *
     * @param title the title
     * @param desc the description
     * @return hover LanguageText
     */
    public static LanguageText hover(Translatable title, Object... desc) {
        TextBuilder tb = LanguageText.builder(MAX_WRAP_LENGTH)
            .append("[", title, "]")
            .line()
            .format(Language.FORMAT_INFO);

        if (desc.length > 0) tb.append(desc);
        else tb.append(Language.GENERIC_NODESC);
        return tb.build();
    }

    /**
     * Creates hover text using the provided title and description objects.
     *
     * @param title the title
     * @param desc the description
     * @return hover LanguageText
     */
    public static LanguageText hover(LanguageText title, Object... desc) {
        TextBuilder tb = LanguageText.builder(MAX_WRAP_LENGTH)
            .append("[", title, "]")
            .line()
            .format(Language.FORMAT_INFO);

        if (desc.length > 0) tb.append(desc);
        else tb.append(Language.GENERIC_NODESC);
        return tb.build();
    }

    /**
     * Formats the provided text as info text.
     *
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText info(Object... text) {
        return Language.wrapInfo(0, text);
    }

    /**
     * Formats the provided text as info text.
     *
     * @param wrap the maximum length of each line
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText wrapInfo(int wrap, Object... text) {
        return LanguageText.withFormat(wrap, FORMAT_INFO, text);
    }

    /**
     * Formats the provided text as an info message.
     *
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText msgInfo(Object... text) {
        return Language.withMsgHeading(FORMAT_INFO, text);
    }

    /**
     * Formats the provided text as prompt text.
     *
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText prompt(Object... text) {
        return Language.wrapPrompt(0, text);
    }

    /**
     * Formats the provided text as prompt text.
     *
     * @param wrap the maximum length of each line
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText wrapPrompt(int wrap, Object... text) {
        return LanguageText.withFormat(wrap, FORMAT_PROMPT, text);
    }

    /**
     * Formats the provided text as a prompt message.
     *
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText msgPrompt(Object... text) {
        return Language.withMsgHeading(FORMAT_PROMPT, text);
    }

    /**
     * Formats the provided text as error text.
     *
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText error(Object... text) {
        return Language.wrapError(0, text);
    }

    /**
     * Formats the provided text as error text.
     *
     * @param wrap the maximum length of each line
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText wrapError(int wrap, Object... text) {
        return LanguageText.withFormat(wrap, FORMAT_ERROR, text);
    }

    /**
     * Formats the provided text as an error message.
     *
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText msgError(Object... text) {
        return Language.withMsgHeading(FORMAT_ERROR, text);
    }

    /**
     * Formats the provided text as a subtitle.
     *
     * @param text the text objects to format
     * @return formatted {@link LanguageText}
     */
    public static LanguageText subtitle(Object... text) {
        return LanguageText.builder()
            .format(FORMAT_SUBTITLE)
            .style(TextStyles.ITALIC)
            .append("\u00BB ")
            .append(text)
            .build();
    }

    private static LanguageText withMsgHeading(TextFormat format, Object... text) {
        return LanguageText.builder().append(SYSTEM_HEADING).format(format).append(text).build();
    }

    /**
     * Loads all internal language files.
     */
    public static void initLanguages() {
        for (String language : Language.AVAILABLE_LANGUAGES) {
            try {
                Asset langFile = Sponge.getAssetManager().getAsset(Miniboi.getInstance(),
                    String.format("lang/%s.lang", language))
                    .orElseThrow(() -> new IllegalStateException(
                        String.format("Missing internal language file for language code %s", language)));

                LOG.info(String.format("Loading language file for code %s", language));

                int count = 0;
                Map<String, String> lang = new HashMap<>();
                List<String> lines = langFile.readLines();
                for (String line : lines) {
                    // ignore comments / empty lines
                    if (line.trim().isEmpty() || line.trim().startsWith("#")) continue;

                    int eq = line.indexOf('=');
                    String key = line.substring(0, eq).trim();
                    String value = line.substring(eq + 1).trim();

                    try {
                        Language.class.getDeclaredField(key.replace('.', '_').toUpperCase());
                        lang.put(key, value);
                    } catch (NoSuchFieldException e) {
                        LOG.warn(String.format("Language file declared unknown language key %s", key));
                    }

                    count++;
                }

                if (count < Language._KEY_COUNT) {
                    LOG.warn(String.format("Language file is incomplete! Missing %d language key(s)", Language._KEY_COUNT - count));
                    LOG.warn("Missing keys will fall back to internal en_US values");
                }

                LANGUAGES.put(language, lang);
            } catch (Exception e) {
                if (language.equals(Locales.EN_US.toString()))
                    throw new IllegalStateException("Failed to load primary language file.", e);
                else
                    LOG.warn(String.format("Failed to load language file for code %s; it will be unavailable for the session.", language), e);
            }
        }

        if (!LANGUAGES.containsKey(Locales.EN_US.toString()))
            throw new IllegalStateException("Failed to load primary language file.");

        LanguageKey.setTranslationFunction((locale, key) -> {
            Map<String, String> en = LANGUAGES.get(Locales.EN_US.toString());
            String value = LANGUAGES.getOrDefault(locale.toString(), en).getOrDefault(key, en.get(key));
            if (value == null) throw new IllegalArgumentException("Unknown language key");
            return value;
        });

        Language.SYSTEM_HEADING = LanguageText.builder()
            .translated(TextColors.BLUE, Language.GENERIC_SYSTEMTAG)
            .append(TextColors.DARK_GRAY, " \u00BB ")
            .build();
    }

/*LANG_CONSTANTS*/
}
